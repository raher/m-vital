#make a list of headers to be added to targets if wanted
file(GLOB_RECURSE HEADER_LIST  *.h*)

set(ILMSENS_LIB_TOOLS_HEADERS "${HEADER_LIST}" CACHE INTERNAL "Header list for tools library")
