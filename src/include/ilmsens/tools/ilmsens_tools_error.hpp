/// @file ilmsens_tools_error.hpp
/// @brief Exceptions used in Ilmsens tools.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#include <string>
#include <stdexcept>
#include "ilmsens/ilmsens_error.h"

#ifndef ILMSENS_TOOLS_ERROR_HPP
#define ILMSENS_TOOLS_ERROR_HPP

/**   
 * @defgroup tools_exception Exceptions & safety tools used for Ilmsens tools.
 * @brief Exceptions only used by Ilmsens tools.
 * @{
 */  

namespace ilmsens 
{
  namespace tools
  {

    /**  Processing-specific error class storing an Ilmsens error code along with a message and a general error number*/
    class Error : public std::runtime_error 
    {
      typedef std::runtime_error Base;

      public:
        Error(const std::string& pMsg, ilmsens_error pCode = ILMSENS_ERROR_UNKNOWN, int pErrNum = 0)
          : Base(pMsg), mErrCode(pCode), mErrNum(pErrNum) {}
        
        ilmsens_error getErrorCode() const { return mErrCode; }
        int           getErrorNum()  const { return mErrNum; }

      private:
        ilmsens_error mErrCode;
        int           mErrNum;
    }; // class Error

  } //namespace tools
} //namespace ilmsens

/** @} tools_exception */

#endif // ILMSENS_TOOLS_ERROR_HPP
