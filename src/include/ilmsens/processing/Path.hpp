/// @file Path.hpp
/// @brief objects contains heartrate with overtones
///
/// @author Saskia Holzlehner <saskia.holzlehner@yahoo.de>

#ifndef PATH_HPP
#define PATH_HPP

#include "IRF_FRF.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {
    // path-class
    class Path
    {
      public:
        Path();
        Path(TSample pFreq);
        Path(TRealIRF pFreqVec);
        ~Path();

        /* Getter */
        TRealIRF getPath();
        TSample getFreq(int pIdx);
        int getPathLen();
        TRealIRF getPathDiff();
        double getLastDiff();
        double getPathMean();
        bool isValid();

        /* Methods */
        bool includes(Path pPath);
        void extendPath(TSample newFreq);
        void setValid(bool valid);
        bool validationTest(double pMinFreqDist, double pMaxFreqDist, double pFreqMeanDist, double pMultDist);

      private:
        TRealIRF mPath;
        TRealIRF mPathDiff;
        double mPathMean;
        bool mValid;
        
        void setPath(TSample pFreq);
        void setPath(TRealIRF pFreqVec);
        

    };

  } //namespace processing
} //namespace ilmsens

#endif // PATH_HPP


