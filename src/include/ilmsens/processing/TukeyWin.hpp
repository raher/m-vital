/// @file TukeyWin.hpp
/// @brief Creates Tukey-Window as vector
///
/// @author Saskia Holzlehner <saskia.holzlehner@yahoo.de>

#ifndef TUKEYWIN_HPP
#define TUKEYWIN_HPP

#include "IRF_FRF.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {

    /** Vector for the Tukey-Window */
    class TukeyWin
    {
      public:
        TukeyWin(unsigned int pWinLen, double pTaper);
        TukeyWin(unsigned int pWinLen);                 // default-Taper = 0.5
        ~TukeyWin();

        const TSampleVec& getTukeyVec(void);              ///< return tukey window as vector

      private:
        
        // window properties
        unsigned int mWinLen;	

        // taper of the window
        double mTaper;

        // vector which represents the tukey window
        TSampleVec mTukeyVec;
    };

  } //namespace processing
} //namespace ilmsens

#endif // TUKEYWIN_HPP
