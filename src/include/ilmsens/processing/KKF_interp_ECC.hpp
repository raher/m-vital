/// @file KKF_interp_ECC.hpp
/// @brief Cross correlation with ideal M-sequence of given IRF with FFT interpolation and up-shifting for ECC sensors.
///
/// Cross correleation is performed by using FFT/IFFT method.
/// Ideal M-sequence is the reference function for correlation.
/// An oversampled mlbsXX.txt is used where XX is the MLBS order. 
/// Before IFFT, the spectrum is shifted from 1st to 2nd nyquist zone 
/// as ECC sensors measure signals physically there.
//  During IFFT, interpolation is performed by zero-padding the spectrum.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef KKF_INTERP_ECC_HPP
#define KKF_INTERP_ECC_HPP

#include <memory>

#include <fftw3.h>    // FFTW

#include "FFTw_tools.hpp"
#include "IRF_FRF.hpp"
#include "KKF.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {

    /** Class for correlating raw ECC M-sequence (implicit oversampling) - data with ideal MLBS using cross-correlation and FFT interpolation */
    class KKF_interp_ECC
    {
      public:

        KKF_interp_ECC(unsigned int pMLBSOrder = 9, unsigned int pOV = 3, unsigned int pIPLen = 4096, bool pUseFilterWin = false);
        ~KKF_interp_ECC();

        const TRealIRF&     calculateKKF(const TRealIRF& pData);      ///< calculate Cross Correlation Function of given data
        const TRealIRF&     calculateECC_KKF(const TRealIRF& pData);  ///< get interpolated and ECC up-mixed result of MLBS cross correlation
        const TRealIRF&     getLastIRF  (void);                   ///< return IRF of last correlation operation
        const fftw_complex* getLastFRF  (void);                   ///< return FRF of last correlation operation
        const fftw_complex* getLastECC_FRF(void);                 ///< return FRF of last ECC correlation operation

      private:

        // MLBS properties
        unsigned int mMLBSOrder;                                  ///< Order of MLBS
        unsigned int mOV;                                         ///< oversampling factor
        unsigned int mMLBSLen;                                    ///< Length of oversampled MLBS (samples)

        // interpolation properties
        unsigned int mIPLen;                                      ///< Length of interpolated data (samples)

        // Reference MLBS as read from file
        TRealIRF mRefMLBS;                                        ///< Ideal MLBS in time domain used as reference fopr correlation

        // frequency domian filtering window
        TRealIRF mFrqWin;                                         ///< frequency window that can be used to supress noise after correlation

        // Result of correlation
        TRealIRF mCorrIRF;                                        ///< Result of KKF (time-domain)

        // Data variables for FFTW
        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDRef;   ///< Data vector for reference spectrum from ideal MLBS

        std::unique_ptr< double >                                                       mTDRaw;   ///< Data vectors time domain for KKF
        std::unique_ptr< double >                                                       mTDCorr;  ///< Data vectors time domain for KKF

        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDRaw;   ///< Data vectors frequency domain for KKF
        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDCorr;  ///< Data vectors frequency domain for KKF

        // FFTW-Plans for KKF
        fftw_plan mFFTPlan;                                       ///< FFT of measured data
        fftw_plan mIFFTPlan;                                      ///< IFFT of correleated data
        
        // Result of interpolation
        TRealIRF mInterpIRF;                                      ///< Result of interpolation (time-domain)

        // Data variables for FFTW
        std::unique_ptr< double >                                                       mTDIp;    ///< Data vector interpolated time domain
        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDIp;    ///< Data vector interpolated frequency domain for ECC KKF
        
        // FFTW-Plans for KKF
        fftw_plan mIFFTPlanIp;                                    ///< IFFT of interpolated data
    };

  } //namespace processing
} //namespace ilmsens

#endif // KKF_INTERP_ECC_HPP
