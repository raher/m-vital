/// @file ilmsens_processing_error.hpp
/// @brief Exceptions used in Ilmsens processing classes.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#include <string>
#include <stdexcept>
#include "ilmsens/ilmsens_error.h"

#ifndef ILMSENS_PROCESSING_ERROR_HPP
#define ILMSENS_PROCESSING_ERROR_HPP

/**   
 * @defgroup processing_exception Exceptions & safety tools used for Ilmsens processing.
 * @brief Exceptions only used by Ilmsens processing classes.
 * @{
 */  

namespace ilmsens 
{
  namespace processing
  {

    /**  Processing-specific error class storing an Ilmsens error code along with a message and a general error number*/
    class Error : public std::runtime_error 
    {
      typedef std::runtime_error Base;

      public:
        Error(const std::string& pMsg, ilmsens_error pCode = ILMSENS_ERROR_UNKNOWN)
          : Base(pMsg), mErrorCode(pCode) {}
        
        ilmsens_error getErrorCode() const { return mErrorCode; }

      private:
        ilmsens_error mErrorCode;
    }; // class Error

  } //namespace processing
} //namespace ilmsens

/** @} processing_exception */

#endif // ILMSENS_PROCESSING_ERROR_HPP
