/// @file Hilbert.hpp
/// @brief Performing Hilbert transform of given IRF/FRF.
///
/// Hilbert transform for measured data (time or frequency domain).
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef HILBERT_HPP
#define HILBERT_HPP

#include <memory>

#include <fftw3.h>  // FFTW

#include "IRF_FRF.hpp"
#include "FFTw_tools.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {

    /** Class performing Hilbert transform of given IRF/FRF using FFTw library */
    class Hilbert
    {
      public:

        Hilbert(unsigned int pWinLen = 511);
        ~Hilbert();
        
        const TRealIRF& calculateHilbTD (const TRealIRF& pTDData);      ///< Hilbert transform of given time domain data
        const TRealIRF& calculateHilbFD (const fftw_complex* pFDData);  ///< Hilbert transform of given frequency data
        
      private:

        // signal properties
        unsigned int mWinLen;                                           ///< Length of IRF/FRF to transform (samples)
        
        // result of Hilbert transform
        TRealIRF mHilbIRF;                                              ///< Result of Hilbert transform in time domain
        
        // reference weight vector for Hilbert transform
        TRealIRF mH;                                                    ///< Hilbert transform reference weights in frequency domain

        // data variables for FFTW
        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mTDHilb;  ///< Data vectors time domain
        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDHilb;  ///< Data vectors frequency domain

        // FFTW-Plans for KKF
        fftw_plan mFFTPlan;   ///< FFT of signal when starting in time domain
        fftw_plan mIFFTPlan;  ///< IFFT after transform

        //perform actual Hilbert transform using frequency domain data;
        const TRealIRF& doHilbert(void);                                ///< Internal function that performs Hilbert transform on frequency domain data
    };

  } //namespace processing
} //namespace ilmsens

#endif // HILBERT_HPP
