(c) Ilmsens GmbH

Notes on processing M-sequence UWB data:
======================================

The text files "mlbsXX.txt" contain the ideal
M-sequences (MLBS) used in the UWB transmitters
of our sensors. These can be used to translate raw 
measured data into impulse responses by correlation.

XX represents the order of the MLBS. Please consult 
your hardware manual, to check which order is used 
in your devices.

An M-sequence of order m is a binary sequence of
maximum length (i.e. 2^m - 1). Each bit (aka chip) of 
the sequence can be either +1 or -1. 
It is a deterministic signal, but the pattern of +1 and -1 
looks pseudo-random.

The numbers in the text files are floating point, but the 
sequence can also be represented with integers (+1/-1).
The sequences are not DC-free. Each sequence chip is 
represented by only one sample, i.e. if your sensor uses
oversampling or frequency conversion frontends you have
to prepare the basic MLBS accordingly to get the real 
reference for correlation.

