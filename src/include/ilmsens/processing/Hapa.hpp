/// @file Hapa.hpp
/// @brief 
///
/// @author Saskia Holzlehner <saskia.holzlehner@yahoo.de>

#ifndef HAPA_HPP
#define HAPA_HPP

#include "IRF_FRF.hpp"

#include "ilmsens/processing/Path.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {
    /** type representing vector of paths */
    typedef std::vector<Path> PathVec;

    /** Hapa-class */
    class Hapa
    {
      public:
        Hapa(TSampleVec pSpectrum,
             TSampleVec pFreq,
             double     pMinHeartFreq,
             double     pMaxHeartFreq,
             double     pThrPeak,
             double     pTolHeartFreqIntvl,
             double     pTolFreqDist,
             int        pMaxPathLen);
        ~Hapa();

        const double& getHeartRate(void);

    private:
      double mHeartRate;
      double mMinHeartFreq;
      double mMaxHeartFreq;
      double mThrPeak;
      double mTolHeartFreqIntvl;
      double mTolFreqDist;
      int    mMaxPathLen;

      PathVec extendPath(PathVec pPathVec, TRealIRF pNextPeaks);
      TRealIRF  subvecOfIntvl(TRealIRF pVec, double pMin, double pMax);
      PathVec addOvertones(PathVec pOldPaths, TRealIRF pOvertones);
      PathVec concardPathVec(PathVec pPathVec1, PathVec pPathVec2);
    };

  } //namespace processing
} //namespace ilmsens

#endif // HAPA_HPP


