/// @file IRF_FRF.hpp
/// @brief Definition of sample, vector, and matrix types for M-sequence sensors.
///
/// The types defined here can be used when doing data processing on MLBS sensor data.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef IRF_FRF_HPP
#define IRF_FRF_HPP

#include <cstdint>
#include <vector>


namespace ilmsens 
{
  namespace processing
  {
    namespace types
    {

      /**   
       * @defgroup irf_frf_sample_types Type definitions for samples used during processing.
       * @brief Double precision data types are the default to avoid instabilities/inaccuracies due to quantisation errors.
       * @{
       */  

      /** Sample and delay data type (real) */
      typedef double TSampleBig;            ///< Type representing amplitudes with double precision
      typedef double TDelayBig;             ///< Type representing delay times with double precision

      /** Sample data type (real) )with less accuracy and memory requirements */
      typedef float TSampleSmall;           ///< Type representing amplitudes with single precision
      typedef float TDelaySmall;            ///< Type representing delay times with single precision


      /** Sample data type used in real vectors and matrices (default: double) */
      #ifdef __IRF_FRF_USE_SMALL_SAMPLES
        typedef TSampleSmall TSample;
        typedef TDelaySmall  TTimeDelay;
      #else
        typedef TSampleBig   TSample;
        typedef TDelayBig    TTimeDelay;
      #endif

      /** Complex sample data type complex using same sample type as the real sample*/
      struct sSampleCx
      {
        TSample mRe;      ///< real part
        TSample mIm;      ///< imag part
      };

      typedef struct sSampleCx TSampleCx; // bit-compatible with C99 complex type in <complex.h> and FFTw3

      /** @} irf_frf_sample_types */

      /**   
       * @defgroup irf_frf_vector_types Type definitions for sample vectors and matrices used during processing.
       * @brief Uses the sample types defined in @ref irf_frf_sample_types.
       * @{
       */  

      /** Measured IRF types */
      typedef std::vector<TSample> TSampleVec;
      typedef TSampleVec           TRealIRF;       ///< Alias for real-valued vectors in time domain

      /** Measured FRF types */
      typedef std::vector<TSampleCx> TSampleCxVec;
      typedef TSampleCxVec           TCplxIRF;     ///< Alias for complex-valued vectors in time domain
      typedef TSampleCxVec           TCplxFRF;     ///< Alias for complex-valued vectors in frequency domain

      /** Radargram/matrix type */
      typedef std::vector< TSampleVec > TSampleMat;
      typedef TSampleMat                TRealMat;       ///< Alias for real-valued matrices in time domain

      /**  Complex matrix type */
      typedef std::vector< TSampleCxVec > TSampleCxMat;


      /** Additional information type */
      typedef uint32_t                TAddInfo;
      typedef std::vector< TAddInfo > TAddInfos;        ///< Information vector type
      typedef TAddInfos               TStatusInfo;      ///< Alias for status information vector

      /** @} irf_frf_vector_types */

    } //namespace types
  } //namespace processing
} //namespace ilmsens


#endif // IRF_FRF_HPP
