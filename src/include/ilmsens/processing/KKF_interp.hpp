/// @file KKF_interp.hpp
/// @brief Cross correlation with ideal M-sequence of given IRF with FFT interpolation.
///
/// Cross correleation is performed by using FFT/IFFT method.
/// Ideal M-sequence is the reference function for correlation.
/// It is loaded from /usr/include/ilmsens/processing/mlbsXX.txt
/// where XX is the MLBS order. During IFFT, interpolation is performed 
/// by zero-padding the spectrum.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef KKF_INTERP_HPP
#define KKF_INTERP_HPP

#include <memory>

#include <fftw3.h>    // FFTW

#include "FFTw_tools.hpp"
#include "IRF_FRF.hpp"
#include "KKF.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {

    /** Class for correlating raw M-sequence - data with ideal MLBS using cross-correlation and FFT interpolation */
    class KKF_interp : public KKF
    {
      public:

        KKF_interp(unsigned int pMLBSLen = 511, unsigned int pIPLen = 512);
        ~KKF_interp();

        const TRealIRF&     calculateInterpolation(void);         ///< get interpolated result of last cross correlation
        const fftw_complex* getLastInterpolationFRF(void);        ///< return last interpolated FRF

      private:

        // interpolation properties
        unsigned int mIPLen;                                      ///< Length of interpolated data (samples)
        
        // Result of interpolation
        TRealIRF mInterpIRF;                                      ///< Result of interpolation (time-domain)

        // Data variables for FFTW
        std::unique_ptr< double >                                                       mTDIp;    ///< Data vector interpolated time domain
        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDIp;    ///< Data vector interpolated frequency domain for KKF
        
        // FFTW-Plans for KKF
        fftw_plan mIFFTPlanIp;                                    ///< IFFT of interpolated data
    };

  } //namespace processing
} //namespace ilmsens

#endif // KKF_INTERP_HPP
