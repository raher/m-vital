/// @file vector_iteration_functions.hpp
/// @brief Simple processing funtions that iterate along vectors.
///
/// min/max/abs/sum/rms/conj/arg, etc.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef VECTOR_ITERATION_FUNCTIONS_HPP
#define VECTOR_ITERATION_FUNCTIONS_HPP

//STL
#include <limits>

#include "IRF_FRF.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {
    /** type representing indexes of samples in a vector */
    typedef std::vector<size_t> TIndexVec;

    /** type representing special indexed sample of a vector */
    typedef struct  sIdxSample
                    { 
                      sIdxSample(): mVal(0.0), mIdx(0) {}
                      sIdxSample(TSample pVal, unsigned int pIdx): mVal(pVal), mIdx(pIdx) {}
                      TSample       mVal; 
                      unsigned int  mIdx; 
                    } TIdxSample;
    
    typedef std::vector<TIdxSample> TIdxVec;

    /** type representing min and max sample of a vector */
    typedef struct  sMinMax 
                    { 
                      TIdxSample mMin; 
                      TIdxSample mMax; 
                    } TMinMax;

    /** type representing various IRF parameters */
    typedef struct  sIRFPara
                    { 
                      sIRFPara(): mSum(0.0), mEn(0.0), mVar(0.0), mCoE(0.0) {}
                      TSample    mSum;
                      TSample    mEn; 
                      TSample    mVar; 
                      TTimeDelay mCoE; 

                      TIdxSample mMin; 
                      TIdxSample mMax;
                      TIdxSample mExt;
                    } TIRFPara;


    /************************/
    /** Extrema            **/
    /************************/

    /** Get maximum value of real sample vector */
    const TIdxSample& getRlMax (const TRealIRF& pData);

    /** Get minimum value of real sample vector */
    const TIdxSample& getRlMin (const TRealIRF& pData);

    /** Get minimum + maximum value of real sample vector */
    const TMinMax& getRlMinMax (const TRealIRF& pData);

    /** Find local maxima according to given rules */
    const TIdxVec& findPeaks(const TRealIRF& pData, bool pUseMPH = false, TSample pMPH = 0.0, TSample pThres = 0.0, TSample pProm = 0.0, size_t pMinDist = 0, size_t pNumPeaks = 0);

    /************************/
    /** Power and Energy   **/
    /************************/

    /** Get sum of real sample vector */
    TSample getRlSum (const TRealIRF& pData);

    /** Get mean of real sample vector */
    TSample getRlMean (const TRealIRF& pData);

    /** Get energy of real sample vector */
    TSample getRlEn (const TRealIRF& pData);

    /** Get variance of real sample vector */
    TSample getRlVar (const TRealIRF& pData);

    /** Get centre of energy of real sample vector */
    TTimeDelay getRlCoE(const TRealIRF& pData, int pTimeOff = 0, TSample* pEn = NULL);

    /** Get centre of moment of real sample vector */
    TTimeDelay getRlCoM(const TRealIRF& pData, int pTimeOff = 0, unsigned int pDeg = 2);


    /************************/
    /** IRF analysis       **/
    /************************/

    /** Get direct real IRF parameters */
    const TIRFPara& getRlIRFPara (const TRealIRF& pData);

    /** Get percentile amplitude for IRF */
    const TSample getRlIRFPctl (const TRealIRF& pData, double pPctl = 0.5);


    /************************/
    /** basic vector math  **/
    /************************/

    /** fill vector witl equidistant values */
    void createGridVec (TRealIRF& pVec, TSample pStartVal, TSample pStepVal, size_t pLen);

    /** add vectors */
    void addRlIRFs (TRealIRF& pResult, const TRealIRF& pData1, const TRealIRF& pData2);

    /** sub vectors */
    void subRlIRFs (TRealIRF& pResult, const TRealIRF& pData1, const TRealIRF& pData2);
    void subRlIRFs (TRealIRF& pResult, const TRealIRF& pData , const double&   pVal);

    /** multiply vectors */
    void mulRlIRFs (TRealIRF& pResult, const TRealIRF& pData1, const TRealIRF& pData2);

    /** absolute value of real sample vector */
    void absRlIRFs (TRealIRF& pResult, const TRealIRF& pData);

    /** squared value of real sample vector */
    void sqrRlIRFs (TRealIRF& pResult, const TRealIRF& pData);

    /** scaling of real sample vector */
    void scaleRlIRFs (TRealIRF& pResult, const TRealIRF& pData, TSample pScale);

    /** centering of real sample vector */
    void centerRlIRFs (TRealIRF& pResult, const TRealIRF& pData, TSample pMean, TSample pStd);

    /** scalar product of vectors */
    TSample scalarProd (TRealIRF& pData1, const TRealIRF& pData2);

    /** diff of successive entries of a vector  */
    void entryDiffRlIRFs (TRealIRF& pResult, const TRealIRF& pData);


    /*********************************/
    /** advanced vector operations  **/
    /*********************************/

    /** exponential BGR of real-valued vectors*/
    void expBGRRl (TRealIRF& pBGR, TRealIRF& pBG, const TRealIRF& pUpdate, double pAlpha);

    /** linear interpolation */
    const TSample lin_interpolation(TRealIRF& pFreq, TRealIRF &pSprec, double pSearchFreq);


  } //namespace processing
} //namespace ilmsens

#endif // VECTOR_ITERATION_FUNCTIONS
