/// @file ilmsens_mvital_error.hpp
/// @brief Exception class used in m:vital applications.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#include <string>
#include <stdexcept>
#include "ilmsens/ilmsens_error.h"

#ifndef ILMSENS_MVITAL_ERROR_HPP
#define ILMSENS_MVITAL_ERROR_HPP

/**   
 * @defgroup mvital_exception Exceptions & safety tools used or m:vital apps.
 * @brief Exceptions only used by m:vital apps.
 * @{
 */  

namespace ilmsens 
{
  namespace mvital
  {
    /**  Application-specific error class storing an Ilmsens error code along with a message */
    class Error : public std::runtime_error 
    {
      typedef std::runtime_error Base;

      public:
        Error(const std::string& pMsg, ilmsens_error pCode = ILMSENS_ERROR_UNKNOWN)
          : Base(pMsg), mErrorCode(pCode) {}
        
        ilmsens_error getErrorCode() const { return mErrorCode; }

      private:
        ilmsens_error mErrorCode;
    };
  } //namespace mvital
} //namespace ilmsens

/** @} mvital_exception */

#endif // ILMSENS_MVITAL_ERROR_HPP
