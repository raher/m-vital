/// @file ilmsens_networking_defn.h
/// @brief Definitions for Ilmsens networking classes.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef ILMSENS_NETWORKING_DEFN_H
#define ILMSENS_NETWORKING_DEFN_H


/**   
 * @defgroup networking_timeouts Timeouts and retry counts used for Ilmsens networking tasks.
 * @brief Timeouts for specific operations, along with maximum retry counts.
 * @{
 */  

#define ILMSENS_NETWORKING_TCP_BIND_MAX_RETRY     100       ///< maximum retries when binding to a socket

#define ILMSENS_NETWORKING_PAUSE_US               100000    ///< default sleep interval between retries [us]

#define ILMSENS_NETWORKING_SEND_TIMEOUT_MS        1000      ///< default timeout for sending on a socket

/** @} networking_timeouts */


/**   
 * @defgroup networking_clients Client-related constants for Ilmsens networking.
 * @brief Limits for client connections.
 * @{
 */  

#define ILMSENS_NETWORKING_MAX_CLIENTS            4         ///< maximum number of queued client connection requests on listening socket

#endif // ILMSENS_NETWORKING_DEFN_H
