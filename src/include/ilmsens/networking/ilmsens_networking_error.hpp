/// @file ilmsens_networking_error.hpp
/// @brief Exceptions used in Ilmsens networking classes.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#include <string>
#include <stdexcept>
#include "ilmsens/ilmsens_error.h"

#ifndef ILMSENS_NETWORKING_ERROR_HPP
#define ILMSENS_NETWORKING_ERROR_HPP

/**   
 * @defgroup networking_exception Exceptions & safety tools used for Ilmsens networking.
 * @brief Exceptions only used by Ilmsens networking classes.
 * @{
 */  

namespace ilmsens 
{
  namespace networking
  {

    /**  Networking-specific error class storing an Ilmsens error code along with a message */
    class Error : public std::runtime_error 
    {
      typedef std::runtime_error Base;

      public:
        Error(const std::string& pMsg, ilmsens_error pCode = ILMSENS_ERROR_UNKNOWN, int pErrNo = 0)
          : Base(pMsg), mErrorCode(pCode), mErrNo(pErrNo) {}
        
        ilmsens_error getErrorCode() const { return mErrorCode; }
        int           getErrorNum()  const { return mErrNo; }

      private:
        ilmsens_error mErrorCode;
        int           mErrNo;
    }; // class Error

  } //namespace networking
} //namespace ilmsens

/** @} networking_exception */

#endif // ILMSENS_NETWORKING_ERROR_HPP
