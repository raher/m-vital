/* shared.cpp
 * 
 * Shared variables, memories, synchronisation mutexes
 * used by the various application threads.
 */

// Std. headers
#include <chrono>
#include <thread>

// Own header
#include "shared.hpp"


// Synchronisation mutexes for thread states
std::mutex mMtxLogging;
std::mutex mMtxProcessing;
std::mutex mMtxExit;

// Synchronisation mutexes for configuration and measurement state 
std::mutex mMtxSharedData;


// Synchronisation mutexes for shared memories
std::mutex mMtxDRCache;      // Mutexes for Cache 1: datareader & processing
std::mutex mMtxPRCache;      // Mutexes for Cache 2: processing & streaming
std::mutex mMtxVDCache;      // Mutex for access to vital data packet cache


// Structure for sharing global variables and programming state between running threads
TSharedData mState;


// Program state transition flags
bool mExitShutdwn = false;
bool mLogMShutdwn = false;
bool mProcShutdwn = false;


// Cache memories
TFIFOCache mDRCache; // Buffer between datareader & processing
TFIFOCache mPRCache; // Buffer between processing & streaming

TVDCache mVDCache;   // Buffer between processing and packet streaming thread

/* -------------------------------------------------------------------------------------------------
 * System helpers
 * 
 * All the fine goodies
 * 
 * -------------------------------------------------------------------------------------------------
 */

namespace ilmsens 
{
  namespace mvital
  {
    namespace ces 
    {

      void delay_ms(unsigned int pDel_ms)
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(pDel_ms));
      }

    } //namespace ces
  } //namespace mvital
} //namespace ilmsens
