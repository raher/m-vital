/// @file vdTCPServer.hpp
/// @brief Main and support funtions for a vital data packet streaming TCP server thread.
///
/// The TCP server running in a separate thread will accept a single client connection
/// and forward vital data packets presented at a shared FIFO cache to that client. If no client
/// is connected, packets are simply discarded.
/// 
/// The server may receive simple 1-byte commands for starting or stopping of packet streaming.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef VDTCPSERVER_HPP
#define VDTCPSERVER_HPP


/** TCP constants */
#define VD_TCP_STREAM_PORT         2048        ///< TCP port of streaming connection
#define VD_TCP_STR_CMD_SIZE        1           ///< size of command messages for packet streaming
#define VD_TCP_STR_SEND_TO_MS      2000        ///< TCP send timeout [ms]
#define VD_TCP_STR_WAIT_TO_MS      1000        ///< TCP waiting timeout for select() [s]

#define VD_TCP_CMD_STOP            0x00        ///< command byte from client for stopping vital data packet stream
#define VD_TCP_CMD_START           0x01        ///< command byte from client for starting vital data packet stream
#define VD_TCP_CMD_EXIT            0xff        ///< command byte from client for exitting the server and app

#define VD_TCP_STR_DEBUG_OUTPUT    0           ///< show debug info about streamed data 

#define VD_SEND_STREAM_DEF         false       ///< actually send data to client by default (activate stream?)

namespace ilmsens 
{
  namespace mvital
  {
    namespace ces 
    {
      namespace streaming
      {

        /** Entry point for streaming TCP server thread */
        void* vdTCPServer(void*);

      } //namespace streaming
    } //namespace ces
  } //namespace mvital
} //namespace ilmsens

#endif //VDTCPSERVER_HPP
