/* ctrlTCPServer.cpp
 * 
 * Thread function for TCP server which will accept a single client connection
 * and subsequently receive control commands and info requests. According to the commands,
 * other working threads (Ilmsens DataReader) will be started/stopped and the app's state changed.
 */



/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <cstring>


/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority


// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP stuff & server class
#ifndef WIN32
  #include <arpa/inet.h> // for htonl() call
#endif

#include "ilmsens/networking/TCP_server_single.hpp"
#include "ilmsens/networking/ilmsens_networking_defn.h"
#include "ilmsens/networking/ilmsens_networking_error.hpp"

// Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// main header for module
#include "ctrlTCPServer.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Using clauses
 * -------------------------------------------------------------------------------------------------
 */

using namespace ilmsens::networking;


/* -------------------------------------------------------------------------------------------------
 * Local defines
 * -------------------------------------------------------------------------------------------------
 */

/* converting floats to network byte order */

#ifndef WIN32
  unsigned int htonf(float pVal)
  {
    union
    {
      float uFloat;
      uint32_t uUInt32;
    };
    uFloat = pVal;
    return(htonl(uUInt32));
  }
#else
 //build into winsock2.h!
#endif

/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * TCP command/data & system helpers
 * 
 * -------------------------------------------------------------------------------------------------
 */

// process & execute commands received from TCP client
void processCommand(TCP_server_single& pServer, unsigned int pCmd)
{
  // for receiving parameter values from client (e.g. software averages)
  unsigned int tMsgSize = sizeof(unsigned int) * TCP_MSG_SIZE;
  unsigned int tMsgBuf[TCP_MSG_SIZE];

  // result for sending/receiving
  //std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received Command: " << pCmd << ANSI_COLOR_RESET << std::endl;

  //before a command can be processed, lock access to global state structure.
  //The lock will only be removed, after the ACK has been sent to the client
  //to avoid races due to asynch processing on server and client machine
  mMtxSharedData.lock();

  unsigned int tACK  = htonl(mState.mACK);
  unsigned int tNACK = htonl(mState.mNACK);

  unsigned int tRepl = tACK; // by default, send ACK as reply
  int tLen; // return value for send/receive operations

  //select command
  switch(pCmd) 
  {
    // Enable DEBUG messages: ASCII "DEB1"
    case 0x44454231 : 

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received DEBUG-ENABLE-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mDebFlag = true;
        break;

    // Disable DEBUG messages: ASCII "DEB0"
    case 0x44454230 : 

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received DEBUG-DISABLE-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mDebFlag = false;
        break;

    // Start: ASCII "RUN "
    case 0x52554e20 : 

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received START-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mRunFlag = true;
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Measurement start triggered" << ANSI_COLOR_RESET << std::endl;
        break;

    // Stop: ASCII "STOP"
    case 0x53544f50 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received STOP-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mRunFlag = false;
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Measurement stop triggered" << ANSI_COLOR_RESET << std::endl;
        break;

    // Exit: ASCII "EXIT"
    case 0x45584954 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received EXIT-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mRunFlag  = false;
        //mState.mExitFlag = true;
        mMtxExit.lock();
          mExitShutdwn = true;
        mMtxExit.unlock();
        break;

    // measurement ID: ASCII "MID#"
    case 0x4D494423 :

    // Software Averages: ASCII "SAVG"
    case 0x53415647 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received SOFT-AVG-Command" << ANSI_COLOR_RESET << std::endl;

        //read value from client
        tLen = pServer.receiveTO((void *) tMsgBuf, tMsgSize, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving new SOFT-AVG value!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tMsgSize)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for SOFT-AVG value: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // all was fine, so store new value
          mState.mSW_AVG = ntohl(tMsgBuf[0]);
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing Soft_AVG to be " << std::dec << mState.mSW_AVG << "." << ANSI_COLOR_RESET << std::endl;
        }
        break;

    // Wait cycles: ASCII "WCYC"
    case 0x57435943 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received WAIT-CYC-Command" << ANSI_COLOR_RESET << std::endl;

        //read value from client
        tLen = pServer.receiveTO((void *) tMsgBuf, tMsgSize, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving new WAIT-CYC value!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tMsgSize)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for WAIT-CYC value: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // all was fine, so store new value
          mState.mWaitCyc = ntohl(tMsgBuf[0]);
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing Wait_Cyc to be " << std::dec << mState.mWaitCyc << "." << ANSI_COLOR_RESET << std::endl;
        }
        break;

    // Number of IRFs: ASCII "#IRF"
    case 0x23495246 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received IRF-per-Run-Command" << ANSI_COLOR_RESET << std::endl;

        //read value from client
        tLen = pServer.receiveTO((void *) tMsgBuf, tMsgSize, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving new IRF per run value!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tMsgSize)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for IRF per Run value: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // all was fine, so store new value
          mState.mNumIRF = ntohl(tMsgBuf[0]);
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing IRF per run to be " << std::dec << mState.mNumIRF << "." << ANSI_COLOR_RESET << std::endl;
        }
        break;

    // Get app status: ASCII "STA?"
    case 0x5354413F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-STATE-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get software averages: ASCII "SAV?"
    case 0x5341563F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-SOFT-AVG-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get hardware averages: ASCII "HAV?"
    case 0x4841563F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-HARD-AVG-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Wait cycles: ASCII "WCY?"
    case 0x5743593F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-WAIT-CYC-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Number of IRFs: ASCII "IRF?"
    case 0x4952463F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-NUM-IRF-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get MLBS order: ASCII "MLO?"
    case 0x4D4C4F3F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-MLBS-Order-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get MLBS system clock: ASCII "CLK?"
    case 0x434C4B3F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-RF-Clk-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get sensor ID string: ASCII "SID?"
    case 0x5349443F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-ID-Command" << ANSI_COLOR_RESET << std::endl;
        break;
        
    default:
        std::cout << ANSI_COLOR_CYAN << "Controlling::WARNING: Unknown command: 0x" << std::hex << std::setfill('0') << std::setw(8) << pCmd << std::setfill(' ') << ", please try again..." << ANSI_COLOR_RESET << std::endl;
        tRepl = tNACK; // Send NACK
        break;
  }
  mMtxSharedData.unlock();

  // send reply first
  unsigned int tReplSize = sizeof(tRepl);
  tLen = pServer.sendTO((void *) &tRepl, tReplSize);

  // reply could be sent?
  if (tLen < 0)
  {
    // client connection lost
    std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while sending reply!" << ANSI_COLOR_RESET << std::endl;
  }
  else if (tLen < (int)tReplSize)
  {
    // not enough data sent
    std::cout << ANSI_COLOR_RED << "Controlling::ERROR: could not send full reply: " << std::dec << tLen << "/" << tReplSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
  }

  // finally send additional data, when required by command
  unsigned int tVal;
  float        tFVal;
  char         *tID  = NULL;
  void         *tMsg = NULL;
  tMsgSize           = 0;
  switch(pCmd) 
  {
    // Get app status: ASCII "STA?"
    case 0x5354413F :

      tVal = 0;
        mMtxSharedData.lock();
          // encode flags
          if (mState.mSenAct)   tVal |= 0x01;
          if (mState.mRunFlag)  tVal |= 0x02;
          if (mState.mProcRun)  tVal |= 0x04;
          if (mState.mDebFlag)  tVal |= 0x08;
          if (mState.mMeasFlag) tVal |= 0x20;
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending STATE value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get software averages: ASCII "SAV?"
    case 0x5341563F :

        mMtxSharedData.lock();
          tVal = mState.mSW_AVG; 
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending SOFT-AVG value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get hardware averages: ASCII "HAV?"
    case 0x4841563F :

        mMtxSharedData.lock();
          tVal = mState.mHW_AVG; 
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending HARD-AVG value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Wait cycles: ASCII "WCY?"
    case 0x5743593F :

        mMtxSharedData.lock();
          tVal = mState.mWaitCyc;
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending WAIT-CYC value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Number of IRFs: ASCII "IRF?"
    case 0x4952463F :

        mMtxSharedData.lock();
          tVal = mState.mNumIRF;
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending NUM-IRF value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get MLBS order: ASCII "MLO?"
    case 0x4D4C4F3F :

        mMtxSharedData.lock();
          tVal = mState.mModInfo.mConfig.mOrder; 
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending MLBS-Order value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get MLBS system clock: ASCII "CLK?"
    case 0x434C4B3F :

        mMtxSharedData.lock();
          tFVal = (float)mState.mModInfo.mConfig.mClk; 
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonf(tFVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending f0 clock value to client: " << std::dec << std::setprecision(6) << tFVal << " GHz" << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get sensor ID string: ASCII "SID?"
    case 0x5349443F :

        mMtxSharedData.lock();
          tVal = (unsigned int)strlen(mState.mID);
          tID  = mState.mID;
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending sensor ID to client: '" << std::dec << tID << "'" << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    default:
        break;
  }

  /* send requested value, if any */
  if (tMsgSize)
  {
    tLen = pServer.sendTO(tMsg, tMsgSize);

    // value could be sent?
    if (tLen < 0)
    {
      // client connection lost
      std::cerr << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while sending requested value!" << ANSI_COLOR_RESET << std::endl;
    }
    else if (tLen < (int)tMsgSize)
    {
      // not enough data sent
      std::cerr << ANSI_COLOR_RED << "Controlling::ERROR: could not send requested value fully: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
    }

    // additionally send a string?
    if (tID)
    {
      tLen = pServer.sendTO((void *)tID, (unsigned int)strlen(tID));

      // value could be sent?
      if (tLen < 0)
      {
        // client connection lost
        std::cerr << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while sending string!" << ANSI_COLOR_RESET << std::endl;
      }
      else if (tLen < (int)tMsgSize)
      {
        // not enough data sent
        std::cerr << ANSI_COLOR_RED << "Controlling::ERROR: could not send requested string fully: " << std::dec << tLen << "/" << strlen(tID) << " bytes only!" << ANSI_COLOR_RESET << std::endl;
      }
    }
  }
}


namespace ilmsens 
{
namespace mvital
{
namespace ces 
{
namespace controlling
{


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

/*
// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mvital.ces.ctrl");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}
*/


/* -------------------------------------------------------------------------------------------------
 * Controlling thread
 * 
 * Creates TCP server socket for command connection and accepts one client at a time.
 * 
 * When a client is successfully connected, the DataReader thread is started (which deploys the
 * processing thread if needed). Received commands are dispatched by the process_command function
 * via gloably defined flags.
 * 
 * When the client closes the connection, flags are set to stop a running measurement and 
 * all other threads are told to end themselves.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* ctrlTCPServer(void*)
{
  bool tCtrlExit = false;

  unsigned int tCtrlPort = TCP_COMMAND_PORT;
  TCP_server_single tServer;

  try
  {
    std::cout << ANSI_COLOR_CYAN << "Controlling: Thread created." << ANSI_COLOR_RESET << std::endl;
    
    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(1, &tCPUSet); // CPU #1
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Controlling::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif
    
    /* start server (listening socket) */
    tServer.startServer(tCtrlPort, TCP_CMD_SEND_TO_MS);
    
  }
  catch(ilmsens::mvital::ces::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Controlling::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tCtrlExit = true;
  }
  catch(ilmsens::networking::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Controlling::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tCtrlExit = true;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Controlling::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tCtrlExit = true;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "Controlling::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tCtrlExit = true;
  }

  // serve TCP command connection until the EXIT command has been received
  while(!tCtrlExit) 
  {
    try 
    {
      // give time for synchronisation after a client disconnected
      delay_ms(10);

      std::cout << ANSI_COLOR_CYAN << "Controlling: Waiting for Client at port " << std::dec << tCtrlPort << " ..." << ANSI_COLOR_RESET << std::endl;

      // accept client connection with timeout
      while (!tCtrlExit)
      {
        bool tClient = tServer.acceptClient(TCP_CMD_WAIT_TO_MS);
        
        if (tClient)
        {
          // a new client was accepted
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: connection initiated with client " << tServer.getClientIP() << " ..." << ANSI_COLOR_RESET << std::endl;
          break;  
        }
        else
        {
          // no client was connected, so update the exit flag
          mMtxExit.lock();
            tCtrlExit = mExitShutdwn;
          mMtxExit.unlock();
          /*
          mMtxControlling.lock();
            tCtrlExit = mCtrlShutdwn;
          mMtxControlling.unlock();
          
          // try to drain DR cache, if no client is there or the previous one was disconnected
          mMtxDRCache.lock();
          while(!mDRCache.empty()) 
          {
            mDRCache.pop();
          }
          mMtxDRCache.unlock();
          */
        }
      } // waiting for client

      // a client is here?
      if (!tCtrlExit)
      {
        // Check connection on socket and subsequently serve commands received
        unsigned int tBuf[TCP_CMD_SIZE];
        char         *tBufBytes = (char *) tBuf; // byte-wise access
        unsigned int tCmdSize = sizeof(unsigned int) * TCP_CMD_SIZE;
        bool tCmdRcvd = true;
        unsigned int tRcvd = 0;

        /*
        mMtxControlling.lock();
          tCtrlExit = mCtrlShutdwn;
        mMtxControlling.unlock();
        mMtxSharedData.lock();
          tCtrlExit = mState.mExitFlag;
        mMtxSharedData.unlock();
        */
        while(!tCtrlExit) 
        {
          //did we process a new command last time?
          if (tCmdRcvd)
          {
            std::cout << ANSI_COLOR_CYAN << "Controlling: waiting for command ..." << ANSI_COLOR_RESET << std::endl;
            tCmdRcvd = false;
          }

          /* Reading command */
          
          /* do non-blocking read to allow asynchr. ending of program*/
          int tLen = tServer.receiveTO((void *)(tBufBytes+tRcvd), tCmdSize-tRcvd, TCP_CMD_RECV_TO_MS);

          if (tLen < 0)
          {
            //client connection has been closed
            // need to stop TCP connection, but at the same time close DR thread
            break; // inner loop
          }
          else
          {
            // at least some data may have been read
            tRcvd += tLen;
            //std::cout << ANSI_COLOR_CYAN << "Controlling: received " << tLen << " bytes ..." << ANSI_COLOR_RESET << std::endl;
          }
          
          if (tRcvd == tCmdSize)
          {
            // a complete command has been received, so process it
            processCommand(tServer, ntohl(tBuf[0]));

            // start to wait for a new command
            tRcvd    = 0;
            tCmdRcvd = true;
          }

          // update exit flag
          mMtxExit.lock();
            tCtrlExit = mExitShutdwn;
          mMtxExit.unlock();
          /*
          mMtxSharedData.lock();
            tCtrlExit = mState.mExitFlag;
          mMtxSharedData.unlock();
          mMtxControlling.lock();
            tCtrlExit = tCtrlExit || mCtrlShutdwn;
          mMtxControlling.unlock();
          */
        }
      }
        
      if (tCtrlExit)
      {
        // ends working threads and also exits the control thread!
        std::cout << ANSI_COLOR_CYAN << "Controlling: received EXIT command, closing threads and application ..." << ANSI_COLOR_RESET << std::endl;
      }
      else
      {
        // only closes client connections and end working threads
        std::cout << ANSI_COLOR_CYAN << "Controlling: no or bad client connection on command socket, closing socket ..." << ANSI_COLOR_RESET << std::endl;
      }

      /*
      // set all flags for shutdown
      mMtxSharedData.lock();
        mState.mRunFlag = false; // Stop measurement
      mMtxSharedData.unlock();
      
      mMtxDataReader.lock();
        mDtRdShutdwn    = true;  // Exit datareader thread
      mMtxDataReader.unlock();
        
      mMtxProcessing.lock();
        mProcShutdwn    = true;  // Exit processing thread
      mMtxProcessing.unlock();

      // wait until DataReader thread has gracefully ended
      pthread_join(tDRThread, NULL);
      
      // Exit streaming socket and empty PR cache
      mMtxStreaming.lock();
        mStrmShutdwn    = true;  // Exit streaming loop
      mMtxStreaming.unlock();
        
      // Meanwhile, empty DR Cache
      mMtxDRCache.lock();
      while(!mDRCache.empty()) 
      {
        mDRCache.pop();
      }
      mMtxDRCache.unlock();
      */
    } 
    catch(ilmsens::mvital::ces::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Controlling::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what() << std::endl
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(ilmsens::networking::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Controlling::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what() << std::endl
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(std::exception& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Controlling::Std-EXCEPTION: " << ANSI_COLOR_RESET 
                << tErr.what() << std::endl;
    }
    catch(...) 
    {
      std::cerr << ANSI_COLOR_RED << "Controlling::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
    }
    
    // close client's TCP connection
    tServer.closeClient();
  }

  // can go now, TCP server will be shutdown by destructor
  std::cout << ANSI_COLOR_CYAN << "Controlling: shutdown complete, exiting thread." << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace controlling
} //namespace ces
} //namespace mvital
} //namespace ilmsens
