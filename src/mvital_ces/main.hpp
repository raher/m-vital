/// @file main.hpp
/// @brief App definitions, declarations, and function prototypes.
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef MAIN_HPP
#define MAIN_HPP

// Logging & Debug
//#define LOG_LEVEL_DEFAULT  Poco::Message::Priority::PRIO_INFORMATION      ///< Default Poco log level of app
#define LOG_LEVEL_DEFAULT               3                                 ///< Default log level of app (similar to Poco: 0=Off, 3=error .. 7=debug .. 8=trace)

// sensor setup
#define CES_SENSOR_SETUP_TIMEOUT 10000                                  ///< Timeout while waiting for sensor setup by dataReader thread

// App configuration
#define CES_AUTO_START_MEAS false                                       ///< automatically start a measurement at application start?

// Measurement timing
#define CES_SW_AVG_DEFAULT  4                                           ///< default software averages
#define CES_WC_DEFAULT      0                                           ///< default wait cycles

// data processing constants
#define CES_DELAY_THRES_DEFAULT     86.5                                ///< max delay treshold for empty probe when no cow is measured
#define CES_ENERGY_THRES_DEFAULT    300.0                               ///< max energy treshold for empty probe when no cow is measured

#endif //MAIN_HPP
