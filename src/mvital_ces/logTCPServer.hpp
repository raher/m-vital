/// @file logTCPServer.hpp
/// @brief Main and support funtions for a log streaming TCP server thread.
///
/// The TCP server running in a separate thread will intercept console output (cout, cerr)
/// and send all messages to a connected client.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef LOGTCPSERVER_HPP
#define LOGTCPSERVER_HPP


/** TCP constants */
#define TCP_LOG_PORT            8200        ///< TCP port of logging connection
#define TCP_LOG_MSG_SIZE        4           ///< size of streaming dummy message (for checking client alive)
#define TCP_LOG_SEND_TO_MS      100         ///< TCP send timeout [ms]
#define TCP_LOG_WAIT_TO_MS      100         ///< TCP waiting timeout for select() [s]

#define TCP_LOG_STATUS_VEC_LEN  16          ///< length of status vector to be sent along with each measured frame

#define TCP_LOG_DEBUG_OUTPUT    1           ///< show debug info about streamed data 

/** Logging constants */
#define TCP_LOG_SEND_THRES      1024        ///< number of characters in the string that trigger sending them to client
#define TCP_LOG_TO_MS           200         ///< timeout for sending buffered messages anyways when no new logs are received


namespace ilmsens 
{
  namespace mvital
  {
    namespace ces 
    {
      namespace logging
      {

        /** Entry point for logging TCP server thread */
        void* logTCPServer(void*);

      } //namespace logging
    } //namespace ces
  } //namespace mvital
} //namespace ilmsens

#endif //LOGTCPSERVER_HPP
