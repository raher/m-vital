/* dataReader.cpp
 * 
 * Thread function handling Ilmsens m:liquid.ces sensors. Ilmsens HAL library is used
 * to interface with one sensor module (the first one found). Measured data is pushed
 * into a FIFO cache for further processing. Raw data is also stored on disk for further
 * offline analysis.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <cstring>
#include <ctime>

// String analysis
#include <regex>
#include <string>

#include <cstdio>

// Time
#include <chrono>                       // For execution time measurements

#if 0
  // Poco logging includes
  #include "Poco/Logger.h"
  #include "Poco/LogStream.h"
  #include "Poco/Message.h"
#endif

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// Ilmsens HAL library
#include "ilmsens/hal/ilmsens_hal.h"    // Ilmsens HAL library API

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// processing thread
#include "procIRFs.hpp" 

// main header for module
#include "dataReader.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Defines
 * -------------------------------------------------------------------------------------------------
 */

#define DR_SWAP_RX 0 // swap data of Rx1 and Rx2 for data processing?


namespace ilmsens 
{
namespace mvital
{
namespace ces 
{
namespace datareader
{


/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */

// convert int to uint without MSB loss
uint32_t int2uint32(TAddInfo pVal)
{
  union
  {
    TAddInfo uInt32;
    uint32_t uUInt32;
  };
  uInt32 = pVal;

  return(uUInt32);
}

/* -------------------------------------------------------------------------------------------------
 * DataReader thread
 * 
 * Setup attached sensor when signalled (a client has connected to command port), 
 * wait for measurement run signal and perform measurement. Forwards measured data
 * to processing thread (after amplitude scaling) via FIFO cache buffer. Stops measurement
 * upon stop signal. Shutdown of sensor/HAL when signalled (client ends the command connection).
 * 
 * -------------------------------------------------------------------------------------------------
 */

#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS 

  #pragma warning(push)
  #pragma warning(disable:4996)
#endif

void* dataReader(void*)
{
  try 
  {
    // bind thread to fixed CPU cores
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(5, &tCPUSet); // CPU #5
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "DataReader::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif

    /*
    // Set Realtime Priority
    struct sched_param tParam;
    sched_getparam(0, &tParam);
    tParam.sched_priority = 99; // Realtime
    
    if(sched_setscheduler(0, SCHED_RR, &tParam) != 0) 
    {
      // Fatal: no real-time priority could be set
      throw ilmsens::mvital::ces::Error("DataReader::ERROR: set scheduling priority to realtime failed!", ILMSENS_ERROR_NO_MEMORY, errno);
    }
    sched_getparam(0, &tParam);
    */

    std::cout << ANSI_COLOR_CYAN << "DataReader: thread created" << ANSI_COLOR_RESET << std::endl << std::endl;

    //use C++ wrapper around HAL library to ensure no memory leaks will occur 
    //in the thread and take care of proper HAL init & unloading
    std::cout << ANSI_COLOR_CYAN << "DataReader: initializing m:liquid.ces sensor(s)..." << ANSI_COLOR_RESET << std::endl;

    /* Create sensor group with only first device activated */
    ilmsens::hal::SensorIndexes tSenIdxs;
    tSenIdxs.push_back(1); //only use first sensor attached to the host

    /* Initialise HAL library and enumerate sensors */
    ilmsens::hal::Context tIlmsensHAL(HAL_LOG_LEVEL_INIT);

    /* Add the sensors to a group and activate them */
    ilmsens::hal::SensorGroup tSenGroup(tSenIdxs);

    /* Get and check sensor ID */
    char tID[ILMSENS_HAL_MOD_ID_BUF_SIZE]; //max length allocated
    int tHALRes = ilmsens_hal_getModId(tSenGroup.at(0), tID, sizeof(tID));
    if (tHALRes < ILMSENS_SUCCESS)
    { 
      throw ilmsens::hal::Error("DataReader::ERROR: getModId failed.", tHALRes);
    }
    if (strlen(tID) == 0) 
    {
      std::cout << ANSI_COLOR_RED << "DataReader::ERROR: Sensor-ID is empty!" << ANSI_COLOR_RESET << std::endl;
      throw ilmsens::hal::Error("DataReader::ERROR: Sensor-ID is empty!", ILMSENS_ERROR_IO);
    }
    else 
    {
      std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: sensor-ID is '" << tID << "'." << ANSI_COLOR_RESET << std::endl;
    }

    /* MLBS sensor properties */
    unsigned tMLBSOrder = DR_MLBS_ORDER;  // M-sequence order
    unsigned tOV        = DR_OV_DEF;      // default oversampling factor for m:vital sensors
    double   tFClk      = DR_MLBS_CLOCK;  // rf system clock f_0 [GHz]

    // analyse device ID to update MLBS order and clock
    std::string tIDStr(tID);
    std::regex  tParaRegExp("-(\\d{1,2})-(\\d+)[KMGkmg](\\d{2,})");
    std::smatch tMatch;
    if (std::regex_search(tIDStr, tMatch, tParaRegExp))
    {
      // analyse the pattern match for system parameters
      tMLBSOrder = (unsigned)std::stoi(tMatch[1]);
      std::string tClkStr = tMatch.str(2) + "." + tMatch.str(3);
      tFClk               = std::stod(tClkStr);
    }    


    /* prepare sensor configuration */
    const unsigned tMasterIdx = 1;                    // Index of master sensor: make first (and only) sensor to be master
    const unsigned tRxCnt = DR_MLBS_NUM_RX;           // Number of Rx-Channels per module

    // ask for SW averaging & debug setting (may be changed by TCP client)
    mMtxSharedData.lock();
      unsigned int tSWAvg = mState.mSW_AVG;
      unsigned int tWC    = mState.mWaitCyc;
      bool         tDebug = mState.mDebFlag;
    mMtxSharedData.unlock();

    /* configure the sensor(s) */
    if (tDebug) std::cout << ANSI_COLOR_CYAN << "DataReader::DEBUG: configuring sensor..." << ANSI_COLOR_RESET << std::endl;
    auto tResponseSize = ilmsens::hal::configure(tSenGroup, tMLBSOrder, tOV, tFClk, tRxCnt, tSWAvg, tMasterIdx);

    // get sensor properties after setup, e.g. for data processing
    ilmsens_hal_ModInfo tMInfo;
    tHALRes = ilmsens_hal_getModInfo (tSenGroup.at(0), &tMInfo);
    if (tHALRes != ILMSENS_SUCCESS)
    { 
      throw ilmsens::hal::Error("DataReader::ERROR: getModInfo failed.", tHALRes);
    }

    // update/calculate the sensor parameters and output them
    tMLBSOrder = tMInfo.mConfig.mOrder;     // MLBS Order
    tFClk      = tMInfo.mConfig.mClk * 1e9; // RF clock frequency [Hz]
    unsigned int   tMLBSLen = ((1 << tMLBSOrder) - 1) * tMInfo.mConfig.mOV;  // Length of M-Sequence in samples

    std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: configuration of sensor is :" << std::endl << std::endl; 
    std::cout << "* RF system clock    [GHz]: " << std::dec << std::setprecision(6) << tMInfo.mConfig.mClk << std::endl;
    std::cout << "* MLBS order              : " << std::dec << tMInfo.mConfig.mOrder << std::endl;
    std::cout << "* Prescaler           1/  : " << std::dec << tMInfo.mConfig.mSub << std::endl;
    std::cout << "* Oversampling        x   : " << std::dec << tMInfo.mConfig.mOV << std::endl;
    std::cout << "* Number of Tx            : " << std::dec << tMInfo.mConfig.mTx << std::endl;
    std::cout << "* Number of Rx            : " << std::dec << tMInfo.mConfig.mRx << std::endl;
    std::cout << "* Number of samples per Rx: " << std::dec << tMInfo.mNumSamp << std::endl;
    std::cout << "* MLBS-Length             : " << std::dec << tMLBSLen << std::endl;
    std::cout << "* Hardware averages       : " << std::dec << tMInfo.mHWAvg << std::endl;
    std::cout << "* Software avg. limits    : [" << std::dec << tMInfo.mAvgLim[0] << " .. " << tMInfo.mAvgLim[1] << "]" << std::endl;
    std::cout << "* Software averages       : " << std::dec << tMInfo.mAvg << std::endl;
    std::cout << "* Wait cycle limits       : [" << std::dec << tMInfo.mWaitLim[0] << " .. " << tMInfo.mWaitLim[1] << "]" << std::endl;
    std::cout << "* Wait cycles             : " << std::dec << tMInfo.mWait << std::endl;
    std::cout << "* ADC full scale range [V]: [" << std::dec << std::setprecision(6) << tMInfo.mFSR[0] << " .. " << tMInfo.mFSR[1] << "]" << std::endl;
    std::cout << "* ADC LSB voltage     [mV]: " << std::dec << std::setprecision(6) << tMInfo.mLSB_Volt*1000.0 << std::endl;
    std::cout << "* Int. sensor temp.   [" << '\370' << "C]: " << std::dec << std::setprecision(2) << tMInfo.mTemp << ANSI_COLOR_RESET << std::endl;

    double tSampClk  = (tFClk * (double)tMInfo.mConfig.mOV) + 1e6;
    double tMeasRate = tSampClk / (double) tMInfo.mConfig.mSub / (double) tMInfo.mNumSamp / (double) (tMInfo.mHWAvg + 1) / (double) (tSWAvg+tWC);


    // Make MLBS-Length & other device properties globally known, get current cow number
    mMtxSharedData.lock();
      // save sensor config
      mState.mMLBSLen   = tMLBSLen;
      strcpy_s(mState.mID, 1024, tID);
      mState.mModInfo   = tMInfo;

      mState.mMeasRate  = tMeasRate;
      mState.mSampClk   = tSampClk / 1e9;

      // clear processing ready & measurement detected flag
      mState.mProcRdy  = false;
      mState.mMeasFlag = false;
    mMtxSharedData.unlock();

    /* create memories and caches */
    TFIFOCacheEntry tNewData;

    tNewData.mRawDat.resize(tResponseSize);
    tNewData.mProcRx1.resize(tMLBSLen);
    tNewData.mProcRx2.resize(tMLBSLen);

    /* Create Processing-Thread (only needs to live when sensor has been activated) */
    pthread_t tProcThread;

    mMtxProcessing.lock();
      mProcShutdwn = false;
    mMtxProcessing.unlock();

    int tErg = pthread_create(&tProcThread, NULL, &ilmsens::mvital::ces::processing::procIRFs, 0); // processing thread

    if(tErg != 0) 
    {
      std::cout << ANSI_COLOR_RED << "Datareader::ERROR: cannot create processing thread!" << ANSI_COLOR_RESET << std::endl;
      throw ilmsens::mvital::ces::Error("DataReader::ERROR: cannot create processing thread!", ILMSENS_ERROR_NO_MEMORY, errno);
    }

    // init is complete, so output fewer log messages
    tIlmsensHAL.setDebugLevel(HAL_LOG_LEVEL_NORM);

    // get parts of sensor ID to be used for raw data storage file names
    std::string tIDDelim = "::";
    std::vector< std::string > tIDParts;

    std::string::size_type tSubStart = 0;
    auto tSubEnd   = tIDStr.find(tIDDelim);
    while (tSubEnd != std::string::npos)
    {
      tIDParts.push_back(tIDStr.substr(tSubStart, tSubEnd - tSubStart));
      tSubStart = tSubEnd + tIDDelim.length();
      tSubEnd = tIDStr.find(tIDDelim, tSubStart);
    }

    /* sensor & infra-structure has been activated */
    mMtxSharedData.lock();
      mState.mSenAct = true;
    mMtxSharedData.unlock();

    /* wait for initialising of processing thread */
    std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: waiting for processing thread to become ready ..." << ANSI_COLOR_RESET << std::endl;
    bool tProcRdy = false;
    bool tEndDR   = false;
    while (!tProcRdy && !tEndDR)
    {
      // get processing ready flag
      mMtxSharedData.lock();
        tProcRdy = mState.mProcRdy;
      mMtxSharedData.unlock();

      mMtxExit.lock();
        tEndDR = mExitShutdwn;
      mMtxExit.unlock();

       // sleep some time to decrease CPU burden
      delay_ms(10);
    }

    /* main loop of DataReader - wait for run flag and do measurement if requested */
    while(!tEndDR) 
    {
      // check, if measurement request has been received & update flags and config info
      mMtxSharedData.lock();
        bool tRunFlag     = mState.mRunFlag;

        tSWAvg            = mState.mSW_AVG;
        tWC               = mState.mWaitCyc;
      mMtxSharedData.unlock();

      /* new measurement run started? */
      if(tRunFlag) 
      {
        std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: starting new measurement ..." << ANSI_COLOR_RESET << std::endl;

        // set the newly retrieved software averages & wait cycles
        tSenGroup.setSWAvgerages(tSWAvg, tWC);
        std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: set SW averages to " << std::dec << tSWAvg << " and wait cycles to " << tWC << "." << ANSI_COLOR_RESET << std::endl;

        // get scaling of measured data as influenced by total averages
        double tAvgScale  = (double) tSWAvg * (double) tMInfo.mHWAvg;
        double tMeasScale = 1/tAvgScale * tMInfo.mLSB_Volt; 

        // calculate the new measurement rate & time between IRFs (only valid for implicit oversampling ECC sensors!)
        tMeasRate = tFClk * (double)tMInfo.mConfig.mOV / (double) tMInfo.mConfig.mSub / (double) tMInfo.mNumSamp / (double) (tMInfo.mHWAvg + 1) / (double) (tSWAvg+tWC);
        unsigned tTOMillis = 100 * (unsigned)(1000.0 / tMeasRate);
        std::chrono::milliseconds tTOVal(tTOMillis);

        std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: measurement rate will be " << std::dec << tMeasRate << " Hz (IRF timeout is set to " << tTOMillis << " ms)." << ANSI_COLOR_RESET << std::endl;

        // output current sensor configuration if debug output is enabled
        if (tDebug)
        {
          ilmsens_hal_ModInfo tSenInfo;
          tHALRes = ilmsens_hal_getModInfo (tSenGroup.at(0), &tSenInfo);
          if (tHALRes != ILMSENS_SUCCESS)
          { 
            throw ilmsens::hal::Error("DataReader::ERROR: getModInfo failed.", tHALRes);
          }
          std::cout << ANSI_COLOR_CYAN << "DataReader::DEBUG: configuration of sensor is :" << std::endl << std::endl; 
          std::cout << "* RF system clock    [GHz]: " << std::dec << std::setprecision(6) << tSenInfo.mConfig.mClk << std::endl;
          std::cout << "* MLBS order              : " << std::dec << tSenInfo.mConfig.mOrder << std::endl;
          std::cout << "* Prescaler           1/  : " << std::dec << tSenInfo.mConfig.mSub << std::endl;
          std::cout << "* Oversampling        x   : " << std::dec << tSenInfo.mConfig.mOV << std::endl;
          std::cout << "* Number of Tx            : " << std::dec << tSenInfo.mConfig.mTx << std::endl;
          std::cout << "* Number of Rx            : " << std::dec << tSenInfo.mConfig.mRx << std::endl;
          std::cout << "* Number of samples per Rx: " << std::dec << tSenInfo.mNumSamp << std::endl;
          std::cout << "* Hardware averages       : " << std::dec << tSenInfo.mHWAvg << std::endl;
          std::cout << "* Software avg. limits    : [" << std::dec << tSenInfo.mAvgLim[0] << " .. " << tSenInfo.mAvgLim[1] << "]" << std::endl;
          std::cout << "* Software averages       : " << std::dec << tSenInfo.mAvg << std::endl;
          std::cout << "* Wait cycle limits       : [" << std::dec << tSenInfo.mWaitLim[0] << " .. " << tSenInfo.mWaitLim[1] << "]" << std::endl;
          std::cout << "* Wait cycles             : " << std::dec << tSenInfo.mWait << std::endl;
          std::cout << "* Total averages          : " << std::dec << tAvgScale << std::endl;
          std::cout << "* ADC full scale range [V]: [" << std::dec << std::setprecision(6) << tSenInfo.mFSR[0] << " .. " << tSenInfo.mFSR[1] << "]" << std::endl;
          std::cout << "* ADC LSB voltage     [mV]: " << std::dec << std::setprecision(6) << tSenInfo.mLSB_Volt*1000.0 << std::endl;
          std::cout << "* Sample scaling factor   : " << std::dec << std::setprecision(6) << tMeasScale << std::endl;
          std::cout << "* Int. temperature    [" << '\370' << "C]: " << std::dec << std::setprecision(2) << tSenInfo.mTemp << ANSI_COLOR_RESET << std::endl;
        }

        /* create buffers for measurement */
        TRealIRF tBuf1(tMLBSLen, 0);
        TRealIRF tBuf2(tMLBSLen, 0);

        /* create & start new measurement */
        ilmsens::hal::Measurement tMeas(tSenGroup, tResponseSize, HAL_MEAS_MODE);

        // counting variables
        unsigned int tIdx    = 0;
        unsigned int tRunIdx = 0;
        unsigned int tLostWarnCnt = 10;

        // initialize execution-time-measurement
        auto tStart     = std::chrono::system_clock::now();
        auto tEnd       = tStart;

        // get initial state of flags and timing variables
        mMtxSharedData.lock();
          tRunFlag = mState.mRunFlag;
          tDebug   = mState.mDebFlag;

          // reset measurement flag, since run has just started
          bool   tMeasFlag = false;
          mState.mMeasFlag = tMeasFlag;

          // get number of IRFs before automatic stop of measurement (0 = do not stop automatically)
          unsigned int tNumIRF = mState.mNumIRF;
        mMtxSharedData.unlock();

        // show other configuration options
        std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: measurement configuration:" << std::endl
                                     << "--------------------------------------------" << std::endl
                  << "- Number of IRFs to measure (0=infinite): " << std::dec << tNumIRF << std::endl
                  << ANSI_COLOR_RESET << std::endl;

        /* main measurement loop */
        while(tRunFlag && !tEndDR) 
        {
          if (tDebug) std::cout << ANSI_COLOR_CYAN << "DataReader: reading dataset #" << std::dec << tRunIdx << " from sensor" << ANSI_COLOR_RESET << std::endl;

          // Get next IRF
          tNewData.mRawDat = tMeas.next(tTOVal);

          // Check for lost data
          unsigned int tIdxDiff = tNewData.mRawDat[tMLBSLen] - tIdx;
          if ((tIdxDiff > 1) && tLostWarnCnt)
          {
            std::cout << ANSI_COLOR_RED << "DataReader::WARNING: lost " << std::dec << tIdxDiff-1 << " IRFs" << ANSI_COLOR_RESET << std::endl;
            tLostWarnCnt --; //limit log outputs
          }
          else
          {
            if (tLostWarnCnt < 10) tLostWarnCnt++;
          }

          // Save new index
          tIdx = tNewData.mRawDat[tMLBSLen];

          // Copy the IRF to buffer (convert to double)
          for(unsigned int tK = 0; tK < tMLBSLen; tK++) 
          {
#if DR_SWAP_RX
            tBuf2[tK] = (double)(tNewData.mRawDat[tK]);
            tBuf1[tK] = (double)(tNewData.mRawDat[tK + tMLBSLen + 1]);
#else
            tBuf1[tK] = (double)(tNewData.mRawDat[tK]);
            tBuf2[tK] = (double)(tNewData.mRawDat[tK + tMLBSLen + 1]);
#endif
          }

          // Time Measurement for debugging
          if (tDebug)
          {
            tEnd = std::chrono::system_clock::now();
            std::chrono::duration<double> tElSec = tEnd - tStart;

            std::cout << ANSI_COLOR_CYAN << "Datareader::DEBUG: calc. current meas. freq.: " << std::dec << 1/tElSec.count() 
                      << " Hz | nominal meas. freq.: " << tMeasRate 
                      << " Hz" << ANSI_COLOR_RESET << std::endl;
            tStart = std::chrono::system_clock::now();
          }

          // Scale data according to averages and copy to cache entry
          // Also mind the maximum value for ADC level estimation
          double tRx1Max = -1.0;
          double tRx2Max = -1.0;
          double tRx1Cur;
          double tRx2Cur;
          for (unsigned int tJ = 0; tJ < tMLBSLen; tJ++) 
          {
            tRx1Cur = tNewData.mProcRx1[tJ] = tBuf1[tJ] * tMeasScale;
            tRx2Cur = tNewData.mProcRx2[tJ] = tBuf2[tJ] * tMeasScale;

            // Mind maximum value
            tRx1Cur = fabs(tRx1Cur);
            tRx2Cur = fabs(tRx2Cur);
            if (tRx1Cur > tRx1Max) tRx1Max = tRx1Cur;
            if (tRx2Cur > tRx2Max) tRx2Max = tRx2Cur;
          }

          /* Additional Information */
          tNewData.mIRFNum     = int2uint32(tNewData.mRawDat[tMLBSLen]);                // current IRF index
          tNewData.mADCLevel1  = (unsigned int) ceil(tRx1Max / tMInfo.mFSR[1] * 100.0); // ADC level Rx1 [%]
          tNewData.mADCLevel2  = (unsigned int) ceil(tRx2Max / tMInfo.mFSR[1] * 100.0); // ADC level Rx2 [%]
          tNewData.mSenTempVal = int2uint32(tNewData.mRawDat[tResponseSize-1]);         // Copy Temperatures (additional IRF info from Rx2 of sensor)
          tNewData.mModTemp    = DR_DEF_TEMP_INVALID;

          /* get/convert sensor temperatures from additional info sample of last Rx */
          unsigned int tTempRaw  = tNewData.mSenTempVal & DR_PTEMP_VAL_MASK; // lower 16 bit decode probe temperature raw value
          float        tTempVal1 = float(tTempRaw >> DR_PTEMP_VAL_SHIFT) / (float)DR_PTEMP_VAL_SCALE - (float)DR_PTEMP_VAL_OFF;

                       tTempRaw  = (tNewData.mSenTempVal >> 16) & DR_TEMP_VAL_MASK; // upper 16 bit decode sensor internal temperature raw value
          float        tTempVal2 = DR_DEF_TEMP_INVALID;
          if (tTempRaw & DR_TEMP_VAL_SIGN) 
          {
            tTempVal2 = (((float) tTempRaw) - (float)DR_TEMP_VAL_OFF) / (float)DR_TEMP_VAL_SCALE;
          } 
          else 
          {
            tTempVal2 = ((float) tTempRaw) / (float)DR_TEMP_VAL_SCALE;
          }

          // save updated sensor temperatures
          tNewData.mSenTemp    = tTempVal1;                    // Copy probe Temp.
          tNewData.mModTemp    = tTempVal2;                    // Copy module Temp.

          /* Store new entry in cache */
          mMtxDRCache.lock();
            tNewData.mCacheSize = (TAddInfo) mDRCache.size(); // current size of cache memory (DR cache)
            mDRCache.push(tNewData);                          // push data into DR cache
          mMtxDRCache.unlock();

          tRunIdx++; // counting finished cycle

          /* check for automatic stop of measurement */
          if (tNumIRF)
          {
            if (tRunIdx >= tNumIRF)
            {
              //stop measuring now
              mMtxSharedData.lock();
                mState.mRunFlag = false;
              mMtxSharedData.unlock();
            }
          }

          // --------- DEBUGGING -----------
          if (tDebug) 
          {
            mMtxDRCache.lock();
              size_t tDRCacheSize = mDRCache.size();
            mMtxDRCache.unlock();

            mMtxPRCache.lock();
              size_t tPRCacheSize = mPRCache.size();
            mMtxPRCache.unlock();

            std::cout << ANSI_COLOR_CYAN << "DataReader::DEBUG: DR-cache-size: " 
                      << std::dec << tDRCacheSize 
                      << ", PR-cache-size: " << tPRCacheSize
                      << ANSI_COLOR_RESET << std::endl;
          }
          //--------------------------------

          // update state information for measurement loop
          mMtxSharedData.lock();
            tRunFlag  = mState.mRunFlag;
            tDebug    = mState.mDebFlag;

            // reset measurement flag when run has ended
            if (!tRunFlag) mState.mMeasFlag = false;

            // update cow measurement detection flag
            tMeasFlag = mState.mMeasFlag;
          mMtxSharedData.unlock();

          // update overall exit flag
          mMtxExit.lock();
            tEndDR = mExitShutdwn;
          mMtxExit.unlock();
        } // main measurement loop

      } 
      else 
      {
        // sleep some time to wait for measurement request and decrease CPU burden
        delay_ms(10);
      }

      //update state information for DataReader
      mMtxExit.lock();
        tEndDR = mExitShutdwn;
      mMtxExit.unlock();
    } //end of main DataReader while loop

    std::cout << ANSI_COLOR_CYAN << "Datareader: preparing for shutdown ..." << ANSI_COLOR_RESET << std::endl;

    /* shut down porcessing thread */
    mMtxProcessing.lock();
      mProcShutdwn = true;
    mMtxProcessing.unlock();

    //give time for synchronisation
    std::cout << ANSI_COLOR_CYAN << "DataReader: Waiting for termination of processing-thread ..." << ANSI_COLOR_RESET << std::endl;
    delay_ms(10);

    pthread_join(tProcThread, NULL);    // Waiting for Processing-Thread

    // deinit will shortly start in context destructor, so output more log messages again
    tIlmsensHAL.setDebugLevel(HAL_LOG_LEVEL_DEIN);
  }
  catch(ilmsens::mvital::ces::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
  }
  catch(ilmsens::hal::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::HAL-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.status_code() << std::endl;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
  }

  /* sensor & infra-structure has been closed */
  mMtxSharedData.lock();
    mState.mSenAct = false;
  mMtxSharedData.unlock();

  // can go now
  std::cout << ANSI_COLOR_CYAN << "DataReader: shutdown complete, exiting thread." ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

#ifdef _MSC_VER
  #undef _CRT_SECURE_NO_WARNINGS 

  #pragma warning(pop)
#endif

} //namespace datareader
} //namespace ces
} //namespace mvital
} //namespace ilmsens
