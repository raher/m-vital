/// @file procIRFs.hpp
/// @brief Main and support funtions for a basic data processing thread.
///
/// The thread takes raw meaured data from a cache memory and pre-processes it.
/// MLBS-correlation will be done as well as other basic processing steps.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef PROCIRFS_HPP
#define PROCIRFS_HPP


/** Sensor-related processing constants */
#define PR_MLBS_ORDER         9         ///< M-Sequence order of m:liquid.ces sensor
#define PR_MLBS_CLOCK         3.619     ///< RF system clock of m:liquid.ces sensor [GHz]
#define PR_MLBS_NUM_RX        2         ///< number of Rx per sensor module

/** IRF processing constants */
#define PR_IP_FAK             4         ///< interpolation factor for pulse parameter extraction
#define PR_USE_CH             1         ///< Rx channel to use for vital data anylsis

#define PR_ROI_START           900      ///< ROI start sample in range
#define PR_ROI_STOP           1027      ///< ROI stop sample in range

#define PR_VD_IRF_NUM           35      ///< number of IRFs to collect between full runs of vital data calculation
#define PR_FWD_IRF_NUM          7       ///< 2x number of IRFs to collect between generations of vital data packets

/** processing buffer configuration */
#define PR_HIST_NUM_BR        1400      ///< number of IRFs in history buffer for extraction of breathing (should equal 20s)
#define PR_HIST_NUM_HR         350      ///< number of IRFs in history buffer for extraction of heartbeat (should equal 5s)

/** person detection constants */
#define PR_PD_ALPHA           0.001         ///< exp. BGR forgetting factor for person detection
#define PR_PD_NUM_PRE_AVG     100           ///< number of IRFs to average for static background estimation

#define PR_DelayMeasThres     86.5          ///< max delay treshold for person detection
#define PR_EnergyMeasThres    300.0         ///< min energy treshold for person detection
#define PR_PDHighCntThres     140           ///< number of positive indicators for detection of person
#define PR_PDLowCntThres       35           ///< number of negative indicators for detection of person
#define PR_ResetLowCntThres   140           ///< number of negative indicators for reset of person detection

#define PR_IdleLowCntThres    140           ///< number of negative indicators for absence of person

/** vital data processing constants */

// heart rate
#define PR_mMaxNumCorrTraces  (16 * PR_IP_FAK / 2)          ///< max. number of well-correlating slow time traces being used for heart rate estimation
#define PR_mPassbandRel       0.4                           ///< define windowing pass band width for STFT
#define PR_STFT_IP_FAK        4                             ///< interpolation factor to give STFT a fine frequency resolution

#define PR_mHRFreqL           0.8                           ///< minimum valid heart rate [Hz] -> 60 bpm
#define PR_mHRFreqU           2.0                           ///< maximum valid heart rate [Hz] -> 120 bpm
#define PR_mHRFreqMax         13.0                          ///< maximum heart rate [Hz] considered for harmonic search -> 780 bpm (up to 6th harmonic for max heart rate)

#define PR_HR_MIN_CORR_COEFF  0.9                           ///< minimum correlation coefficent of residual traces with teh one at breathing index 

#define PR_HR_HAPA_PCTL       0.5                           ///< percentile amplitude as threshold for HAPA
#define PR_HR_HAPA_TOL_INT    0.3                           ///< HAPA frequency interval tolerance
#define PR_HR_HAPA_TOL_DIST   0.1                           ///< HAPA frequency distance tolerance
#define PR_HR_HAPA_MAX_LEN    4                             ///< HAPA maximum path length (i.e. max. harmonic to look for)


// breathing rate
#define PR_SMOOTH_NUM_IRF     28                            ///< length of smoothing window for breathing trace extraction (should equal 0.4 s)

#define PR_mBRFreqL           0.10                          ///< minimum valid breathing rate [Hz] -> 6 bpm
#define PR_mBRFreqU           1.0                           ///< maximum valid breathing rate [Hz] -> 60 bpm
#define PR_mBRMaxVar          (0.4 * 0.4)                   ///< maximum variance of breathing trace when person is resting
#define PR_mBRMaxSwing        1.0                           ///< maximum p2p swing of breathing trace when person is resting

#define PR_mBRDefSwingProm    0.03                          ///< default peak prominence for peak detection of breathing trace

/** Data forwarding constants */
#define PR_MIN_UPDATE_WAIT_MS 1000      ///< minimum time interval [ms] between forwarding data to streaming process

/** Thread configuration constants */
#define PR_DEBUG_OUTPUT       1         ///< show debug info about processing operation


namespace ilmsens 
{
  namespace mvital
  {
    namespace ces 
    {
      namespace processing
      {

        /** Entry point for processing thread */
        void* procIRFs(void*);

      } //namespace processing
    } //namespace ces
  } //namespace mvital
} //namespace ilmsens

#endif //PROCIRFS_HPP
