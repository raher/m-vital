/* procIRFs.cpp
 * 
 * Thread function doing basic processing of raw M-sequence data retrieved from a cache buffer.
 * MLBS-correlation is performed by using cross-correlation method.
 * Processed data is then forwarded into another cache buffer.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// IO, and Strings
#include <iostream>
#include <iomanip>
#include <vector>
#include <deque>
#include <chrono>

#include <cstring>

// Math
#include <algorithm>                    // for max_element
#include <limits>                       // for max/min float values

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority


// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// Ilmsens processing library
#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/KKF_interp_ECC.hpp"   // Ilmsens MLBS-KKF class for ECC-sensors
#include "ilmsens/processing/vector_iteration_functions.hpp"
#include "ilmsens/processing/simple_math_functions.hpp"
#include "ilmsens/processing/TukeyWin.hpp"
#include "ilmsens/processing/FFT_interp.hpp"
#include "ilmsens/processing/Hapa.hpp"

#include "ilmsens/tools/indexed_sort.hpp"

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// mean header for module
#include "procIRFs.hpp" 

using namespace ilmsens::processing;

namespace ilmsens 
{
namespace mvital
{
namespace ces 
{
namespace processing
{

/* -------------------------------------------------------------------------------------------------
 * Processing types
 */

/** Derived types for data processing */
typedef TTimeDelay TPulseDelay;           // type representing delay times of IRF pulses

/** person detection state information */
typedef struct  stPDetState
                {
                  double        mUWBTimeStep;  // time step fast time (between UWB samples) [ns]
                  TPulseDelay   mDelayThres;   // max delay treshold
                  TSample       mEnergyThres;  // min energy treshold

                  size_t        mNumBGAvg;     // number of averages to take as initial static background
                  double        mAlpha;        // for getting factor of exp. BGR
                  TRealIRF      mBG;           // buffer for background estimate

                  size_t        mPDLowCnt;     // count neg. detection
                  size_t        mPDHighCnt;    // count pos. detection
                  bool          mPDetected;    // person detected?
                } TPDetState;

/** vital data state information */
typedef struct  stVitalState
                {
                  unsigned      mState;        // vital data detection state
                  unsigned      mBrIdx;        // index in history buffer of slow-time trace used for breathing analysis 
                  TSample       mBR;           // breathing rate [bpm]
                  TSample       mHR;           // heart beat rate [bpm]
                  TSample       mBRAmp;        // current breathing signal amplitude
                } TVitalState;

/** vital data processing information */
typedef struct  stVitalProcInfo
                {
                  double        mMeasRate;    // actual measurement rate (IRFs/s)
                  double        mTimeStep;    // time step [s] between IRFs
                  size_t        mBRNum;       // number of samples for breathing trace history
                  TSampleVec    mBRTimes;     // slow time vector according to breathing trace history
                  double        mBRFrqStep;   // frequency step of breathing trace history
                  TSampleVec    mBRFrqs;      // frequency vector corresponding to slow time breathing trace history
                  TSample       mBRSwingProm; // peak prominence value based on last detected signal swing of breathing signal

                  size_t        mHRNum;       // number of samples for heart beat history
                  TSampleVec    mHRTimes;     // slow time vector according to heart beat trace history
                  TSampleVec    mHRWin;       // windo function to be applied to HR trace before FFT

                  size_t        mSTFTNum;     // number of samples for heart beat trace FFT
                  double        mHRFrqStep;   // frequency step of heart rate trace history
                  TSampleVec    mHRFrqs;      // frequency vector corresponding to slow time heart beat trace spectrum
                  TSampleVec    mHapaFrqs;    // frequency vector used in HAPA algorithm

                  size_t        mHRStartIdx;  // starting index of valid heart rate frequencies
                  size_t        mHRStopIdx;   // stopping index of valid heart rate frequencies
                  size_t        mHRMaxIdx;    // index of maxmimum considered heart rate frequency when using harmonic path
                } TVitalProcInfo;

/** type of buffer for IRF history */
//typedef std::deque< TRealIRF > THistBuf; 
typedef std::vector< TRealIRF > THistBuf; 

/* -------------------------------------------------------------------------------------------------
 * Local processing defines
 */

// vital data state machine
#define VD_OFF      0                        // vital data detection not running (i.e. when no measurement is running or just starting)
#define VD_RDY      1                        // vital data detection running but idle
#define VD_DET      2                        // person in the chair detected, but no valid vital data available
#define VD_BR       3                        // person in the chair detected, breathing rate is valid (good breathing quality)
#define VD_HR       4                        // person in the chair detected, breathing and heart rate is valid (good heart beat quality)

/* -------------------------------------------------------------------------------------------------
 * Processing buffers and variables
 */

// ROI
static size_t sROIStart;                    // range sample where ROI starts after interpolation
static size_t sROIStop;                     // range sample where ROI stops after interpolation
static size_t sROINum;                      // number of samples in ROI 

// history buffer
static TRealIRF sHistEntry;                 // entry in history buffer

static THistBuf sHistRaw;                   // history of raw IRFs
static THistBuf sHistSmooth;                // smoothed history of IRFs
static THistBuf sHistRest;                  // residual between raw and smoothed IRFs

static unsigned sIRFCntUpd;                 // count received IRFs to decide when to send a vital data packet update
static unsigned sIRFCntVD;                  // count received IRFs to decide when to do actual vital data analysis

// tracked signal properties
static TSampleVec sSmoothVar;                // variance of smoothed ROI samples
static TSampleVec sRestMean;                 // mean of residual ROI samples
static TSampleVec sRestVar;                  // variance of residual ROI samples

static TSampleVec sROICoE;                   // centre of energy within ROI samples
static TSample    sROICoEMean;               // mean of CoE vector

// signal processing states
static TPDetState  sPDState;                // processing states of person detection
static TVitalState sVDState;                // processing states of vital data extraction

static stVitalProcInfo sVPInfo;             // vital data processing infos

/* -------------------------------------------------------------------------------------------------
 * State and buffer initializers
 */

// person detection states
void resetPDState(double pSampClk)
{
  sPDState.mUWBTimeStep = 1/pSampClk;

  sPDState.mNumBGAvg  = PR_PD_NUM_PRE_AVG;
  sPDState.mAlpha     = PR_PD_ALPHA;
  sPDState.mBG.clear();
  sPDState.mBG.resize(sROINum, 0.0);

  sPDState.mPDLowCnt  = 0;
  sPDState.mPDHighCnt = 0;
  sPDState.mPDetected = false;

  // processing constants
  mMtxSharedData.lock();
    sPDState.mDelayThres  = mState.mDelThres;
    sPDState.mEnergyThres = mState.mEnThres;
  mMtxSharedData.unlock();
}

// vital data state
void resetVitalState(void)
{
  sVDState.mState = VD_OFF;

  sVDState.mBrIdx = 0;

  sVDState.mBR = 0.0;
  sVDState.mHR = 0.0;

  sVDState.mBRAmp = 0.0;

  // also reset IRF counters
  sIRFCntUpd = 0;
  sIRFCntVD  = 0;
}

void resetHistBuf(void)
{
  //set size of historic buffers and generate empty entries
  sHistEntry.clear();
  sHistEntry.resize(PR_HIST_NUM_BR, 0.0);

  sHistRaw.clear();
  sHistRaw.resize(sROINum, sHistEntry);
  
  sHistSmooth.clear();
  sHistSmooth.resize(sROINum, sHistEntry);

  TRealIRF tHREntry(PR_HIST_NUM_HR, 0.0);

  sHistRest.clear();
  sHistRest.resize(sROINum, tHREntry);

  sSmoothVar.clear();
  sSmoothVar.resize(sROINum, 0.0);

  sRestMean.clear();
  sRestMean.resize(sROINum, 0.0);

  sRestVar.clear();
  sRestVar.resize(sROINum, 0.0);

  sROICoEMean = ((TSample)PR_ROI_START + (TSample)PR_ROI_STOP)/2.0;
  sROICoE.clear();
  sROICoE.resize(PR_HIST_NUM_BR, sROICoEMean);
}

// fill vital data processing info structure with pre-computed constants
void resetVPInfo(double pMeasRate)
{
  sVPInfo.mMeasRate = pMeasRate;
  sVPInfo.mTimeStep = 1/pMeasRate;

  sVPInfo.mBRNum        = PR_HIST_NUM_BR;       // number of samples for breathing trace history
  createGridVec(sVPInfo.mBRTimes, 0.0, sVPInfo.mTimeStep, sVPInfo.mBRNum);     // slow time vector according to breathing trace history

  sVPInfo.mBRFrqStep    = pMeasRate/(double)sVPInfo.mBRNum;
  createGridVec(sVPInfo.mBRFrqs, 0.0, sVPInfo.mBRFrqStep, sVPInfo.mBRNum);      // frequency vector corresponding to slow time breathing trace history
  sVPInfo.mBRSwingProm  = PR_mBRDefSwingProm; // peak prominence value based on last detected signal swing of breathing signal

  sVPInfo.mHRNum        = PR_HIST_NUM_HR;       // number of samples for heart beat history
  createGridVec(sVPInfo.mHRTimes, 0.0, sVPInfo.mTimeStep, sVPInfo.mHRNum);     // slow time vector according to heart beat trace history
  
  //make windows function
  TukeyWin tSTFTWin((unsigned int) sVPInfo.mHRNum, PR_mPassbandRel);
  sVPInfo.mHRWin        = tSTFTWin.getTukeyVec();       // window function to be applied to HR trace before FFT

  sVPInfo.mSTFTNum      = (size_t)getNextPow2_32((uint32_t)sVPInfo.mHRNum) * PR_STFT_IP_FAK;     // number of samples for heart beat trace FFT
  sVPInfo.mHRFrqStep    = pMeasRate/(double)sVPInfo.mSTFTNum; // frequency step of STFT spectrum
  createGridVec(sVPInfo.mHRFrqs, 0.0, sVPInfo.mHRFrqStep, sVPInfo.mSTFTNum);      // frequency vector corresponding to interpolated slow time heart beat trace spectrum

  sVPInfo.mHRStartIdx   = (size_t)floor(PR_mHRFreqL  /  sVPInfo.mHRFrqStep);  // starting index of valid heart rate frequencies
  sVPInfo.mHRStopIdx    = (size_t)ceil(PR_mHRFreqU   /  sVPInfo.mHRFrqStep);  // stopping index of valid heart rate frequencies
  sVPInfo.mHRMaxIdx     = (size_t)ceil(PR_mHRFreqMax /  sVPInfo.mHRFrqStep);  // index of maximum heart rate frequency for harmonic path

  TSampleVec tHapaFrqs(sVPInfo.mHRFrqs.begin(), sVPInfo.mHRFrqs.begin()+sVPInfo.mHRMaxIdx);
  sVPInfo.mHapaFrqs = tHapaFrqs;
}

/* -------------------------------------------------------------------------------------------------
 * Online data analysis algorithm(s)
 * 
 * Receives correlated IRFs and tries to extract vital data when a person is detected.
 * 
 * -------------------------------------------------------------------------------------------------
 */

// prototypes for separate parts of processing
void detectPerson(bool pDebug);
void calcVitalDataBR(bool pDebug);
void calcVitalDataHR(FFT_interp& pFFT, bool pDebug);
void sendVitalData(void);

// main processing flow for vital data extraction
void procVitalData (TRealIRF& pNewIRF, FFT_interp& pFFT, bool pDebug)
{
  /* add new IRF to history buffer and update smoothed versions and residual as well */

  //extract ROI from new IRF
  sHistEntry = TRealIRF(pNewIRF.begin()+sROIStart, pNewIRF.begin()+sROIStop);

  // calculate centre of energy in ROI and save it into history buffer
  TTimeDelay tCoE = getRlCoE(sHistEntry, (int) sROIStart);
  std::rotate(sROICoE.begin(), sROICoE.begin() + 1, sROICoE.end()); 
  sROICoEMean += tCoE - sROICoE.back(); // update mean ROI value
  sROICoE.back() = tCoE;

  for (size_t tI = 0; tI < sROINum; tI ++)
  {
    //get raw, smoothed, and residual vectors for current ROI sample
    TRealIRF& tCurRaw    = sHistRaw.at(tI);
    TRealIRF& tCurSmooth = sHistSmooth.at(tI);
    TRealIRF& tCurRest   = sHistRest.at(tI);

    //get new value for current ROI sample
    TSample tCurSamp = sHistEntry[tI];
    
    //get new smoothed value -> subtract oldest raw smoothing sample and add new raw sample
    TSample tSmoothVal = tCurSmooth.back() + (tCurSamp - tCurRaw.at(tCurRaw.size() - PR_SMOOTH_NUM_IRF))/((TSample)PR_SMOOTH_NUM_IRF);

    //get residual between raw and smoothed data
    TSample tRestVal = tCurSamp - tSmoothVal;

    //update tracked parameters for current ROI sample
    sSmoothVar[tI] += tSmoothVal*tSmoothVal - tCurSmooth[0]*tCurSmooth[0];
    sRestMean[tI]  += tRestVal - tCurRest[0];
    sRestVar[tI]   += tRestVal * tRestVal - tCurRest[0] * tCurRest[0];

    //remove oldest samples in buffers by rotation and replacing end with new values
    std::rotate(tCurRaw.begin(), tCurRaw.begin() + 1, tCurRaw.end()); 
    std::rotate(tCurSmooth.begin(), tCurSmooth.begin() + 1, tCurSmooth.end()); 
    std::rotate(tCurRest.begin(), tCurRest.begin() + 1, tCurRest.end()); 

    tCurRaw.back()    = tCurSamp;
    tCurSmooth.back() = tSmoothVal;
    tCurRest.back()   = tRestVal;
    
    /*
    tCurRaw.erase(tCurRaw.begin());
    tCurSmooth.erase(tCurSmooth.begin());
    tCurRest.erase(tCurRest.begin());

    //add newest samples to buffers
    tCurRaw.push_back(tCurSamp);
    tCurSmooth.push_back(tSmoothVal);
    tCurRest.push_back(tRestVal);
    */
  }


  /* perform person detection */
  detectPerson(pDebug);


  /* check, if enough IRFs have been collected to justify full vital sign detection processing */
  sIRFCntVD ++;
  if (sIRFCntVD >= PR_VD_IRF_NUM)
  {
    //time to run vital data processing ... breathing first
    calcVitalDataBR(pDebug);

    // analyse heart beat trace as well?
    if (sVDState.mState >= VD_BR)
    {
      // it makes sense to look for heart beat
      calcVitalDataHR(pFFT, pDebug);
    }

    //reset counter
    sIRFCntVD = 0;
  }

  /* check, if enough IRFs have been collected for generation of the next vital data update package */
  sIRFCntUpd += 2;
  if (sIRFCntUpd >= PR_FWD_IRF_NUM)
  {
    //update breathing amplitude in vital data state
    sVDState.mBRAmp = sROICoE.back() - sROICoEMean;

    //time to send a vital data update package
    sendVitalData();

    //decrease counter
    sIRFCntUpd -= PR_FWD_IRF_NUM;
  }
}

/* decide whether a person is in the chair and whether this person is not moving too much */
void detectPerson(bool pDebug)
{
  /* background removal */
  
  // pre-average some IRFs to get good BG estimate to begin with
  if (sPDState.mNumBGAvg)
  {
    // add current ROI to BG buffer
    addRlIRFs(sPDState.mBG, sPDState.mBG, sHistEntry);
    sPDState.mNumBGAvg --; // it counts

    // did we finish to accumulate the BG estimate?
    if (!sPDState.mNumBGAvg)
    {
      // BG estimation has finished, so take average
      scaleRlIRFs(sPDState.mBG, sPDState.mBG, 1/(double)PR_PD_NUM_PRE_AVG);
    }

    // set states to off
    sPDState.mPDetected = false;
    sVDState.mState     = VD_OFF;
  
    //nothing more to do
    return;
  }

  // do BGR 
  TRealIRF tBGR(sHistEntry.size(), 0.0);
  if (!sPDState.mPDetected)
  {
    // no person detected, do slow update of BG by exp. filtering
    expBGRRl(tBGR, sPDState.mBG, sHistEntry, sPDState.mAlpha);
  }
  else
  {
    // as long as a person is detected, just do static BGR
    subRlIRFs(tBGR, sHistEntry, sPDState.mBG);
  }

  /* person detection */

  // get CoE and energy of current BGR trace
  TSample tEn;
  TTimeDelay tCoE = getRlCoE(sHistEntry, (int) sROIStart, &tEn) * sPDState.mUWBTimeStep;

  if (sPDState.mPDetected)
  {
    //positive detection occurred
    if ((tCoE <= sPDState.mDelayThres) && (tEn >= sPDState.mEnergyThres))
    {
      // reduce negative indicators when positive indicators occur
      if (sPDState.mPDLowCnt)
      {
        sPDState.mPDLowCnt--;
      }
    }
    else
    {
      // count negative indicators
      sPDState.mPDLowCnt++;
    }

    // check neg. indicators to decide absence of person
    if (sPDState.mPDLowCnt >= PR_IdleLowCntThres)
    {
      // no person is detected any more
      sPDState.mPDetected = false;
      sPDState.mPDHighCnt = 0;
      sPDState.mPDLowCnt  = 0;

      if (pDebug)
      {
        std::cout << ANSI_COLOR_YELLOW << "detectPerson::INFO: absence of person detected!" << ANSI_COLOR_RESET << std::endl;
      }
    }
  }
  else
  {
    // try to detect a person according to delay and energy criteria
    if ((tCoE <= sPDState.mDelayThres) && (tEn >= sPDState.mEnergyThres))
    {
      // count this IRF as positive indicator
      sPDState.mPDHighCnt++;
    }
    else
    {
      // count this IRF as negative indicator, if positive indicators have been there
      if (sPDState.mPDHighCnt)
      {
        sPDState.mPDLowCnt++;
      }
    }

    // check pos/neg indicators to decide start of measurement
    if ((sPDState.mPDHighCnt >= PR_PDHighCntThres) && (sPDState.mPDLowCnt <= PR_PDLowCntThres))
    {
      /* more positive than negative indicators have occured, a person might have been detected  */
      sPDState.mPDetected = true;
      sPDState.mPDHighCnt = 0;
      sPDState.mPDLowCnt  = 0;

      if (pDebug)
      {
        std::cout << ANSI_COLOR_YELLOW << "detectPerson::INFO: person detected!" << ANSI_COLOR_RESET << std::endl;
      }
    }
    else
    {
      // reset positive counts, if too many negative counts have occured
      if (sPDState.mPDLowCnt >= PR_ResetLowCntThres)
      {
        // too many negative indicators, so start over
        sPDState.mPDHighCnt = 0;
        sPDState.mPDLowCnt  = 0;
      }
    }
  }

  //update vital data state, if needed
  if (sPDState.mPDetected)
  {
    //state must be at least DETECTED
    if (sVDState.mState < VD_DET)
    {
      sVDState.mState = VD_DET;
    }
  }
  else
  {
    //state must be ready (when initial static BG averaging has been finished)
    sVDState.mState = VD_RDY;
  }
} //detectPerson

/** vital data breathing extraction processing flow */
void calcVitalDataBR(bool /* pDebug */)
{
  //check current state, vital data processing only makes sense when a person has been detected
  if (sVDState.mState < VD_DET) return;

  /* analyse breathing trace */
  TIdxSample tBRVarMax = getRlMax(sSmoothVar);

  //get breathing trace from smoothed history buffer
  TRealIRF tBRSig = sHistSmooth.at(tBRVarMax.mIdx);
  //save index of breathing trace
  sVDState.mBrIdx = tBRVarMax.mIdx;

  //get partial breathing signal for swing analysis
  TRealIRF tBREnd(tBRSig.end()-sVPInfo.mHRNum, tBRSig.end());
  //get IRF parameters
  TIRFPara tBRPara = getRlIRFPara(tBREnd);
  TSample  tBRSwing = tBRPara.mMax.mVal - tBRPara.mMin.mVal;

  // do sanity check of breathing signal
  if ((tBRPara.mVar < PR_mBRMaxVar) && (tBRSwing < PR_mBRMaxSwing))
  {
    //breathing signal can be analysed for peaks
    unsigned int tPeakDist;
    double tBR;
    TIdxVec tBRPeaks = findPeaks(tBRSig, false, 0.0, 0.0, sVPInfo.mBRSwingProm, (size_t) ceil(sVPInfo.mMeasRate/2.0), 2);

    //go on with deciding how many peaks have been found
    switch (tBRPeaks.size())
    {
      // 2 peaks were found, can calculate breathing rate and later detect heart rate
      case 2:
        tPeakDist = tBRPeaks[1].mIdx - tBRPeaks[0].mIdx;
        tBR = sVPInfo.mMeasRate / (double)tPeakDist;
        //check boundaries
        if (tBR < PR_mBRFreqL) tBR = PR_mBRFreqL;
        if (tBR > PR_mBRFreqU) tBR = PR_mBRFreqU;

        sVDState.mBR    = tBR * 60.0; //convert to bpm
        sVDState.mState = VD_BR;
        break;

      // only one peak was found, so heart rate detection still makes sense
      case 1:
        //do not change breathing rate, but keep state for now
        sVDState.mState = VD_BR;
        break;
      
      // could not find any meaningful breathing peaks
      case 0:
        sVDState.mState = VD_DET;
        break;
    }
  }
  else
  {
    //breathing signal is too disturbed to be analysed -> also no heart rate analysis
    sVDState.mState = VD_DET;
  }

  //report the used breathing trace index
  return;
}

/** vital data breathing extraction processing flow */
void calcVitalDataHR(FFT_interp& pFFT, bool /* pDebug */)
{
  // first, get residual trace of breathing signal
  size_t tBRIdx = (size_t)sVDState.mBrIdx;
  TRealIRF tHRTrace = sHistRest.at(tBRIdx);

  //get centred version of it, i.e. remove mean and normalize variance to 1.0
  centerRlIRFs(tHRTrace, tHRTrace, sRestMean[tBRIdx], sqrt(sRestVar[tBRIdx]));

  // calculate correlation between residual traces with the residual of the breathing trace
  TSampleVec tCorr;
  TIndexVec tBestCorrIdxs;
  for (size_t tI = 0; tI < sHistRest.size(); tI++)
  {
    if (tI == tBRIdx)
    {
      // auto-corr is always best:-)
      tBestCorrIdxs.push_back(tI);
      tCorr.push_back(1.0);
    }
    else
    {
      TRealIRF tCurTrace = sHistRest.at(tI);
      centerRlIRFs(tCurTrace, tCurTrace, sRestMean[tI], sqrt(sRestVar[tI]));

      //calculate correlation at lag 0 only -> scalar product of vectors
      TSample tCorrCoeff = scalarProd(tHRTrace, tCurTrace);

      // correlation is good?
      if (abs(tCorrCoeff) >= PR_HR_MIN_CORR_COEFF)
      {
        // count this trace
        tBestCorrIdxs.push_back(tI);
        tCorr.push_back(tCorrCoeff);
      }
    }
  }

  // sort index vector according to decreasing correlation coefficient
  TSampleVec tSortedCorr;
  TIndexVec tSortIdxs;

  ilmsens::tools::indexed_sort< TSample > (tCorr, tSortedCorr, tSortIdxs);

  // accumulate best correlating traces and weight them with their correlation factor
  size_t tNumTraces = tBestCorrIdxs.size();
  if (tNumTraces > PR_mMaxNumCorrTraces) tNumTraces = PR_mMaxNumCorrTraces;

  TRealIRF tHRSig(tHRTrace.size(), 0.0);
  for (size_t tI = 0; tI < tNumTraces; tI ++)
  {
    TRealIRF tWeightedTrace;
    scaleRlIRFs(tWeightedTrace, sHistRest.at(tBestCorrIdxs[tSortIdxs[tI]]), tSortedCorr[tI]);

    // accumulate weighted traces
    addRlIRFs(tHRSig, tHRSig, tWeightedTrace);
  }

  // apply window function to mean-free HR signal
  TSample tHRMean = getRlMean(tHRSig);
  subRlIRFs(tHRSig, tHRSig, tHRMean);
  mulRlIRFs(tHRSig, tHRSig, sVPInfo.mHRWin);

  //apply FFT with interpolation to HR trace
  const fftw_complex* tHRSpec = pFFT.getInterpolatedFRF(tHRSig);
  //get absolut
  TRealIRF tHRSpecAbs(sVPInfo.mHRMaxIdx, 0.0);
  for (size_t tI = sVPInfo.mHRStartIdx; tI < sVPInfo.mHRMaxIdx; tI ++)
  {
    double tRe = tHRSpec[tI][0];
    double tIm = tHRSpec[tI][1];
    tHRSpecAbs[tI] = tRe*tRe + tIm*tIm;
  }

  // get percentile level
  TSample tPctl = getRlIRFPctl(tHRSpecAbs, PR_HR_HAPA_PCTL);

  // use HAPA to find heart rate
  Hapa tHAPA(tHRSpecAbs, sVPInfo.mHapaFrqs, PR_mHRFreqL, PR_mHRFreqU, tPctl, PR_HR_HAPA_TOL_INT, PR_HR_HAPA_TOL_DIST, PR_HR_HAPA_MAX_LEN);
  double tHeartRate = tHAPA.getHeartRate();

  //store heart rate and change state if it seems valid
  sVDState.mHR = tHeartRate * 60.0; //convert to bpm
  if ((tHeartRate >= PR_mHRFreqL) && (tHeartRate <= PR_mHRFreqU))
  {
    //seems valid
    sVDState.mState = VD_HR;
  }


}


/** create and send vital data packet to GUI client */
void sendVitalData(void)
{
  TVitalPacket tVP;

  //put the current state of vital data processing into packet
  tVP.mState = (uint32_t)sVDState.mState;

  //put the breathing and heart rate into the packet
  tVP.mBR_100 = static_cast<int32_t>(sVDState.mBR * 100.0);
  tVP.mHR_100 = static_cast<int32_t>(sVDState.mHR * 100.0);

  //add amplitude of current breathing signal into packet
  tVP.mBRAmp_100 = static_cast<int32_t>(sVDState.mBRAmp * 1000.0); //keep one more digit of the FP value

  // finally send the packet to streaming thread
  mMtxVDCache.lock();
    mVDCache.push(tVP);
  mMtxVDCache.unlock();
}


/* -------------------------------------------------------------------------------------------------
 * Processing thread
 * 
 * Receives data during running measurement from DataReader and process it according to requested
 * processing flags from client. The thread is created and shutdown by the DataReader.
 * 
 * By default, MLBS correlation by KKF is performed. 
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* procIRFs(void*)
{
  try 
  {
    std::cout << ANSI_COLOR_YELLOW << "Processing: thread created" << ANSI_COLOR_RESET << std::endl;

    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(7, &tCPUSet); // CPU #7
//    CPU_SET(8, &tCPUSet); // CPU #8
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Processing::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif

    /* Get required configuration information for processing */
    mMtxSharedData.lock();
      unsigned int tMLBSLen = mState.mMLBSLen;
      bool tDebug           = mState.mDebFlag;
      ilmsens_hal_ModInfo tModInfo = mState.mModInfo;
      double tMeasRate      = mState.mMeasRate;
      double tSampClk       = mState.mSampClk;
    mMtxSharedData.unlock();

    // mind number of samples before interpolation
    double tNumSamp = (double) tMLBSLen;

    // apply 2x interpolation
    tMLBSLen *= PR_IP_FAK;
    //round it up t0 next power of 2
    tMLBSLen = (unsigned int) ilmsens::processing::getNextPow2_32((uint32_t) tMLBSLen);

    // mind number of samples after interpolation and get final ROI definition
    double tNumSampIP = (double) tMLBSLen;

    //update (virtual) sampling clock according to interpolation
    tSampClk *= tNumSampIP / tNumSamp;

    sROIStart = static_cast<size_t>(floor(tNumSampIP / tNumSamp * PR_ROI_START));
    sROIStop  = static_cast<size_t>(ceil (tNumSampIP / tNumSamp * PR_ROI_STOP));
    sROINum   = sROIStop - sROIStart + 1;

    std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: ROI before interpolation (" << (unsigned)tNumSamp << " | " << PR_ROI_START << " | " << PR_ROI_STOP << ") will be (" 
                                   <<  (unsigned)tNumSampIP << " | " << sROIStart << " | " << sROIStop << " | " << sROINum << ") with interpolation." << ANSI_COLOR_RESET << std::endl;

    /* Acquire pre-processing ressources as needed */
    KKF_interp_ECC myKKF(tModInfo.mConfig.mOrder, tModInfo.mConfig.mOV, tMLBSLen, true);
    std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: KKF handler created with MLBs order " << tModInfo.mConfig.mOrder 
                                   << " | oversampling factor " << tModInfo.mConfig.mOV 
                                   << " | IRF length (incl. interpolation) " << tMLBSLen << ANSI_COLOR_RESET << std::endl;

    // additional vectors/buffers
    TFIFOCacheEntry tRawDat;
    TFIFOCacheEntry tProcData;

    tProcData.mProcRx1.resize(tMLBSLen);
    tProcData.mProcRx2.resize(tMLBSLen);

    // buffers for correlation results (including interpolation)
    TRealIRF tIRF1(tMLBSLen, 0);
    TRealIRF tIRF2(tMLBSLen, 0);

    /* prepare processing states  and buffers*/
    resetPDState(tSampClk);
    resetVitalState();
    resetHistBuf();
    resetVPInfo(tMeasRate);

    /* STFT ressources */
    FFT_interp tSTFT((unsigned int)sVPInfo.mHRNum, (unsigned int)sVPInfo.mSTFTNum);
    std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: FFT handler created with IRF length " << sVPInfo.mHRNum 
                                   << " | FRF length (incl. interpolation) " << sVPInfo.mSTFTNum << ANSI_COLOR_RESET << std::endl;

    // output current processing constants
    std::cout << ANSI_COLOR_YELLOW
              << "Processing::INFO: data processing constants ..." << std::endl
              << std::endl
              << "Person detection delay threshold    : " << std::setprecision(6) << sPDState.mDelayThres << std::endl
              << "Person detection energy threshold   : " << std::setprecision(6) << sPDState.mEnergyThres << std::endl
              << ANSI_COLOR_RESET << std::endl;

    /* main loop is executed until shutdown flag is received */

    // initialize execution-time-measurement
    auto tStart = std::chrono::steady_clock::now();
    auto tEnd   = std::chrono::steady_clock::now();

    // set processing ready flag and get further processing flags
    mMtxSharedData.lock();
      mState.mProcRdy = true;

#if 0
      // output current processing constants
      std::cout << ANSI_COLOR_YELLOW
                << "Processing::INFO: data processing constants ..." << std::endl
                << std::endl
//                << "Measurement delay threshold    : " << sMDState.mDelayThres << std::endl
//                << "Measurement energy threshold   : " << std::setprecision(6) << sMDState.mEnergyThres << std::endl
                << ANSI_COLOR_RESET << std::endl;
#endif
    mMtxSharedData.unlock();

    // get state of processing
    std::cout << ANSI_COLOR_YELLOW << "Processing: waiting for data to process ..." << ANSI_COLOR_RESET << std::endl;
    mMtxProcessing.lock();
      bool tEndProc = mProcShutdwn;
    mMtxProcessing.unlock();
    while(!tEndProc) 
    {
      // Check if new data is available in DataReader cache
      mMtxDRCache.lock();
        bool tDRCEmpty = mDRCache.empty();
        
      if (!tDRCEmpty)
      {
        /* new data is there, so get & process it */

        // pop data from cache
          tRawDat = mDRCache.front(); // Get new Data
          mDRCache.pop();             // Delete new Data in Cache
        mMtxDRCache.unlock();

        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: received new data ..." << ANSI_COLOR_RESET << std::endl;
        auto tStartProc = std::chrono::steady_clock::now();

        /* do MLBS correlation */
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting KKF ..." << ANSI_COLOR_RESET << std::endl;
        tIRF2 = myKKF.calculateECC_KKF(tRawDat.mProcRx2);
        tIRF1 = myKKF.calculateECC_KKF(tRawDat.mProcRx1);
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: KKF done ..." << ANSI_COLOR_RESET << std::endl;
        auto tElKKF = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

        /* extract pulse parameters for time-zero and energy analysis */       
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting raw pulse analysis ..." << ANSI_COLOR_RESET << std::endl;

#if PR_USE_CH == 1
        TRealIRF& tUseIRF = tIRF1;
#else
        TRealIRF& tUseIRF = tIRF2;
#endif
        // analyse IRF parameters
        if (tDebug)
        {
          TIRFPara tVoltPara = getRlIRFPara(tUseIRF); 
          std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: pulse parameters Rx1:" << std::endl 
                    << "total energy  : " << std::dec << tVoltPara.mEn << std::endl
                    << "max. val | idx: " << std::dec << tVoltPara.mMax.mVal << " | " << tVoltPara.mMax.mIdx << std::endl
                    << "min. val | idx: " << std::dec << tVoltPara.mMin.mVal << " | " << tVoltPara.mMin.mIdx << std::endl
                    << "Processing::INFO: temperatures (probe/module/add.): " << std::dec << tProcData.mSenTemp << "°C | " << tProcData.mModTemp << "°C | " << tProcData.mAddTemp << "°C" << std::endl 
                    << ANSI_COLOR_RESET << std::endl;
        }
        auto tElPara = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);


        /* --- analyse IRF(s) to find vital data --- */
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting vital data analysis ..." << ANSI_COLOR_RESET << std::endl;
        procVitalData (tUseIRF, tSTFT, tDebug);
        auto tElProc = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

        /* output timing info */
        if (tDebug)
        {
          std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: durations of different processing stages: " << std::endl
                    << "KKF             : " << std::dec << tElKKF.count() << " us" << std::endl
                    << "Pulse parameter : " << std::dec << tElPara.count() << " us" << std::endl
                    << "Vital processing: " << std::dec << tElProc.count() << " us" << std::endl
                    << ANSI_COLOR_RESET << std::endl;
        }


        /* forward new data to streaming, if suitable time interval has elapsed */

        // get time elpased since last data retrieval
        tEnd = std::chrono::steady_clock::now();
        auto tElapsed = std::chrono::duration_cast< std::chrono::milliseconds > (tEnd - tStart);
        if (tElapsed.count() >= PR_MIN_UPDATE_WAIT_MS)
        {
          // enough time has passed, so forward data to streaming process
          tStart = tEnd;

          // copy state and additional info from DataReader
          tProcData.mIRFNum     = tRawDat.mIRFNum;      // Copy index
          tProcData.mADCLevel1  = tRawDat.mADCLevel1;   // Copy ADC Level
          tProcData.mADCLevel2  = tRawDat.mADCLevel2;   // Copy ADC Level
          tProcData.mSenTempVal = tRawDat.mSenTempVal;  // Copy Temp. (additional IRF info from Rx2 of sensor)
          tProcData.mAddMaxTemp = tRawDat.mAddMaxTemp;  // max. Temp. of XU4
          tProcData.mAddTemp    = tRawDat.mAddTemp;     // Copy XU4 Temp.
          tProcData.mSenTemp    = tRawDat.mSenTemp;     // probe temperature
          tProcData.mModTemp    = tRawDat.mModTemp;     // module electronic's temperature

          // copy correlated and processed data to cache entry
          tProcData.mProcRx1 = tIRF1;
          tProcData.mProcRx2 = tIRF2;

          /* Store data in streaming cache */
          mMtxPRCache.lock();
            tProcData.mCacheSize = (TAddInfo) mPRCache.size();
            mPRCache.push(tProcData);
          mMtxPRCache.unlock();
        }
      }
      else
      {
        // unlock DR cache
        mMtxDRCache.unlock();

        // wait some time to lower CPU burden
        delay_ms(1);

        // Update processing state - only when cache is empty.
        // This should ensure, that all received data is being processed, before the thread is closed
        mMtxProcessing.lock();
          tEndProc = mProcShutdwn;
        mMtxProcessing.unlock();
      }

      // Update debugging and run state
      mMtxSharedData.lock();
        tDebug   = mState.mDebFlag;
      mMtxSharedData.unlock();
    }

    //give time for synchronisation
    delay_ms(1);
  } 
  catch(ilmsens::mvital::ces::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
  }
  catch(ilmsens::processing::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::Prc-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
  }
  
#if 0
  // cleanup history buffers
  sHistBuf.clear();
#endif
  
  // can go now
  std::cout << ANSI_COLOR_YELLOW << "Processing: shutdown complete, exiting thread" << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace processing
} //namespace ces
} //namespace mvital
} //namespace ilmsens
