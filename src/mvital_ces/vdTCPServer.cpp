/* vdTCPServer.cpp
 * 
 * Thread function for TCP server which will accept a single client connection
 * and subsequently forward vital data packets to the client. When no connection is made, data is 
 * discarded. The client may send simple 1-byte command for starting/stopping of the packet stream
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <cstring>

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
//#include <sched.h>                      // for Realtime Priority

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP stuff & server class
#ifndef WIN32
  #include <arpa/inet.h> // for htonl() call
#endif
#include "ilmsens/networking/TCP_server_single.hpp"
#include "ilmsens/networking/ilmsens_networking_defn.h"
#include "ilmsens/networking/ilmsens_networking_error.hpp"

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// main header for module
#include "vdTCPServer.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Using clauses
 * -------------------------------------------------------------------------------------------------
 */

using namespace ilmsens::networking;


/* -------------------------------------------------------------------------------------------------
 * Local defines
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */


namespace ilmsens 
{
namespace mvital
{
namespace ces 
{
namespace streaming
{


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

/*
// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mvital.ces.ctrl");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}
*/


/* -------------------------------------------------------------------------------------------------
 * Vital data packet streaming thread
 * 
 * Creates TCP server socket for streaming vital data packets to a client.
 * 
 * Accesses a cache shared with processing thread. 
 * 
 * If no client is connected, the data is just discarded. If a client is connected, data
 * is forwarded to the client which can send simple one-byte commands to start/stop the packet stream.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* vdTCPServer(void*)
{
  bool tStrmExit = false;

  bool tDoStream = VD_SEND_STREAM_DEF; 

  unsigned int tStrmPort = VD_TCP_STREAM_PORT;
  TCP_server_single tServer;

  try
  {
    std::cout << ANSI_COLOR_BLUE << "VitalDataServer: Thread created." << ANSI_COLOR_RESET << std::endl;
    
    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(2, &tCPUSet); // CPU #2
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Streaming::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif
    
    /* start server (listening socket) */
    tServer.startServer(tStrmPort, VD_TCP_STR_SEND_TO_MS);
  }
  catch(ilmsens::mvital::ces::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "VitalDataServer::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(ilmsens::networking::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "VitalDataServer::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "VitalDataServer::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "VitalDataServer::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tStrmExit = true;
  }

  // streaming thread is always running (always waiting for client)
  // until the EXIT command has been received from global states or client
  while(!tStrmExit) 
  {
    try 
    {
      // give time for synchronisation after a client disconnected
      delay_ms(10);

      std::cout << ANSI_COLOR_BLUE << "VitalDataServer: Waiting for Client at port " << std::dec << tStrmPort << " ..." << ANSI_COLOR_RESET << std::endl;

      // accept client connection with timeout
      while (!tStrmExit)
      {
        bool tClient = tServer.acceptClient(VD_TCP_STR_WAIT_TO_MS);
        
        if (tClient)
        {
          // a new client was accepted
          std::cout << ANSI_COLOR_BLUE << "VitalDataServer::INFO: connection initiated with client " << tServer.getClientIP() << " ..." << ANSI_COLOR_RESET << std::endl;
          break;  
        }
        else
        {
          // no client was connected, so update the exit flag
          mMtxExit.lock();
            tStrmExit = mExitShutdwn;
          mMtxExit.unlock();
          
          // try to drain PR cache, if no client is there or the previous one was disconnected
          mMtxPRCache.lock();
          while(!mPRCache.empty()) 
          {
            mPRCache.pop();
          }
          mMtxPRCache.unlock();
        }
      } // waiting for client
      
      // a client is here?
      if (!tStrmExit)
      {
        /* DATA TRANSFER */

        // count packages streamed
        unsigned int tIdx = 0;
        
        /* Create data tansfer variables */
        TVitalPacket tTXData;
        
        // output debugging messages?
        unsigned int tDebug = VD_TCP_STR_DEBUG_OUTPUT;

        mMtxSharedData.lock();
          tDebug        = mState.mDebFlag;
        mMtxSharedData.unlock();

        //send incomming data to client as long as the connection is alive
        while(!tStrmExit)
        {
          // Waiting for new data in PR cache
          mMtxVDCache.lock();
            bool tEmpty = mVDCache.empty();
          
          if (!tEmpty)
          {
            // new data has arrived from processing thread, so copy and forward it
              tTXData = mVDCache.front();
              mVDCache.pop();
            mMtxVDCache.unlock();

            if (tDoStream)
            {
              //add simple XOR CRC-8 to frame
              uint8_t *tBuf = (uint8_t *)&tTXData;
              uint8_t tCRC = 0;
              for (unsigned int tB = 0; tB < sizeof(TVitalPacket)-1; tB ++)
              {
                tCRC ^= tBuf[tB];
              }
              tTXData.mCRC = tCRC; 

              // calculate transmit sizes
              unsigned int tTxSize = sizeof(TVitalPacket); // CRC8 byte will be included!
              unsigned int tTxLen = 0;

              if (tDebug)
              {
                std::cout << ANSI_COLOR_BLUE << "VitalDataServer::INFO: sending vital data packet #" << std::dec << tIdx << " of size " << tTxSize<< " bytes ..." << ANSI_COLOR_RESET << std::endl;
              }

              bool tExitStream = false;
              while ( tTxLen < tTxSize )
              {
                int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tTxSize - tTxLen);
                if(tLen < 0) 
                {
                  // client connection has been lost
                  tExitStream = true;
                  break;
                }
                else
                {
                  // count the bytes sent
                  tTxLen += tLen;
                }
              }
              if (tExitStream) break;

              // package has been sent, so it counts
              tIdx++;
            }
          }
          else
          {
            // cache was empty
            mMtxVDCache.unlock();
            
            // test, if connection is still alive...do non-blocking read with short timeout
            uint8_t tCmdBuf[VD_TCP_STR_CMD_SIZE];
            int tLen = tServer.receiveTO((void *)tCmdBuf, VD_TCP_STR_CMD_SIZE, 1);
            if (tLen < 0)
            {
              // client connection has been closed,
              // need to stop streaming TCP connection
              tDoStream = false; // automatically stop stream when client is disconnected
              break;
            }
            else if (tLen == VD_TCP_STR_CMD_SIZE)
            {
              //client sent us a command?
              switch(tCmdBuf[0])
              {
                // Disable streaming of vital info frames
                case VD_TCP_CMD_STOP :

                    std::cout << ANSI_COLOR_BLUE << "VitalDataServer::INFO: Received STREAM-STOP-Command" << ANSI_COLOR_RESET << std::endl;
                    tDoStream = false;
                    break;

                // Enable streaming of vital info frames
                case VD_TCP_CMD_START :

                    std::cout << ANSI_COLOR_BLUE << "VitalDataServer::INFO: Received STREAM-START-Command" << ANSI_COLOR_RESET << std::endl;
                    tDoStream = true;
                    break;

                // Exit TCP server and close app
                case VD_TCP_CMD_EXIT :

                    std::cout << ANSI_COLOR_BLUE << "VitalDataServer::INFO: Received EXIT-Command" << ANSI_COLOR_RESET << std::endl;
                    mMtxExit.lock();
                      mExitShutdwn = true;
                    mMtxExit.unlock();
                    break;

                default:
                    std::cout << ANSI_COLOR_BLUE << "VitalDataServer::WARNING: Unknown command: 0x" << std::hex << std::setfill('0') << std::setw(8) << tCmdBuf[0] << std::setfill(' ') << ", please try again..." << ANSI_COLOR_RESET << std::endl;
                    break;
              }
            }
            else
            {
              // nothing happened, so wait some time to lower CPU burden
              delay_ms(10);
            }
          }

          // update configuration and shutdown flags 
          mMtxSharedData.lock();
            tDebug    = mState.mDebFlag;
          mMtxSharedData.unlock();
          mMtxExit.lock();
            tStrmExit = mExitShutdwn;
          mMtxExit.unlock();
        }
      }

      if (tStrmExit)
      {
        std::cout << ANSI_COLOR_BLUE << "VitalDataServer: received EXIT command, ending thread ..." << ANSI_COLOR_RESET << std::endl;
      }
      else
      {
        std::cout << ANSI_COLOR_BLUE << "VitalDataServer: no or bad client connection on streaming socket ..." << ANSI_COLOR_RESET << std::endl;
      }
    } 
    catch(ilmsens::mvital::ces::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "VitalDataServer::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what() << std::endl
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(ilmsens::networking::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "VitalDataServer::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what() << std::endl
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(std::exception& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "VitalDataServer::Std-EXCEPTION: " << ANSI_COLOR_RESET 
                << tErr.what() << std::endl;
    }
    catch(...) 
    {
      std::cerr << ANSI_COLOR_RED << "VitalDataServer::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
    }
    
    // close client's TCP connection
    tServer.closeClient();
  }

  // can go now, TCP server will be shutdown by destructor
  std::cout << ANSI_COLOR_BLUE << "VitalDataServer: shutdown complete, exiting thread." << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace streaming
} //namespace ces
} //namespace mvital
} //namespace ilmsens
