/// @file data_cache_FIFO.hpp
/// @brief Definition of buffers and structures for measured data.
///
/// The types defined here can be used for data exchange between functions and threads.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef PROCESSING_BUFFERS_HPP
#define PROCESSING_BUFFERS_HPP

#include <queue>
#include <vector>

#include "hal_wrap.hpp"
#include "ilmsens/processing/IRF_FRF.hpp"

using namespace ilmsens::processing::types;

/**   
 * @defgroup processing_cache_type Cache memories for exchange between threads.
 * @brief Types for exchanging measured data along with additional information.
 * @{
 */  


/** Cache FIFO entry structure */
struct sFIFOCacheEntry
{
  ilmsens::hal::ImpulseResponse mRawDat;  ///< raw data from sensor/HAL
  TRealIRF mProcRx1;        ///< processed IRF of Rx1
  TRealIRF mProcRx2;        ///< processed IRF of Rx2
  
  TAddInfo mIRFNum;         ///< number/index of IRF in current run (info attached to Rx1 data from sensor)

  TAddInfo mADCLevel1;      ///< ADC Level Rx1 [%]
  TAddInfo mADCLevel2;      ///< ADC Level Rx2 [%]
  TAddInfo mSenTempVal;     ///< Temperature info from sensor (info attached to Rx2 data)
  TAddInfo mAddMaxTemp;     ///< max. (CPU-) Temperature of host
  TAddInfo mCacheSize;      ///< current FIFO cache size

  TSampleSmall mSenTemp;    ///< probe temperature [°C]
  TSampleSmall mModTemp;    ///< sensor electronics temperature  [°C]
  TSampleSmall mAddTemp;    ///< additional (i.e. host) temperature  [°C]
};

/** Cache FIFO entry type */
typedef struct sFIFOCacheEntry TFIFOCacheEntry; 

/** Cache FIFO type */
typedef std::queue<TFIFOCacheEntry> TFIFOCache;

/** @} processing_cache_type */

#endif //PROCESSING_BUFFERS_HPP
