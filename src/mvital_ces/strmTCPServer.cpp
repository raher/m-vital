/* strmTCPServer.cpp
 * 
 * Thread function for TCP server which will accept a single client connection
 * and subsequently forward measured data to the client. When no connection is made, data is 
 * discarded.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <cstring>

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP stuff & server class
#ifndef WIN32
  #include <arpa/inet.h> // for htonl() call
#endif
#include "ilmsens/networking/TCP_server_single.hpp"
#include "ilmsens/networking/ilmsens_networking_defn.h"
#include "ilmsens/networking/ilmsens_networking_error.hpp"

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// main header for module
#include "strmTCPServer.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Using clauses
 * -------------------------------------------------------------------------------------------------
 */

using namespace ilmsens::networking;


/* -------------------------------------------------------------------------------------------------
 * Local defines
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */

uint32_t float2uint32(float pVal)
{
  union
  {
    float uFloat;
    uint32_t uUInt32;
  };
  uFloat = pVal;

  return(uUInt32);
}


namespace ilmsens 
{
namespace mvital
{
namespace ces 
{
namespace streaming
{


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

/*
// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mvital.ces.ctrl");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}
*/


/* -------------------------------------------------------------------------------------------------
 * Streaming thread
 * 
 * Creates TCP server socket for streaming processed data to client.
 * 
 * Accesses a cache shared with processing thread. 
 * 
 * If no client is connected, the data is just discarded. If a client is connected, data
 * is forwarded to the client as fast as possible. Rx1 data and Rx2 data are sent directly,
 * an additional vector containing various status information is attached to each IRF.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* strmTCPServer(void*)
{
  bool tStrmExit = false;

  unsigned int tStrmPort = TCP_STREAM_PORT;
  TCP_server_single tServer;

  try
  {
    std::cout << ANSI_COLOR_MAGENTA << "Streaming: Thread created." << ANSI_COLOR_RESET << std::endl;
    
    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(2, &tCPUSet); // CPU #2
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Streaming::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif
    
    /* start server (listening socket) */
    tServer.startServer(tStrmPort, TCP_STR_SEND_TO_MS);
  }
  catch(ilmsens::mvital::ces::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(ilmsens::networking::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tStrmExit = true;
  }

  // streaming thread is always running (always waiting for client)
  // until the EXIT command has been received from client or signal
  while(!tStrmExit) 
  {
    try 
    {
      // give time for synchronisation after a client disconnected
      delay_ms(10);

      std::cout << ANSI_COLOR_MAGENTA << "Streaming: Waiting for Client at port " << std::dec << tStrmPort << " ..." << ANSI_COLOR_RESET << std::endl;

      // accept client connection with timeout
      while (!tStrmExit)
      {
        bool tClient = tServer.acceptClient(TCP_STR_WAIT_TO_MS);
        
        if (tClient)
        {
          // a new client was accepted
          std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: connection initiated with client " << tServer.getClientIP() << " ..." << ANSI_COLOR_RESET << std::endl;
          break;  
        }
        else
        {
          // no client was connected, so update the exit flag
          mMtxExit.lock();
            tStrmExit = mExitShutdwn;
          mMtxExit.unlock();
          
          // try to drain PR cache, if no client is there or the previous one was disconnected
          mMtxPRCache.lock();
          while(!mPRCache.empty()) 
          {
            mPRCache.pop();
          }
          mMtxPRCache.unlock();
        }
      } // waiting for client
      
      // a client is here?
      if (!tStrmExit)
      {
        /* DATA TRANSFER */

        // count packages streamed
        unsigned int tIdx = 0;
        
        /* Create data tansfer variables */
        TFIFOCacheEntry tTXData;

        TStatusInfo tStatusData(TCP_STR_STATUS_VEC_LEN, 0);
        unsigned int tStatSize = sizeof(TStatusInfo::value_type) * TCP_STR_STATUS_VEC_LEN;
        
        // output debugging messages?
        unsigned int tDebug = TCP_STR_DEBUG_OUTPUT;

        mMtxSharedData.lock();
          tDebug        = mState.mDebFlag;
        mMtxSharedData.unlock();

        //send incomming data to client as long as the connection is alive
        while(!tStrmExit)
        {
          // Waiting for new data in PR cache
          mMtxPRCache.lock();
            bool tEmpty = mPRCache.empty();
          
          if (!tEmpty)
          {
            // new data has arrived from processing thread, so copy and forward it
              tTXData = mPRCache.front();
              mPRCache.pop();
            mMtxPRCache.unlock();

            std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending frame #" << std::dec << tIdx << " with " << tTXData.mProcRx1.size() << " samples/Rx ..." << ANSI_COLOR_RESET << std::endl;

            // fill in status data from cache entry
            tStatusData[ 0] = tTXData.mIRFNum;
            tStatusData[ 1] = float2uint32(tTXData.mSenTemp);
            tStatusData[ 2] = float2uint32(tTXData.mModTemp);
            tStatusData[ 3] = float2uint32(tTXData.mAddTemp);
            tStatusData[ 4] = tTXData.mADCLevel1;
            tStatusData[ 5] = tTXData.mADCLevel2;
            tStatusData[ 6] = tTXData.mCacheSize;
            tStatusData[ 7] = tTXData.mSenTempVal;
            /*
            tStatusData[ 4] = tTXData.mCacheSize;
            tStatusData[ 5] = tTXData.mSenTempVal;
            tStatusData[ 6] = tTXData.mXU4MaxTemp;
            tStatusData[ 7] = float2uint32(tTXData.mSenTemp);
            tStatusData[ 8] = float2uint32(tTXData.mModTemp);
            tStatusData[ 9] = float2uint32(tTXData.mXU4Temp);
            */

            // calculate transmit sizes
            unsigned int tTxSize = static_cast<unsigned int>(sizeof(TRealIRF::value_type) * tTXData.mProcRx1.size());

            bool tExitStream = false;
            // send Rx 1 data
            if (tDebug)
            {
              // for debugging
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending Rx1 (" << std::dec << tTxSize << " Bytes)" << ANSI_COLOR_RESET << std::endl;
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: 1. value of Rx1: " << tTXData.mProcRx1[0] << ANSI_COLOR_RESET << std::endl;
            }

            unsigned int tTxLen = 0;
            char *tBuf = (char *)tTXData.mProcRx1.data();
            while ( tTxLen < tTxSize )
            {
              int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tTxSize - tTxLen);
              if(tLen < 0) 
              {
                // client connection has been lost
                tExitStream = true;
                break;
              }
              else
              {
                // count the bytes sent
                tTxLen += tLen;
              }
            }
            if (tExitStream) break;

            // send Rx 2 data
            if (tDebug)
            {
              // for debugging
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending Rx2 (" << std::dec << tTxSize << " Bytes)" << ANSI_COLOR_RESET << std::endl;
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: 1. value of Rx2: " << tTXData.mProcRx2[0] << ANSI_COLOR_RESET << std::endl;
            }
            tTxLen = 0;
            tBuf = (char *)tTXData.mProcRx2.data();
            while ( tTxLen < tTxSize )
            {
              int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tTxSize - tTxLen);
              if(tLen < 0) 
              {
                // client connection has been lost
                tExitStream = true;
                break;
              }
              else
              {
                // count the bytes sent
                tTxLen += tLen;
              }
            }
            if (tExitStream) break;

            // send status data
            if (tDebug)
            {
              // for debugging
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending status data (" << std::dec << tStatSize << " Bytes)" << ANSI_COLOR_RESET << std::endl;
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: IRF #       : " << tTXData.mIRFNum << std::endl
                                              << "Streaming::INFO: sensor temp.: " << tTXData.mSenTempVal
                                              << ANSI_COLOR_RESET << std::endl;
            }

            tTxLen = 0;
            tBuf = (char *)tStatusData.data();
            while ( tTxLen < tStatSize )
            {
              int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tStatSize - tTxLen);
              if(tLen < 0) 
              {
                // client connection has been lost
                tExitStream = true;
                break;
              }
              else
              {
                // count the bytes sent
                tTxLen += tLen;
              }
            }
            if (tExitStream) break;

            // package has been sent, so it counts
            tIdx++;
          }
          else
          {
            // cache was empty
            mMtxPRCache.unlock();
            
            // test, if connection is still alive...do non-blocking read with short timeout
            char tBuf[TCP_STR_MSG_SIZE];
            int tLen = tServer.receiveTO((void *)tBuf, TCP_STR_MSG_SIZE, 1);
            if (tLen < 0)
            {
              // client connection has been closed,
              // need to stop streaming TCP connection
              break;
            }
            else
            {
              // wait some time to lower CPU burden
              delay_ms(10);
            }
          }

          // update configuration and shutdown flags 
          mMtxSharedData.lock();
            tDebug    = mState.mDebFlag;
          mMtxSharedData.unlock();
          mMtxExit.lock();
            tStrmExit = mExitShutdwn;
          mMtxExit.unlock();
        }
      }

      if (tStrmExit)
      {
        std::cout << ANSI_COLOR_MAGENTA << "Streaming: received EXIT command, ending thread ..." << ANSI_COLOR_RESET << std::endl;
      }
      else
      {
        std::cout << ANSI_COLOR_MAGENTA << "Streaming: no or bad client connection on streaming socket ..." << ANSI_COLOR_RESET << std::endl;
      }
    } 
    catch(ilmsens::mvital::ces::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what() << std::endl
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(ilmsens::networking::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what() << std::endl
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(std::exception& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::Std-EXCEPTION: " << ANSI_COLOR_RESET 
                << tErr.what() << std::endl;
    }
    catch(...) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
    }
    
    // close client's TCP connection
    tServer.closeClient();
  }

  // can go now, TCP server will be shutdown by destructor
  std::cout << ANSI_COLOR_MAGENTA << "Streaming: shutdown complete, exiting thread." << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace streaming
} //namespace ces
} //namespace mvital
} //namespace ilmsens
