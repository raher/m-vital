/* KKF.CPP
 * 
 * Calculating cross correlation between real-valued stimulus and received signal. For better performance, 
 * we first convert the signals into frequency domain (FFT), calculate the cross correlation spectrum 
 * and after that we do a backtransformation (IFFT) into time domain..
 * 
 * For FFT/IFFT we are using FFTW-Library 3.3.4.
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include <cmath>

#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/ilmsens_processing_references.hpp"
#include "ilmsens/processing/KKF.hpp"


namespace ilmsens 
{
namespace processing
{

/* Constructor */
KKF::KKF(unsigned int pMLBSLen)
: mMLBSLen(pMLBSLen), 
  mFDRef ( fftw_alloc_complex ((size_t) pMLBSLen) ),
  mTDRaw ( fftw_alloc_real    ((size_t) pMLBSLen) ),
  mTDCorr( fftw_alloc_real    ((size_t) pMLBSLen) ),
  mFDRaw ( fftw_alloc_complex ((size_t) pMLBSLen) ),
  mFDCorr( fftw_alloc_complex ((size_t) pMLBSLen) )
{
  /* Getting ideal M-Sequence reference */
  mRefMLBS.resize(mMLBSLen);

  // guess MLBS order and get reference MLBS
  switch (mMLBSLen)  
  {
    case 511:     // 9th order 
      mRefMLBS = ilmsens::processing::references::getMLBSReferenceTD(9, 1);
      break;
    
    case 4095:    // 12th order
      mRefMLBS = ilmsens::processing::references::getMLBSReferenceTD(12, 1);
      break;
    
    case 32767:   // 15th order
      mRefMLBS = ilmsens::processing::references::getMLBSReferenceTD(15, 1);
      break;
      
    default:
      std::cerr << "KKF::ERROR: Unknown MLBS-Length: " << std::dec << mMLBSLen << "!" << std::endl;
      throw ilmsens::processing::Error("KKF: unknown MLBS length!", ILMSENS_ERROR_INVALID_PARAM);
  }
   
  /* Calculate Spectrum of ideal M-Sequence */

#if 0
  fftw_complex *tTD_mseq = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mMLBSLen); // Time-Domain ideal M-Sequence
  fftw_plan tFFTPlan = fftw_plan_dft_1d(mMLBSLen, tTD_mseq, mFDRef.get(), FFTW_FORWARD, FFTW_PATIENT); // FFT plan
  //fill ideal MLBS into vector
  double tNormScale = sqrt((double)mMLBSLen);
  for (unsigned int tI = 0; tI < mMLBSLen; tI++) 
  {
    tTD_mseq[tI][0] = mRefMLBS[tI] / tNormScale;  // Real Part
    tTD_mseq[tI][1] = 0;                          // Imaginary Part
  }
#endif

  double *tTD_mseq = (double*) fftw_alloc_real((size_t) mMLBSLen);                            // Time-Domain ideal M-Sequence
  fftw_plan tFFTPlan = fftw_plan_dft_r2c_1d(mMLBSLen, tTD_mseq, mFDRef.get(), FFTW_PATIENT);  // FFT plan for real MLBS
  double tNormScale = sqrt((double)mMLBSLen);
  for (unsigned int tI = 0; tI < mMLBSLen; tI++) 
  {
    tTD_mseq[tI] = mRefMLBS[tI] / tNormScale;  // Real Part
  }
  
  // do FFT of ideal MLBS
  fftw_execute(tFFTPlan);
  
  //clean up temporary FFTw ressources
  fftw_destroy_plan(tFFTPlan);
  fftw_free(tTD_mseq);
   
  /* Allocate all required FFTw memories for KKF calculation */
  mCorrIRF.resize(mMLBSLen);

  /* Create FFTw plans for KKF */
  mFFTPlan  = fftw_plan_dft_r2c_1d(mMLBSLen, mTDRaw.get(), mFDRaw.get(), FFTW_PATIENT);   //FFT plan
  mIFFTPlan = fftw_plan_dft_c2r_1d(mMLBSLen, mFDCorr.get(), mTDCorr.get(), FFTW_PATIENT); //IFFT plan
}

/* Destructor */
KKF::~KKF()
{
  //destroy FFTw plans
  fftw_destroy_plan(mFFTPlan); //FFT
  fftw_destroy_plan(mIFFTPlan); //IFFT
}

/* Calculate Crosscorrelation between received impulse response and ideal m-sequence */
const TRealIRF& KKF::calculateKKF(const TRealIRF& pData) 
{
  double *tTDRaw  = mTDRaw.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    tTDRaw[tI] = pData[tI]; // Real part
  }

  /* FFT: Calculate Spectrum of raw data */
  fftw_execute(mFFTPlan);

  /* Calculate Cross-Spectrum FFT(x(t)) * conj(FFT(mlbs(t))) */
  fftw_complex *tFDRaw  = mFDRaw.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDRef  = mFDRef.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDCorr = mFDCorr.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    //complex multiply, ATTENTION: mFDRef is used in complex conjugate form!
    tFDCorr[tI][0] = (tFDRaw[tI][0] * tFDRef[tI][0]) + (tFDRaw[tI][1] * tFDRef[tI][1]); // Real part
    tFDCorr[tI][1] = (tFDRaw[tI][1] * tFDRef[tI][0]) - (tFDRaw[tI][0] * tFDRef[tI][1]); // Imag. part
  }

  /* Calculate IFFT of Cross-Spectrum */
  fftw_execute(mIFFTPlan);

  /* Normalize and copy */
  double tNormScale = (double) mMLBSLen;
  double *tTDCorr = mTDCorr.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    mCorrIRF[tI] = tTDCorr[tI] / tNormScale;
  }

  return(mCorrIRF);
}

/* return IRF of last correlation operation */
const TRealIRF& KKF::getLastIRF(void)
{
  return(mCorrIRF);
}

/* return FRF of last correlation operation */
const fftw_complex* KKF::getLastFRF(void)
{
  return (mFDCorr.get());
}

} //namespace processing
} //namespace ilmsens
