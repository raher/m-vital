/* KKF_interp.CPP
 * 
 * Calculating cross correlation between stimulus and received signal. For better performance, 
 * we first convert the signals into frequency domain (FFT), calculate the cross correlation spectrum 
 * and after that we do a backtransformation (IFFT) into time domain. Additionally, an interpolated
 * vector can be retrieved.
 * 
 * For FFT/IFFT we are using FFTW-Library 3.3.4.
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include <cmath>
#include <cstring>

#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/KKF_interp.hpp"


namespace ilmsens 
{
namespace processing
{

/* Constructor */
KKF_interp::KKF_interp(unsigned int pMLBSLen, unsigned int pIPLen)
: KKF(pMLBSLen),
  mIPLen(pIPLen), 
  mTDIp ( fftw_alloc_real((size_t) pIPLen) ),
  mFDIp ( fftw_alloc_complex((size_t) pIPLen) )
{
  /* Check interpolation length */
  if (mIPLen < mMLBSLen)
  {
    // not allowed
    throw ilmsens::processing::Error("KKF_interp: interpolation length must not be smaller than MLBS length!", ILMSENS_ERROR_INVALID_PARAM);
  }
  
  /* Allocate all required FFTw memories for KKF calculation */
  mInterpIRF.resize(mIPLen);

  /* Cleanup FFTW data vectors for interpolation */
  fftw_complex *tFDIp  = mFDIp.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mIPLen; tI++)
  {
    tFDIp[tI][0] = 0.0;  // Real Part
    tFDIp[tI][1] = 0.0;  // Imaginary Part
  }

  /* Create FFTw plans for interpolation */
  mIFFTPlanIp = fftw_plan_dft_c2r_1d(mIPLen, mFDIp.get(), mTDIp.get(), FFTW_PATIENT); // IFFT plan
}

/* Destructor */
KKF_interp::~KKF_interp()
{
  //destroy FFTw plan
  fftw_destroy_plan(mIFFTPlanIp); // interpolated IFFT
}

/* Calculate interpolation of last correlated FRF */
const TRealIRF& KKF_interp::calculateInterpolation(void) 
{
  /* Copy last FRF to zero padded interpolation buffer */
  const fftw_complex *tFDCorr = getLastFRF();   // get raw pointer to last correlated FRF
  fftw_complex *tFDIp         = mFDIp.get();    // get raw pointer (not required, but faster:-)

  //clear input array
  memset((void *)tFDIp, 0, (size_t)mIPLen * sizeof(fftw_complex));

  // positive frequencies
  unsigned int tNumPos  = (mMLBSLen >> 1) + 1;
  for (unsigned int tI = 0; tI < tNumPos; tI++)
  {
    tFDIp[tI][0] = tFDCorr[tI][0];  // Real part
    tFDIp[tI][1] = tFDCorr[tI][1];  // Imag. part
  }

  // negative frequencies
  unsigned int tNumNeg  = mMLBSLen - tNumPos;
  tFDCorr += tNumPos;
  tFDIp   += mIPLen - tNumNeg;
  for (unsigned int tI = 0; tI < tNumNeg; tI++)
  {
    tFDIp[tI][0] = tFDCorr[tI][0];  // Real part
    tFDIp[tI][1] = tFDCorr[tI][1];  // Imag. part
  }
  
  /* Calculate IFFT of zero padded Cross-Spectrum */
  fftw_execute(mIFFTPlanIp);
  
  /* Normalize and copy */
  double tNormScale = (double)mMLBSLen; // revert scaling of FFT/IFFT to get original amplitudes 
                                        // like in uninterpolated data
  double *tTDIp = mTDIp.get();          // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mIPLen; tI++)
  {
    mInterpIRF[tI] = tTDIp[tI] / tNormScale;
  }
  
  return(mInterpIRF);
}

/* return FRF of last interpolation operation */
const fftw_complex* KKF_interp::getLastInterpolationFRF(void)
{
  return (mFDIp.get());
}

} //namespace processing
} //namespace ilmsens
