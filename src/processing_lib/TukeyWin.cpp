/* TUKEYWIN.CPP
 * 
 * Calculates the Tukey-Window for short time fouriertransformation
 * Code is taken from MatLab R2018a
 *
 */

#include <iostream>
#include <cmath>
#define _USE_MATH_DEFINES
#include <math.h>

#include "ilmsens/processing/TukeyWin.hpp"
#include "ilmsens/processing/HannWin.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens
{
  namespace processing
  {
    
    /* Constructor */
    TukeyWin::TukeyWin(unsigned int pWinLen, double pTaper)
      : mWinLen   (unsigned int (pWinLen)),
        mTaper    (double       (pTaper)),
        mTukeyVec (pWinLen, 1.0)
    {
      if (mTaper <= 0)    // rectangular window
      {}
      else if (mTaper >= 1)    // Hann-Window
      {
        // TukeyWin = HannWin(mWinLen);
        HannWin hannWin(mWinLen);
        mTukeyVec = hannWin.getHannVec();
      }
      else
      {
        double period = mTaper/2;
        
        // left taper
        int tl = (int) floor(period * (mWinLen - 1));         // left  taper idx:0..t1
        for (int i = 0; i <= tl; i++)
        {
          mTukeyVec[i] = (1 + cos(M_PI / period * (i - period))) / 2;
        }

        // right taper
        int tr = mWinLen - tl;                   // right taper idx:tr..mWinLen-1
        for (unsigned int i = tr; i < mWinLen; i++)
        {
          mTukeyVec[i] = (1 + cos(M_PI / period * (i - 1 + period))) / 2;
        }
      }
    }

    TukeyWin::TukeyWin(unsigned int pWinLen)
    { 
      TukeyWin(pWinLen, 0.5);
    }
    
    /* Destructor */
    TukeyWin::~TukeyWin()
    {
    }

    /* Getter */
    const TSampleVec& TukeyWin::getTukeyVec(void)
    {
      return(mTukeyVec);
    }
  
  } //namespace processing
} //namespace ilmsens
