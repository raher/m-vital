/* HAPA.CPP
 * 
 *
 */

#include <iostream>
#include <algorithm>
#include <iomanip>

#include "ilmsens/processing/Hapa.hpp"
#include "ilmsens/processing/Path.hpp"
#include "ilmsens/processing/vector_iteration_functions.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens
{
  namespace processing
  {
    
    /* Constructor */
    Hapa::Hapa(TSampleVec pSpectrum,
               TSampleVec pFreq,
               double     pMinHeartFreq,
               double     pMaxHeartFreq,
               double     pThrPeak,
               double     pTolHeartFreqIntvl,
               double     pTolFreqDist,
               int        pMaxPathLen)
      : mHeartRate         (0.0),
        mMinHeartFreq      (double (pMinHeartFreq)),
        mMaxHeartFreq      (double (pMaxHeartFreq)),
        mThrPeak           (double (pThrPeak)),
        mTolHeartFreqIntvl (double (pTolHeartFreqIntvl)),
        mTolFreqDist       (double (pTolFreqDist)),
        mMaxPathLen        (int    (pMaxPathLen))
    {
      // Find Peaks in spectrum
      TRealIRF tPeakFreq(0);
      TIdxVec tPeaks = findPeaks(pSpectrum, true, mThrPeak);
      // extract idx-values and get frequency-vector for peaks
      for(int i = 0; i < tPeaks.size(); i++)
      {
        unsigned int idx = tPeaks[i].mIdx;
        tPeakFreq.push_back(pFreq[idx]);
      }

      // Find all possible f0 and 2*f0 (first overtone)
      TRealIRF tFirstHeart  = subvecOfIntvl(tPeakFreq,     mMinHeartFreq - mTolHeartFreqIntvl,     mMaxHeartFreq + mTolHeartFreqIntvl);
      TRealIRF tSecondHeart = subvecOfIntvl(tPeakFreq, 2 * mMinHeartFreq - mTolHeartFreqIntvl, 2 * mMaxHeartFreq + mTolHeartFreqIntvl);

      // init paths
      PathVec curPaths(0);
      for (int idx= 0; idx < tFirstHeart.size(); idx++)
        {
          Path tPath(tFirstHeart[idx]);
          curPaths.push_back(tPath);
        }

      PathVec curPathsOvertones(0);
      for (int idx= 0; idx < tSecondHeart.size(); idx++)
        {
          Path tpath(tSecondHeart[idx]);
          curPathsOvertones.push_back(tpath);
        }

      PathVec paths = curPaths;
      TRealIRF tNextHeart;

      if (tFirstHeart.empty() & tSecondHeart.empty())
      {
        // no heart-rate
        mHeartRate = 0.0;
      }
      else
      {
  
        // extend paths
        int startHarm;

        // choose startHarm
        if (tFirstHeart.empty())
        {
          startHarm = 3;
          paths = concardPathVec(paths, curPathsOvertones);
        }
        else
        {
          startHarm = 2;
        }

        // search nextHarm
        for (int harm = startHarm; harm <= mMaxPathLen; harm++)
        {
          tNextHeart = subvecOfIntvl(tPeakFreq, harm * mMinHeartFreq - mTolHeartFreqIntvl, harm * mMaxHeartFreq + mTolHeartFreqIntvl);
          

          curPaths = extendPath(curPaths, tNextHeart);
          paths = concardPathVec(paths, curPaths);

          if(harm > 2)
          {
            curPathsOvertones = extendPath(curPathsOvertones, tNextHeart); 
          }

          paths = concardPathVec(paths, curPathsOvertones);
        } // end for-loop: startHarm - pMaxPathLen

        // search the paths for the best and extract the heart rate
        std::vector<double> energie(paths.size());

        for (int i = 0; i < paths.size(); i++)
        {
          Path curPath = paths[i];
          energie[i]   =0;
          for (int curPathIdx = 1; curPathIdx < curPath.getPathLen(); curPathIdx++)
          {
            double tPathFreq = curPath.getFreq(curPathIdx);
            double interpolPathPoint = lin_interpolation(pFreq, pSpectrum, tPathFreq);
            energie[i] =+ interpolPathPoint;
          }
        }

        // Index with max energy
        std::vector<double>::iterator it;
        it = std::max_element(energie.begin(),energie.end());
        size_t idx = std::distance(energie.begin(), it);

        Path path = paths[idx];
        mHeartRate = path.getPathMean();
      } // end init
    }
    
    /* Destructor */
    Hapa::~Hapa()
    {
    }

    /* Getter */
    const double& Hapa::getHeartRate(void)
    {
      return mHeartRate;
    }

    /* Path-Extension
       searches next harmonics for a given path
    */
    PathVec Hapa::extendPath(PathVec pPathVec, TRealIRF pNextPeaks)
    {
      PathVec extPathVec(0);
      for(int curPathIdx = 0; curPathIdx < pPathVec.size(); curPathIdx++)
      {
        Path curPath = pPathVec[curPathIdx];
        if (!pNextPeaks.empty())
        {
          // extends curPath with all  possible frequeny peaks
          PathVec curPathExtVec(0);
          for (int nextPeaks = 0; nextPeaks < pNextPeaks.size(); nextPeaks++)
          {
            Path extPath = curPath;
            extPath.extendPath(pNextPeaks[nextPeaks]);
            
            // check validity
            bool validTest = extPath.validationTest(mMinHeartFreq + mTolHeartFreqIntvl, mMaxHeartFreq + mTolHeartFreqIntvl, mTolHeartFreqIntvl, mTolFreqDist);
            extPath.setValid(validTest);
            
            if (extPath.isValid())
            {
              extPathVec.push_back(extPath);
            }
          }
        }
      }
      return extPathVec;
    }

    /* Subvector, elements within a given open interval
       prerequisite: pVec is sorted vector
    */
    TRealIRF Hapa::subvecOfIntvl(TRealIRF pVec, double pMinIntvl, double pMaxIntvl)
    {
      TRealIRF subvec;
      for(int i = 0; i < pVec.size(); i++)
      {
        if(pVec[i] > pMinIntvl)     // lowerbound
        {
          if (pVec[i] < pMaxIntvl)  // and upperbound
          {
            subvec.push_back(pVec[i]);
          }
          else
            break;
        }
      } // end for-loop
      return subvec;
    }

    /* paths = paths + overtones */
    PathVec Hapa::addOvertones(PathVec pOldPaths, TRealIRF pOvertones)
    {
      PathVec newPaths(0);
      for (int oldPathIdx = 0; oldPathIdx  < pOldPaths.size(); oldPathIdx ++)
      {
        for (int overtoneIdx = 0; overtoneIdx  < pOvertones.size(); overtoneIdx ++)
        {
          Path newPath = pOldPaths[oldPathIdx];
          newPath.extendPath(pOvertones[overtoneIdx]);
          newPaths.push_back(newPath);
        }
      }
      return newPaths;
    }

    /* concardinate two path-vectors
     * without path1 of pPathVec1 if it is sub-path of a path in pPathVec2
    */
    PathVec Hapa::concardPathVec(PathVec pPathVec1, PathVec pPathVec2)
    {
      PathVec newPathVec = pPathVec2;
      for(int i = 0; i < pPathVec1.size(); i++)
      {
        Path path1 = pPathVec1[i];
        for (int j = 0; j < pPathVec2.size(); j++)
        {
          Path path2 = pPathVec2[j];
          if(path2.includes(path1))
          {
            break;
          }
        }
        newPathVec.push_back(path1);
      }
      return newPathVec;
    } 
  
  } //namespace processing
} //namespace ilmsens

