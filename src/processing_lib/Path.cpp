/* PATH.CPP
 * 
 * class for heartrate with overtones
 */

#include <iostream>
#include <algorithm>
#include <iomanip>

#include "ilmsens/processing/Path.hpp"
#include "ilmsens/processing/vector_iteration_functions.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens
{
  namespace processing
  {
    /* Constructor */
    Path::Path()
      : mPath(1, 0),
        mPathDiff(0),
        mPathMean(0),
        mValid(true)
    {
    }

    Path::Path(TSample pFreq)
      : mPath(1,0),
        mPathDiff(1),
        mPathMean(pFreq/2.0),
        mValid(true)
    {
      setPath(pFreq);
      entryDiffRlIRFs(mPathDiff, mPath);
    }

    Path::Path(TRealIRF pFreqVec)
      : mPath(1,0),
        mValid(true)
    {
      setPath(pFreqVec);
      entryDiffRlIRFs(mPathDiff, mPath);
      mPathMean = pFreqVec.back() /static_cast<double>(pFreqVec.size());
    }

    /* Deconstructor */
    Path::~Path()
    {
    }

    /* Setter - private */
    void Path::setPath(TSample pFreq){
      mPath[1] = pFreq;
    }

    void Path::setPath(TRealIRF pFreqVec)
    {
      for (int i = 0; i < pFreqVec.size(); i++)
      {
        mPath[i+1] = pFreqVec[i];
      }
    }


    /* Getter */
    TRealIRF Path::getPath()
    {
      return mPath;
    }

    TSample Path::getFreq(int pIdx)
    {
      return mPath[pIdx];
    }

    int Path::getPathLen()
    {
      return static_cast<int>(mPath.size());
    }

    TRealIRF Path::getPathDiff()
    {
      return mPathDiff;
    }

    double Path::getLastDiff()
    {
      return mPathDiff.back();
    }

    double Path::getPathMean()
    {
      return mPathMean;
    }

    bool Path::isValid()
    {
      return mValid;
    }



    /* Methods */

    bool Path::includes(Path pPath)
    {
      // pr�fe ob pPath in mPath enthalten ist
      TRealIRF pSub = pPath.getPath();
      if (std::includes(mPath.begin(), mPath.end(), pSub.begin(), pSub.end()))
      {
        return true;
      }
      else
      {
        return false;
      }
    }


    void Path::extendPath(TSample newFreq)
    {
      mPath.push_back(newFreq);
      mPathDiff.push_back(newFreq - mPath.back());
      mPathMean = mPath.back() / static_cast<double>(mPath.size());
    }

    void Path::setValid(bool pValid)
    {
      mValid = pValid;
    }

    bool Path::validationTest(double pMinFreqDist, double pMaxFreqDist, double pFreqMeanDist, double pMultDist)
    {
      // invalid last Step
      if(mPathDiff.back() < pMinFreqDist || mPathDiff.back() > pMaxFreqDist)
        return false;
      // diff between path-steps and mean
      for(int diffIdx = 0; diffIdx < mPathDiff.size(); diffIdx++)
      {
        double tAbsDiff = abs(mPathDiff[diffIdx] - mPathMean);
        if(tAbsDiff > pFreqMeanDist)
          return false;
      }
      // path-elements are multiples
      for(int pathIdx = 0; pathIdx < mPath.size(); pathIdx++)
      {
        double pTolInt = (mPath[pathIdx] / mPathMean) - static_cast<double>(pathIdx);
        if(pTolInt > pMultDist)
          return false;
      }
      return true;
    }

  } //namespace processing
} //namespace ilmsens

