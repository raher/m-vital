/* HILBERT.CPP
 * Calculating Hilbert transform of given data.
 * 
 * For FFT/IFFT we are using FFTW-Library 3.3.4.
 */

#include <iostream>
#include <iomanip>
#include <cstring>

#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/Hilbert.hpp"


namespace ilmsens 
{
namespace processing
{

/* Constructor */
Hilbert::Hilbert(unsigned int pWinLen)
:mWinLen(pWinLen), 
 mTDHilb( fftw_alloc_complex((size_t) pWinLen) ), 
 mFDHilb( fftw_alloc_complex((size_t) pWinLen) ) 
{
  /* Allocate remaining memories for KKF calculation */
  mHilbIRF.resize(mWinLen);
  
  /* Allocate FFTw plans for KKF calculation */
  mFFTPlan  = fftw_plan_dft_1d(mWinLen, mTDHilb.get(), mFDHilb.get(), FFTW_FORWARD, FFTW_PATIENT);  //FFT plan
  mIFFTPlan = fftw_plan_dft_1d(mWinLen, mFDHilb.get(), mTDHilb.get(), FFTW_BACKWARD, FFTW_PATIENT); //IFFT plan
    
  //generate reference weight vector for Hilbert transform
  mH[0] = 1.0;
  mH[mWinLen/2] = 1.0;
  for (unsigned int tI = 1; tI < mWinLen/2 - 1; tI++) 
  {
    mH[tI] = 2.0;
  }
  for (unsigned int tI = mWinLen/2 + 1; tI < mWinLen; tI++)
  {
    mH[tI] = 0.0;
  }
}

/* Destructor */
Hilbert::~Hilbert()
{
  //destroy FFTw plans
  fftw_destroy_plan(mFFTPlan); //FFT
  fftw_destroy_plan(mIFFTPlan); //IFFT
}

/* Hilbert transform for time domain data */
const TRealIRF& Hilbert::calculateHilbTD (const TRealIRF& pTDData)
{
  //check data length
  unsigned int tCurLen = (unsigned int)pTDData.size();
  if (tCurLen != mWinLen)
  {
    std::cerr << "Hilbert::ERROR: length of input data (" << std::dec << tCurLen << ") does not match reference length (" << mWinLen << ")!" << std::endl;
    throw ilmsens::processing::Error("Hilbert: wrong signal length!", ILMSENS_ERROR_INVALID_PARAM);
  }

  //copy data into FFTw buffer
  for (unsigned int tI = 0; tI < mWinLen; tI++)
  {
    mTDHilb[tI][0] = pTDData[tI];
    mTDHilb[tI][1] = 0.0;
  }

  //perform FFT
  fftw_execute(mFFTPlan);

  //do the transform and return result
  return (doHilbert());
}

/* Hilbert transform for frequency domain data */
const TRealIRF& Hilbert::calculateHilbFD (const fftw_complex* pFDData)
{
  /* TODO: how to check data length?
  //check data length
  */

  //copy data into FFTw buffer
  memcpy((void *) mFDHilb.get(), (const void *) pFDData, sizeof(fftw_complex) * mWinLen);

  //do the transform and return result
  return (doHilbert());
}

/* Do the Hilbert transform and return result */
const TRealIRF& Hilbert::doHilbert(void)
{
  //calculate Hilbert product
  for (unsigned int tI = 0; tI < mWinLen; tI++)
  {
    mFDHilb[tI][0] = mFDHilb[tI][0] * mH[tI];
    mFDHilb[tI][1] = mFDHilb[tI][1] * mH[tI];
  }
  
  //do IFFT
  fftw_execute(mIFFTPlan);
  
  //copy data to output buffer
  for (unsigned int tI = 0; tI < mWinLen; tI++)
  {
    //TODO: hilbert transform is complex, the actual time domain result is in the imaginary part
    mHilbIRF[tI] = mTDHilb[tI][1];
  }
  
  return (mHilbIRF);
}

} //namespace processing
} //namespace ilmsens
