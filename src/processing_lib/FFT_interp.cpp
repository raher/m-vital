/* FFT_interp.CPP
 * 
 * Calculating FFT of given IRF with frequency interpolation by zero padding in time domain.
 * 
 * For FFT we are using FFTW-Library 3.3.4.
 */

#include <iostream>
#include <iomanip>
#include <string>

#include <cmath>

#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/FFT_interp.hpp"


namespace ilmsens 
{
namespace processing
{

/* Constructor */
FFT_interp::FFT_interp(unsigned int pIRFLen, unsigned int pIPLen)
: mIRFLen(pIRFLen),
  mIPLen(pIPLen > pIRFLen ? pIPLen : pIRFLen), 
  mTDIp ( fftw_alloc_real((size_t) this->mIPLen) ),
  mFDIp ( fftw_alloc_complex((size_t) this->mIPLen) )
{
  /* Cleanup FFTW data vectors for interpolation */
  double       *tTDIp  = mTDIp.get(); // get raw pointer (not required, but faster:-)
  fftw_complex *tFDIp  = mFDIp.get(); // get raw pointer (not required, but faster:-)

  for (unsigned int tI = 0; tI < mIPLen; tI++)
  {
    tTDIp[tI]    = 0.0;  // TD sample
    tFDIp[tI][0] = 0.0;  // Real Part
    tFDIp[tI][1] = 0.0;  // Imaginary Part
  }

  /* Create FFTw plan */
  mFFTPlan = fftw_plan_dft_r2c_1d(mIPLen, tTDIp, tFDIp, FFTW_PATIENT);   //FFT plan
}

/* Destructor */
FFT_interp::~FFT_interp()
{
  //destroy FFTw plan
  fftw_destroy_plan(mFFTPlan);
}

/* Calculate interpolated spectrum by zero padding in time domain */
const fftw_complex* FFT_interp::getInterpolatedFRF(TRealIRF& pData) 
{
  /* Copy given IRF to zero padded interpolation buffer */
  double       *tTDIp  = mTDIp.get(); // get raw pointer (not required, but faster:-)

  //clear time domain array
  memset((void *)tTDIp, 0, (size_t)mIPLen * sizeof(double));
  //copy input data
  for (unsigned int tI = 0; tI < mIRFLen; tI++)
  {
    tTDIp[tI] = pData[tI];
  }
  
  /* Calculate FFT of zero padded time domain */
  fftw_execute(mFFTPlan);
  
  //return teh result
  return(mFDIp.get());
}

} //namespace processing
} //namespace ilmsens
