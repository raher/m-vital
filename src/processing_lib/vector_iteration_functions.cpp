/* vector_iteration_functions.CPP
 * 
 * Functions iterating along a given vector.
 */

//#include <iostream>
//#include <iomanip>
#include <string>
#include <cmath>
#include <algorithm>

#define __func__ __FUNCTION__                //fix for VS below 2015

#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/vector_iteration_functions.hpp"

/************************/
/** convenience macros **/
/************************/

/** check for empty input sample vector */
#define CHECK_INPUT_NON_EMPTY(pFunc, pIn)                                                          \
  if (!pIn.size()) { throw ilmsens::processing::Error(std::string(pFunc) + ": input data vector must not be empty!", ILMSENS_ERROR_INVALID_PARAM); }



namespace ilmsens 
{
namespace processing
{


/************************/
/** Extrema            **/
/************************/

/** Get maximum value of real sample vector */
const TIdxSample& getRlMax(const TRealIRF& pData)
{
  static TIdxSample sSamp;

  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // get maximum
  sSamp.mVal = pData[0];
  sSamp.mIdx = 0;
  for (unsigned int tI = 1; tI < pData.size(); tI ++)
  {
   if (pData[tI] > sSamp.mVal)
   {
     sSamp.mVal = pData[tI];
     sSamp.mIdx = tI;
   }
  }

  return(sSamp);
}

/** Get minimum value of real sample vector */
const TIdxSample& getRlMin (const TRealIRF& pData)
{
  static TIdxSample sSamp;

  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // get minimum
  sSamp.mVal = pData[0];
  sSamp.mIdx = 0;
  for (unsigned int tI = 1; tI < pData.size(); tI ++)
  {
   if (pData[tI] < sSamp.mVal)
   {
     sSamp.mVal = pData[tI];
     sSamp.mIdx = tI;
   }
  }

  return(sSamp);
}

/** Get minimum + maximum value of real sample vector */
const TMinMax& getRlMinMax (const TRealIRF& pData)
{
  static TMinMax sSamps;

  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // get minimum & maximum
  sSamps.mMin.mVal = pData[0];
  sSamps.mMin.mIdx = 0;
  sSamps.mMax.mVal = pData[0];
  sSamps.mMax.mIdx = 0;
  for (unsigned int tI = 1; tI < pData.size(); tI ++)
  {
   if (pData[tI] > sSamps.mMax.mVal)
   {
     sSamps.mMax.mVal = pData[tI];
     sSamps.mMax.mIdx = tI;
   }
   else
   {
     if (pData[tI] < sSamps.mMin.mVal)
     {
       sSamps.mMin.mVal = pData[tI];
       sSamps.mMin.mIdx = tI;
     }
   }
  }

  return(sSamps);
}

/** Find local maxima according to given rules */
const TIdxVec& findPeaks(const TRealIRF& pData, bool pUseMPH, TSample pMPH, TSample pThres, TSample pProm, size_t pMinDist, size_t pNumPeaks)
{
  static TIdxVec sMaxIdxs;
  static TIdxSample sSamp;

  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  //clear result buffer
  sMaxIdxs.clear();

  // check input length again
  if (pData.size() <= 2)
  {
    // no peaks can be dected with less than 3 samples
    return(sMaxIdxs);
  }

  //check threshold
  if (pThres < 0.0)
  {
    pThres = 0.0;
  }
  //set MinPeakHeight to lowest value when not using given value
  if (!pUseMPH)
  {
    pMPH = std::numeric_limits< TSample>::lowest();
  }

  //set starting conditions
  TSample tLastMin    = pData[0];
  size_t  tLastMinIdx = 0;
  TSample tLastMax    = pData[0];
  size_t  tLastMaxIdx = 0;

  TSample tVal     = pData[0];
  TSample tValPrev = pData[0];
  TSample tValNext = pData[1];
  size_t  tCurIdx  = 0;
  for (size_t tI = 1; tI < pData.size()-1; tI ++)
  {
    // save current as previous value when next value was dfferent last time
    if (tVal != tValNext)
    {
      tValPrev = tVal;

      //check for minimum value between maxima
      if (tValPrev < tLastMin)
      {
        //update
        tLastMin    = tValPrev;
        tLastMinIdx = tCurIdx;
      }
      
      //update current value as well
      tVal     = pData[tI];
      tCurIdx  = tI;
    }

    // get next value in any case, even if the value stays the same
    tValNext = pData[tI+1];

    // possible maximum?
    if (((tVal-pThres) > tValPrev) && ((tVal-pThres) > tValNext))
    {
      //prepare prominence check
      TSample tPromThres = tVal - pProm;

      //check other conditions
      if ( (((tCurIdx-tLastMaxIdx) > pMinDist) || !sMaxIdxs.size()) && (tVal >= pMPH) && (tLastMin < tPromThres) )
      {
        //check prominence looking forward
        if (pProm > 0.0)
        {
          //iterate future values
          for (size_t tF = tI + 1; tF < pData.size(); tF ++)
          {
            if (pData[tF] < tPromThres)
            {
              //condition met, so save the result
              TIdxSample tNewMax(tVal, (unsigned int)tCurIdx);
              sMaxIdxs.push_back(tNewMax);

              //update last values
              tLastMax    = tVal;
              tLastMaxIdx = tCurIdx;
              tLastMin    = tValNext;
              tLastMinIdx = tI+1;

              break;
            }
            if (pData[tF] > tVal)
            {
              //condition lost, so just go without saving the result
              break;
            }
          }
        }
        else
        {
          //prominence ignored, so store the newly found maximum
          TIdxSample tNewMax(tVal, (unsigned int)tCurIdx);
          sMaxIdxs.push_back(tNewMax);

          //update last values
          tLastMax    = tVal;
          tLastMaxIdx = tCurIdx;
          tLastMin    = tValNext;
          tLastMinIdx = tI+1;
        }

        //check for end of search: 0 = find all peaks
        if (pNumPeaks && (sMaxIdxs.size() >= pNumPeaks))
        {
          //finish search loop
          break;
        }
      }
    }
  }

  return(sMaxIdxs);
}


/************************/
/** Power and Energy   **/
/************************/

/** Get sum of real sample vector */
TSample getRlSum (const TRealIRF& pData)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // get sum of samples
  TSample tVal = 0.0;
  for (unsigned int tI = 0; tI < pData.size(); tI ++)
  {
    tVal += pData[tI];
  }

  return(tVal);
}

/** Get mean of real sample vector */
TSample getRlMean (const TRealIRF& pData)
{
  return(getRlSum(pData) / (TSample)pData.size());
}

/** Get energy of real sample vector */
TSample getRlEn (const TRealIRF& pData)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // get sum of squared samples
  TSample tVal = 0.0;
  for (size_t tI = 0; tI < pData.size(); tI ++)
  {
    tVal += pData[tI] * pData[tI];
  }

  return(tVal);
}

/** Get variance of real sample vector */
TSample getRlVar (const TRealIRF& pData)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // get sum of samples and squared samples
  TSample tSum = 0.0;
  TSample tEn  = 0.0;
  size_t tLen = pData.size();
  for (size_t tI = 0; tI < tLen; tI ++)
  {
    tSum += pData[tI];
    tEn  += pData[tI] * pData[tI];
  }

  TSample tN = (TSample) tLen;
  return(tEn/tN - tSum/tN*tSum/tN );
}

/** Get centre of energy of real sample vector */
TTimeDelay getRlCoE(const TRealIRF& pData, int pTimeOff, TSample* pEn)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // get sum of time-weighted energy
  TSample tMom  = 0.0;
  TSample tEn   = 0.0;
  TSample tTime = (TSample) pTimeOff;
  size_t tLen = pData.size();
  for (size_t tI = 0; tI < tLen; tI ++)
  {
    TSample tCurEn = pData[tI] * pData[tI];
    tMom += tCurEn * tTime;
    tEn  += tCurEn;

    tTime += 1.0;
  }

  // report energy if requested by caller
  if (pEn != NULL)
  {
    *pEn = tEn;
  }

  return( (TTimeDelay)(tMom/tEn) );
}

/** Get centre of moment of real sample vector */
TTimeDelay getRlCoM(const TRealIRF& pData, int pTimeOff, unsigned int pDeg)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // check degree (for faster implementation)
  if (pDeg == 2)
  {
    return(getRlCoM(pData, pTimeOff));
  }

  // get sum of time-weighted moments
  TSample tMom  = 0.0;
  TSample tEn   = 0.0;
  TSample tTime = (TSample) pTimeOff;
  TSample tDeg  = (TSample) pDeg;
  size_t tLen = pData.size();
  for (size_t tI = 0; tI < tLen; tI ++)
  {
    TSample tCurEn = pow(fabs(pData[tI]), tDeg);
    tMom += tCurEn * tTime;
    tEn  += tCurEn;

    tTime += 1.0;
  }

  return( (TTimeDelay)(tMom/tEn) );
}


/************************/
/** IRF analysis       **/
/************************/

/** Get direct real IRF parameters */
const TIRFPara& getRlIRFPara (const TRealIRF& pData)
{
  static TIRFPara sPP;

  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // get minimum & maximum, energy and sample sum
  sPP.mMin.mVal = pData[0];
  sPP.mMin.mIdx = 0;
  sPP.mMax.mVal = pData[0];
  sPP.mMax.mIdx = 0;
  TSample tSum = 0.0;
  TSample tEn  = 0.0;
  for (unsigned int tI = 1; tI < pData.size(); tI ++)
  {
    // min & max
    if (pData[tI] > sPP.mMax.mVal)
    {
     sPP.mMax.mVal = pData[tI];
     sPP.mMax.mIdx = tI;
    }
    else
    {
      if (pData[tI] < sPP.mMin.mVal)
      {
        sPP.mMin.mVal = pData[tI];
        sPP.mMin.mIdx = tI;
      }
    }

    // sum and energy
    tSum += pData[tI];
    tEn  += pData[tI] * pData[tI];
  }
  
  // save further results
  sPP.mSum = tSum;
  sPP.mEn  = tEn;

  TSample tN = (TSample) pData.size();
  sPP.mVar   = tEn/tN - tSum/tN*tSum/tN;

  return(sPP);
}

/** Get percentile amplitude for IRF */
const TSample getRlIRFPctl (const TRealIRF& pData, double pPctl)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // check percentile argument
  if ((pPctl <= 0.0) || (pPctl > 1.0))
  {
    throw ilmsens::processing::Error("getRlIRFPctl::ERROR: percentile parameter out of range ]0,1.0]!", ILMSENS_ERROR_INVALID_PARAM);
  }

  //copy & sort the vector
  TRealIRF tCpyData(pData);
  std::sort(tCpyData.begin(), tCpyData.end());

  //get index of percentile sample
  size_t tPctlIdx = (size_t)floor((double)tCpyData.size() * pPctl);

  TSample tPctlAmp = tCpyData.at(tPctlIdx);

  return(tPctlAmp);
}


/************************/
/** basic vector math  **/
/************************/


/** fill vector witl equidistant values */
void createGridVec (TRealIRF& pVec, TSample pStartVal, TSample pStepVal, size_t pLen)
{
  //empty vector
  pVec.clear();
  // check input length
  if (!pLen)
  {
    //nothing more to do
    return;
  }

  // resize and pre-fill vector
  pVec.resize(pLen, pStartVal);
  
  // fill vectoir with incremented values
  TSample tVal = pStartVal;
  for (size_t tI = 0; tI < pLen; tI++)
  {
    pVec[tI] = tVal;
    tVal += pStepVal;
  }
}

/** add vectors */
void addRlIRFs (TRealIRF& pResult, const TRealIRF& pData1, const TRealIRF& pData2)
{
  // check input lengths
  CHECK_INPUT_NON_EMPTY(__func__, pData1)
  CHECK_INPUT_NON_EMPTY(__func__, pData2)
  
  // operate til minimum length
  size_t tLen1 = pData1.size();
  size_t tLen2 = pData2.size();
  size_t tLen  = (tLen1 < tLen2) ? tLen1:tLen2;
  if (tLen != pResult.size())
  {
    pResult.resize(tLen);
  }
  
  for (size_t tI= 0; tI < tLen; tI++)
  {
    pResult[tI] = pData1[tI] + pData2[tI];
  }
}

/** subtract vectors */
void subRlIRFs (TRealIRF& pResult, const TRealIRF& pData1, const TRealIRF& pData2)
{
  // check input lengths
  CHECK_INPUT_NON_EMPTY(__func__, pData1)
  CHECK_INPUT_NON_EMPTY(__func__, pData2)
  
  // operate til minimum length
  size_t tLen1 = pData1.size();
  size_t tLen2 = pData2.size();
  size_t tLen  = (tLen1 < tLen2) ? tLen1:tLen2;
  if (tLen != pResult.size())
  {
    pResult.resize(tLen);
  }
  
  for (size_t tI= 0; tI < tLen; tI++)
  {
    pResult[tI] = pData1[tI] - pData2[tI];
  }
}

void subRlIRFs (TRealIRF& pResult, const TRealIRF& pData , const double&   pVal)
{
  // check input lengths
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  size_t tLen = pData.size();
  if(tLen != pResult.size())
  {
    pResult.resize(tLen);
  }
  
  for(size_t tI = 0; tI < tLen; tI++)
  {
    pResult[tI] = pData[tI] - pVal;
  }
}

/** multiply vectors */
void mulRlIRFs (TRealIRF& pResult, const TRealIRF& pData1, const TRealIRF& pData2)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData1)
  size_t tLen = pData1.size();

  //check same length
  if (pData2.size() != tLen)
  {
    throw ilmsens::processing::Error(std::string(__func__) + ": vectors must have the same length!", ILMSENS_ERROR_INVALID_PARAM);
  }

  for(size_t tI = 0; tI < tLen; tI++)
  {
    pResult[tI] = pData1[tI] * pData2[tI];
  }
}

/** absolute value of real sample vector */
void absRlIRFs (TRealIRF& pResult, const TRealIRF& pData)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // harmonise lengths
  size_t tLen = pData.size();
  if (tLen != pResult.size())
  {
    pResult.resize(tLen);
  }

  for (size_t tI = 0; tI < tLen; tI ++)
  {
    pResult[tI] = fabs(pData[tI]);
  }
}

/** squared value of real sample vector */
void sqrRlIRFs (TRealIRF& pResult, const TRealIRF& pData)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // harmonise lengths
  size_t tLen = pData.size();
  if (tLen != pResult.size())
  {
    pResult.resize(tLen);
  }

  for (size_t tI = 0; tI < tLen; tI ++)
  {
    pResult[tI] = pData[tI] * pData[tI];
  }
}

/** scaling of real sample vector */
void scaleRlIRFs (TRealIRF& pResult, const TRealIRF& pData, TSample pScale)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)

  // harmonise lengths
  size_t tLen = pData.size();
  if (tLen != pResult.size())
  {
    pResult.resize(tLen);
  }

  for (size_t tI = 0; tI < tLen; tI ++)
  {
    pResult[tI] = pData[tI] * pScale;
  }
}

/** centering of real sample vector */
void centerRlIRFs (TRealIRF& pResult, const TRealIRF& pData, TSample pMean, TSample pStd)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData)
  size_t tLen = pData.size();

  // check std. deviation
  if (pStd == 0.0)
  {
    // when there is no variance in the signal, the centered result is zero!
    pResult.resize(tLen, 0.0);
    return;
  }

  // harmonise lengths
  if (tLen != pResult.size())
  {
    pResult.resize(tLen);
  }

  for (size_t tI = 0; tI < tLen; tI ++)
  {
    pResult[tI] = (pData[tI] - pMean) / pStd;
  }
}

/** scalar product of vectors */
TSample scalarProd (TRealIRF& pData1, const TRealIRF& pData2)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pData1)
  size_t tLen = pData1.size();

  //check same length
  if (pData2.size() != tLen)
  {
    throw ilmsens::processing::Error(std::string(__func__) + ": vectors must have the same length!", ILMSENS_ERROR_INVALID_PARAM);
  }

  //calculate scalar product
  TSample tSP = 0.0;
  for (size_t tI= 0; tI < tLen; tI++)
  {
    tSP += pData1[tI] * pData2[tI];
  }

  return(tSP);
}

/** diff of successive entries of a vector  */
void entryDiffRlIRFs (TRealIRF& pResult, const TRealIRF& pData)
{
  // check input lengths
  CHECK_INPUT_NON_EMPTY(__func__, pData)
  
  // operate til minimum length
  size_t tLenData = pData.size();
  size_t tLen  = (tLenData < 1) ? (tLenData - 1) : 1;
  if (tLen != pResult.size())
  {
    pResult.resize(tLen);
  }
  
  if (tLenData > 1)
  {
    for (size_t tI= 0; tI < tLen; tI++)
    {
      pResult[tI] = pData[tI] - pData[tI + 1];
    }
  }
  else
  {
    pResult[1] = 0;
  }
}


/*********************************/
/** advanced vector operations  **/
/*********************************/

/** exponential BGR */
void expBGRRl (TRealIRF& pBGR, TRealIRF& pBG, const TRealIRF& pUpdate, double pAlpha)
{
  // check input length
  CHECK_INPUT_NON_EMPTY(__func__, pBGR)
  CHECK_INPUT_NON_EMPTY(__func__, pBG)
  CHECK_INPUT_NON_EMPTY(__func__, pUpdate)
  
  // check alpha parameter
  if (pAlpha < 0.0)
  {
    throw ilmsens::processing::Error(std::string(__func__) + ": alpha must not be below 0.0!", ILMSENS_ERROR_INVALID_PARAM);
  }
  if (pAlpha > 1.0)
  {
    throw ilmsens::processing::Error(std::string(__func__) + ": alpha must not be above 1.0!", ILMSENS_ERROR_INVALID_PARAM);
  }

  // harmonise lengths
  size_t tLen = pUpdate.size();
  if (tLen != pBGR.size())
  {
    pBGR.resize(tLen, 0.0);
  }
  if (tLen != pBG.size())
  {
    pBG.resize(tLen, 0.0);
  }

  // perform exponential BGR
  double tAInv = 1.0 - pAlpha;
  for (size_t tI = 0; tI < tLen; tI ++)
  {
    TSample tCurVal = pUpdate[tI];
    TSample tCurBG  = pBG[tI];
    
    // do BGR
    pBGR[tI] = tCurVal - tCurBG; 

    // update the background
    pBG[tI] = tAInv * tCurBG + pAlpha * tCurVal;
  }
}

const TSample lin_interpolation(TRealIRF &pFreq, TRealIRF &pSprec, double pSearchFreq)
{
  int tSize = static_cast<int>(pFreq.size());
  int tIdx  = 0;

  if(pSearchFreq >= pFreq[tSize - 2])
  {
    tIdx = tSize - 2;
  }
  else
  {
    while (pSearchFreq > pFreq[tIdx+1])
    {
      tIdx++;
    }
  }
  double pFreqL  = pFreq[tIdx];
  double pFreqR  = pFreq[tIdx + 1];
  double pSprecL = pSprec[tIdx];
  double pSprecR = pSprec[tIdx + 1];

  if(pSearchFreq < pFreqL)
    pSprecR = pSprecL;
  if(pSearchFreq > pFreqR)
    pSprecL = pSprecR;

  double aux = (pSprecR - pSprecL) / (pFreqR - pFreqL);
  
  return pSprecL + aux * (pSearchFreq - pFreqL);
}

} //namespace processing
} //namespace ilmsens
