#get headers in this folder as well
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )

#static networking library

# decide to use correct class for environment
if (WIN32)
  set (TCP_sources "TCP_server_single_win.cpp")
else()
  set (TCP_sources "TCP_server_single.cpp")
endif ()

add_library(ilmsens_networking STATIC ${TCP_sources} ${ILMSENS_LIB_NETWORKING_HEADERS})

#add required Winsock2 lib(s)
if (WIN32)
  target_link_libraries(ilmsens_networking "Ws2_32.lib")
endif ()

set_property(TARGET ilmsens_networking PROPERTY POSITION_INDEPENDENT_CODE ON)
set_property(TARGET ilmsens_networking PROPERTY FOLDER "Ilmsens Libraries")
