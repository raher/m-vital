/* TCP_server_single.CPP
 * 
 * TCP server for a single client (i.e. other clients are not served, when a client connection is valid).
 */

// Strings and std.-output
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <chrono>
#include <thread>
#include <mutex>

// TCP networking

//#include <windows.h>

//#include <sys/socket.h>                   // TCP-Sockets and gneral functions
//#include <netinet/in.h>                   // for sock_addr_in (strcutures for internet addresses), 
                                          // also includes stuff from <arpa/inet.h> like htonl(...) ...
//#include <arpa/inet.h>                                          

//#include <unistd.h>                       // for select() call (accept with timeout), usleep(...), ...

//#include <errno.h>                        // error definitions for socket functions...
//#include <time.h>

//#include <arpa/inet.h>                            // for TCP-Sockets
//#include <netdb.h>                          // for TCP-Sockets
//#include <sys/types.h>                    // for TCP-Sockets

// own headers
#include "ilmsens/networking/TCP_server_single.hpp"
#include "ilmsens/networking/ilmsens_networking_defn.h"
#include "ilmsens/networking/ilmsens_networking_error.hpp"

namespace ilmsens 
{

namespace networking
{

// protect WSA startup
typedef std::lock_guard<std::mutex> LockGuard;

std::mutex& WSALock()
{
  static std::mutex sLock;
  return sLock;
}



/**************************************************************************************************/
/** Constructor                                                                                   */
/**************************************************************************************************/
TCP_server_single::TCP_server_single(unsigned int pTCPPort, unsigned int pSendTO_ms)
: mTCPPort(pTCPPort),
  mSendTO(pSendTO_ms),
  mServerSock(INVALID_SOCKET),
  mClientSock(INVALID_SOCKET),
  mWSAInit(false),
  mClient(false) 
{
  // empty client IP string
  mClientIP[0] = 0;

  // create the listening socket, if port is already valid
  if (mTCPPort)
  {
    startServer(mTCPPort, mSendTO); // use default bind timeout
  }
}


/**************************************************************************************************/
/** Destructor                                                                                    */
/**************************************************************************************************/
TCP_server_single::~TCP_server_single()
{
  // close the client socket, if still connected, and close the server's listening socket
  closeServer();
}


/**************************************************************************************************/
/** Make connections                                                                              */
/**************************************************************************************************/

/* Register listening socket for server */
void TCP_server_single::startServer(unsigned int pTCPPort, unsigned int pSendTO_ms, unsigned int /*pBindTO_ms*/)
{
  /* Check parameters and cleanup */

  // check given port
  if (!pTCPPort && !mTCPPort)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: TCP port number must not be 0!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  else
  {
    // set new port if valid
    if (pTCPPort) mTCPPort = pTCPPort;
  }
  
  // check given send timeout
  if (!pSendTO_ms && !mSendTO)
  {
    // use default send timeout
    mSendTO = ILMSENS_NETWORKING_SEND_TIMEOUT_MS;
  }
  else
  {
    // use given timeout if valid
    if (pSendTO_ms) mSendTO = pSendTO_ms;
  }

  // cleanup potential old connections
  closeServer();

  
  LockGuard tGuard(WSALock());
      
  // Initialize Winsock
  if (!mWSAInit)
  {
    int tWSARes = WSAStartup(MAKEWORD(2,2), &mWSAData);
    if (tWSARes != 0)
    {
      throw ilmsens::networking::Error("TCP_server_single::ERROR: could not initialize Winsock2!", ILMSENS_ERROR_NO_MEMORY, WSAGetLastError());
    }

    mWSAInit = true;
  }
  //std::cout << "TCP_server_single::INFO: WSAData is  V" << (mWSAData.wVersion >> 8) << "." << (mWSAData.wVersion & 0xff)
  //                                                      << " | desc: '" << mWSAData.szDescription 
  //                                                      << "' | state: '" << mWSAData.szSystemStatus
  //                                                      << "' for TCP port " << mTCPPort << "." << std::endl;

  /* get server address info */
  struct addrinfo tHints;

  memset((void *)&tHints, 0, sizeof(addrinfo));
  tHints.ai_family   = AF_INET;
  tHints.ai_socktype = SOCK_STREAM;
  tHints.ai_protocol = IPPROTO_TCP;
  tHints.ai_flags    = AI_PASSIVE;

  // Resolve the server address and port
  struct addrinfo *tResAddr = NULL;
  int tRes = getaddrinfo(NULL, std::to_string(mTCPPort).c_str(), &tHints, &tResAddr);
  if (tRes != 0 ) 
  {
    //cleanup
    WSACleanup();
    mWSAInit = false;

    throw ilmsens::networking::Error("TCP_server_single::ERROR: could not resolve server address!", ILMSENS_ERROR_NO_MEMORY, tRes);
  }
  /*
  else
  {
    struct sockaddr_in  *tSA = (struct sockaddr_in *)tResAddr->ai_addr;
    std::string tCanName(inet_ntoa(tSA->sin_addr));
    std::cout << "TCP_server_single::INFO: server address is " << tCanName.c_str() << " for TCP port " << mTCPPort << "." << std::endl;
  }
  */

  /* Create a SOCKET for connecting to server */
  mServerSock = socket(tResAddr->ai_family, tResAddr->ai_socktype, tResAddr->ai_protocol);
  if (mServerSock == INVALID_SOCKET)
  {
    //cleanup
    tRes = WSAGetLastError();
    freeaddrinfo(tResAddr);
    WSACleanup();
    mWSAInit = false;

    throw ilmsens::networking::Error("TCP_server_single::ERROR: could not create server socket for listening!", ILMSENS_ERROR_NO_MEMORY, tRes);
  }

  // configure server socket: reuse an already occupied address
  bool tOpt = true;
  tRes = setsockopt(mServerSock, SOL_SOCKET, SO_REUSEADDR, (const char *)&tOpt, sizeof(tOpt));
  if (tRes == SOCKET_ERROR)
  {
    //cleanup
    tRes = WSAGetLastError();
    freeaddrinfo(tResAddr);
    closesocket(mServerSock);
    mServerSock = INVALID_SOCKET;
    WSACleanup();
    mWSAInit = false;

    throw ilmsens::networking::Error("TCP_server_single::ERROR: could not set property REUSEAADR for server socket!", ILMSENS_ERROR_NO_MEMORY, tRes);
  }

  // configure server socket: send timeout
  DWORD tTO = (DWORD) mSendTO;
  tRes = setsockopt(mServerSock, SOL_SOCKET, SO_SNDTIMEO, (const char *)&tTO, sizeof(tTO));
  if (tRes == SOCKET_ERROR)
  {
    //cleanup
    tRes = WSAGetLastError();
    freeaddrinfo(tResAddr);
    closesocket(mServerSock);
    mServerSock = INVALID_SOCKET;
    WSACleanup();
    mWSAInit = false;

    throw ilmsens::networking::Error("TCP_server_single::ERROR: could not set property SENDTIMEOUT for server socket!", ILMSENS_ERROR_NO_MEMORY, tRes);
  }

  /* Bind to socket */
  tRes = bind(mServerSock, tResAddr->ai_addr, (int)tResAddr->ai_addrlen);
  if (tRes == SOCKET_ERROR)
  {
    //cleanup
    tRes = WSAGetLastError();
    freeaddrinfo(tResAddr);
    closesocket(mServerSock);
    mServerSock = INVALID_SOCKET;
    WSACleanup();
    mWSAInit = false;

    throw ilmsens::networking::Error("TCP_server_single::ERROR: could not bind server socket!", ILMSENS_ERROR_NO_MEMORY, tRes);
  }

  // finally free unused address ressources
  freeaddrinfo(tResAddr);

  /* Listen on the newly bound socket */
  tRes = listen(mServerSock, SOMAXCONN);
  if (tRes == SOCKET_ERROR) 
  {
    //cleanup
    tRes = WSAGetLastError();
    closesocket(mServerSock);
    mServerSock = INVALID_SOCKET;
    WSACleanup();
    mWSAInit = false;

    throw ilmsens::networking::Error("TCP_server_single::ERROR: could not listen on server socket!", ILMSENS_ERROR_NO_MEMORY, tRes);
  }
}

/* Accept a client (with timeout) */
bool TCP_server_single::acceptClient(unsigned int pTO_ms)
{
  // check, if a client is already connected
  if (!mClient && (mServerSock != INVALID_SOCKET))
  {
    // no client accepted yet ...
    // define timeout for select
    timeval tTO;
    tTO.tv_usec = (pTO_ms % 1000) * 1000;
    tTO.tv_sec = pTO_ms / 1000;
    
    // accepting with timeout using select
    fd_set tReadFDS;
    
    FD_ZERO(&tReadFDS);              //clear it
    FD_SET (mServerSock, &tReadFDS); //add listening socket to FD set
    int tFDMax = 0;                  // maximum socket descriptor number in the set -> ignored in WinSock2!
    
    // call select with timeout for accepting new connections
    int tSelRes = select(tFDMax, &tReadFDS, NULL, NULL, &tTO);
    if (tSelRes == SOCKET_ERROR) 
    {
      throw ilmsens::networking::Error("TCP_server_single::ERROR: error during waiting for client!", ILMSENS_ERROR_IO, WSAGetLastError());
    }

    // test, if new client was connected
    if (tSelRes && FD_ISSET(mServerSock, &tReadFDS))
    {
      // a new client can be accepted
      struct sockaddr_in tClient;
      socklen_t tLen = sizeof(tClient);

      mClientSock = accept(mServerSock, (struct sockaddr*)&tClient, &tLen); 
      if(mClientSock == INVALID_SOCKET) 
      {
        throw ilmsens::networking::Error("TCP_server_single::ERROR: error accepting new client!", ILMSENS_ERROR_NO_MEMORY, WSAGetLastError());
      }

      // apply socket options: send timeout
      DWORD tTO = (DWORD) mSendTO;
      int tRes = setsockopt(mClientSock, SOL_SOCKET, SO_SNDTIMEO, (const char *)&tTO, sizeof(tTO));
      if (tRes == SOCKET_ERROR)
      {
        //cleanup
        tRes = WSAGetLastError();
        closesocket(mClientSock);
        mClientSock = INVALID_SOCKET;

        throw ilmsens::networking::Error("TCP_server_single::ERROR: could not set property SENDTIMEOUT for client socket!", ILMSENS_ERROR_NO_MEMORY, tRes);
      }

      // get client's IP
      strcpy_s(mClientIP, IMSENS_NETWORKING_IP_STR_BUF_LEN, inet_ntoa(tClient.sin_addr));

      mClient = true;
    }
  }

  return(mClient);
}

/* Return the IP string of the connected client */
const char* TCP_server_single::getClientIP(void)
{
  return(mClientIP);
}


/**************************************************************************************************/
/** Break connections                                                                             */
/**************************************************************************************************/
        
/* close connection to current client */
void TCP_server_single::closeClient(void)
{
  //check, if client is currently connected
  if (mClient)
  {
    if (mClientSock != INVALID_SOCKET) 
    {
      // close old connection
      closesocket(mClientSock);
      
      // prepare for new client connections
      mClientSock = INVALID_SOCKET;
    }
    
    // no client connected anymore
    mClient = false;
    // empty client IP string
    mClientIP[0] = 0;
  }
}

/* close connection to client and also listenung socket of server */
void TCP_server_single::closeServer(void)
{
  // close client connection, if any
  closeClient();

  // also close listening socket
  if (mServerSock != INVALID_SOCKET) 
  {
    // close old connection
    closesocket(mServerSock);
    
    // prepare for new connections
    mServerSock = INVALID_SOCKET;
  }
  
  LockGuard tGuard(WSALock());

  // cleanup WSA ressources
  if (mWSAInit)
  {
    WSACleanup();
    mWSAInit = false;
  }
}


/**************************************************************************************************/
/** Receiving data from client                                                                    */
/**************************************************************************************************/
        
/* Blocking receiving */
int TCP_server_single::receiveWait(void *pBuffer, unsigned int pNumBytes)
{
  int tErg = -1; // default result: client connection not available
  
  // check inputs
  if (!pBuffer)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: receive buffer pointer was NULL!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  if (!pNumBytes)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: number of bytes to receive was 0!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  
  // check, if a client is connected anyways
  if (mClient && (mClientSock != INVALID_SOCKET))
  {
    char *tBufBytes = (char *)pBuffer; //access buffer byte-wise

    tErg = 0;
    while ((unsigned int)tErg < pNumBytes)
    {

      int tLen = recv(mClientSock, tBufBytes+tErg, pNumBytes - tErg, 0); // no flags, i.e. no timeout
      
      // check result
      if (tLen == SOCKET_ERROR)
      {
        // an error occured, check for errors that indicate no data was there
        int tErrNo = WSAGetLastError();

        if((tErrNo == WSAECONNRESET) || (tErrNo == WSAECONNABORTED))
        {
          //client connection closed unexpectedly
          closeClient();

          tErg = -1;
          break;
        }

        // report another error
        throw ilmsens::networking::Error("TCP_server_single::ERROR: could not receive data from client!", ILMSENS_ERROR_IO, tErrNo);
      }
      if (tLen == 0)
      {
        // client has gracefully closed the connection
        tErg = -1;
        break;
      }
      else
      {
        // something has been received
        tErg += tLen;
      }

    } // receive while loop
  } // valid client connection
  
  // return the result
  return(tErg);
}

/* Non-blocking receiving */
int TCP_server_single::receiveTO(void *pBuffer, unsigned int pNumBytes, unsigned int pTO_ms)
{
  int tErg = -1; // default result: client connection not available
  
  // check inputs
  if (!pBuffer)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: receive buffer pointer was NULL!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  if (!pNumBytes)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: number of bytes to receive was 0!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  
  // check, if a client is connected anyways
  if (mClient && (mClientSock != INVALID_SOCKET))
  {
    // prepare minding the timeout
    auto tStartTime = std::chrono::system_clock::now();

    // define timeout for select
    timeval tTO;
    tTO.tv_usec = (pTO_ms % 1000) * 1000;
    tTO.tv_sec = pTO_ms / 1000;
    
    // reading with timeout using select
    fd_set tReadFDS;
    
    char *tBufBytes = (char *)pBuffer; //access buffer byte-wise
    tErg = 0;
    while ((unsigned int)tErg < pNumBytes)
    {
      FD_ZERO(&tReadFDS);              //clear it
      FD_SET (mClientSock, &tReadFDS); //add client socket to FD set
      int tFDMax = 0;                  // maximum socket descriptor number in the set -> ignored in WinSock2!

      // call select with timeout for readable connections
      int tSelRes = select(tFDMax, &tReadFDS, NULL, NULL, &tTO);
      if (tSelRes == SOCKET_ERROR) 
      {
        throw ilmsens::networking::Error("TCP_server_single::ERROR: error during reading from client!", ILMSENS_ERROR_IO, WSAGetLastError());
      }

      // test, if new client was connected
      if (tSelRes && FD_ISSET(mClientSock, &tReadFDS))
      {
        int tLen = recv(mClientSock, (tBufBytes+tErg), pNumBytes - tErg, 0); // do not block when reading
      
        // check result for timeout and error
        if (tLen == SOCKET_ERROR)
        {
          // an error occured, check for errors that indicate no data was there
          int tErrNo = WSAGetLastError();

          if((tErrNo == WSAECONNRESET) || (tErrNo == WSAECONNABORTED))
          {
            //client connection closed unexpectedly
            closeClient();

            tErg = -1;
            break;
          }

          // non-recoverable error
          throw ilmsens::networking::Error("TCP_server_single::ERROR: could not receive data from client!", ILMSENS_ERROR_IO, tErrNo);
        }

        // check result for lost connection
        if (tLen == 0)
        {
          // client has gracefully closed the connection
          tErg = -1;
          break;
        }
        else
        {
          // something has been received
          tErg += tLen;
        }

      } //socket selected

      // mind the timeout
      if (pTO_ms && ((unsigned int)tErg < pNumBytes))
      {
        auto tDur = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now() - tStartTime);
        if (tDur.count() > pTO_ms)
        {
          // timeout elapsed, so leave
          break;
        }
        else
        {
          // nothing received, so wait some time & try to receive more
          std::this_thread::sleep_for(std::chrono::milliseconds(1)); // wait 1 ms
          continue;
        }
      } // timeout check

    } // receive while loop
  } // valid client connection
  
  // return the result
  return(tErg);
}


/**************************************************************************************************/
/** Sending data to client                                                                        */
/**************************************************************************************************/

/* Sending data to client with default send-timeout */
int TCP_server_single::sendTO(void *pBuffer, unsigned int pNumBytes)
{
  int tErg = -1; // default result: client connection not available
  
  // check inputs
  if (!pBuffer)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: send buffer pointer was NULL!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  if (!pNumBytes)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: number of bytes to send was 0!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  
  // check, if a client is connected anyways
  if (mClient && (mClientSock != INVALID_SOCKET))
  {
    // prepare minding the timeout
    auto tStartTime = std::chrono::system_clock::now();

    char *tBufBytes = (char *)pBuffer; //access buffer byte-wise
    tErg = 0;
    while ((unsigned int)tErg < pNumBytes)
    {

      int tLen = send(mClientSock, tBufBytes+tErg, pNumBytes - tErg, 0); // use default timeout
      
      // check result for timeout and lost connection error
      if (tLen == SOCKET_ERROR)
      {
        // an error occured, check for errors that indicate no data was there
        int tErrNo = WSAGetLastError();

        if((tErrNo == WSAECONNRESET) || (tErrNo == WSAECONNABORTED))
        {
          //client connection closed unexpectedly
          closeClient();

          tErg = -1;
          break;
        }

        // check for timeout
        if (tErrNo == WSAETIMEDOUT)
        {
          // timeout while sending data
          auto tDur = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now() - tStartTime);
          if (tDur.count() > mSendTO)
          {
            // timeout elapsed, so leave
            break;
          }
          else
          {
            // try to send more
            continue;
          }
        }
        else
        {
          // non-recoverable error
          throw ilmsens::networking::Error("TCP_server_single::ERROR: could not send data to client!", ILMSENS_ERROR_IO, tErrNo);
        }
      }

      // check result for lost connection or timeout
      if (tLen == 0)
      {
        // nothing was sent within timeout, so leave
        break;
      }
      else
      {
        // something has been sent
        tErg += tLen;
      }

    } // send while loop
  } // valid client connection
  
  // return the result
  return(tErg);
}

} //namespace networking
} //namespace ilmsens
