/* TCP_server_single.CPP
 * 
 * TCP server for a single client (i.e. other clients are not served, when a client connection is valid).
 */

// Strings and std.-output
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <chrono>

// TCP networking
#include <sys/socket.h>                   // TCP-Sockets and gneral functions
#include <netinet/in.h>                   // for sock_addr_in (strcutures for internet addresses), 
                                          // also includes stuff from <arpa/inet.h> like htonl(...) ...
#include <arpa/inet.h>                                          

#include <unistd.h>                       // for select() call (accept with timeout), usleep(...), ...
#include <errno.h>                        // error definitions for socket functions...
#include <time.h>

//#include <arpa/inet.h>                            // for TCP-Sockets
//#include <netdb.h>                          // for TCP-Sockets
//#include <sys/types.h>                    // for TCP-Sockets

// own headers
#include "ilmsens/networking/TCP_server_single.hpp"
#include "ilmsens/networking/ilmsens_networking_defn.h"
#include "ilmsens/networking/ilmsens_networking_error.hpp"


namespace ilmsens 
{

namespace networking
{


/**************************************************************************************************/
/** Constructor                                                                                   */
/**************************************************************************************************/
TCP_server_single::TCP_server_single(unsigned int pTCPPort, unsigned int pSendTO_ms)
: mTCPPort(pTCPPort),
  mSendTO(pSendTO_ms),
  mServerSock(0),
  mClientSock(0),
  mClient(false) 
{
  // empty client IP string
  mClientIP[0] = 0;

  // create the listening socket, if port is already valid
  if (mTCPPort)
  {
    startServer(mTCPPort, mSendTO); // use default bind timeout
  }
}


/**************************************************************************************************/
/** Destructor                                                                                    */
/**************************************************************************************************/
TCP_server_single::~TCP_server_single()
{
  // close the client socket, if still connected, and close the server's listening socket
  closeServer();
}


/**************************************************************************************************/
/** Make connections                                                                              */
/**************************************************************************************************/

/* Register listening socket for server */
void TCP_server_single::startServer(unsigned int pTCPPort, unsigned int pSendTO_ms, unsigned int pBindTO_ms)
{
  /* Check parameters and cleanup */

  // check given port
  if (!pTCPPort && !mTCPPort)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: TCP port number must not be 0!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  else
  {
    // set new port if valid
    if (pTCPPort) mTCPPort = pTCPPort;
  }
  
  // check given send timeout
  if (!pSendTO_ms && !mSendTO)
  {
    // use default send timeout
    mSendTO = ILMSENS_NETWORKING_SEND_TIMEOUT_MS;
  }
  else
  {
    // use given timeout if valid
    if (pSendTO_ms) mSendTO = pSendTO_ms;
  }
  
  // cleanup potential old connections
  closeServer();


  /* Create new listening socket for server */
  mServerSock = socket(AF_INET, SOCK_STREAM, 0);
  if(mServerSock < 0) 
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: could not create listening socket!", ILMSENS_ERROR_NO_MEMORY, errno);
  }
  
  // configure server socket: reuse an already occupied address
  int tOpt = 1;
  setsockopt(mServerSock, SOL_SOCKET, SO_REUSEADDR, &tOpt, sizeof(tOpt));
  
  // configure server socket: send timeout
  timeval tTO;
  tTO.tv_usec = (mSendTO % 1000) * 1000;
  tTO.tv_sec = mSendTO / 1000;
  setsockopt(mServerSock, SOL_SOCKET, SO_SNDTIMEO, &tTO, sizeof(tTO));


  /* Bind to socket */
  
  // prepare binding: define socket properties such as TCP port of interface address to bind to
  struct sockaddr_in tServerAddr;
  memset((void *)&tServerAddr, 0, sizeof(tServerAddr)); // start clean
  tServerAddr.sin_family      = AF_INET;                // IPv4
  tServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);      // bind to every IP-Adress (interface) )available
  tServerAddr.sin_port        = htons(mTCPPort);        // set TCP port
  
  // get binding timeout
  unsigned int tMaxTries = ILMSENS_NETWORKING_TCP_BIND_MAX_RETRY;
  if (pBindTO_ms)
  {
    tMaxTries = pBindTO_ms * 1000; // make it us
    tMaxTries /= ILMSENS_NETWORKING_PAUSE_US; // calculate maximum number of tries
  }

  // binding to socket
  unsigned int tBindTries = 0;
  while( bind(mServerSock, (struct sockaddr*)&tServerAddr, sizeof(tServerAddr)) < 0 )
  {
    if (tBindTries < tMaxTries)
    {
      // wait some time, socket may become available
      usleep(ILMSENS_NETWORKING_PAUSE_US);
    }
    else
    {
      // error, close the socket again and go
      close(mServerSock);
      mServerSock = 0;

      throw ilmsens::networking::Error("TCP_server_single::ERROR: could not bind to server socket!", ILMSENS_ERROR_NO_MEMORY, errno);
    }
  }
  
  // listen on the newly bound socket
  if(listen(mServerSock, ILMSENS_NETWORKING_MAX_CLIENTS) < 0) 
  { 
    // error, close the socket again and go
    close(mServerSock);
    mServerSock = 0;

    throw ilmsens::networking::Error("TCP_server_single::ERROR: could not listen on server socket!", ILMSENS_ERROR_NO_MEMORY, errno);
  }
}

/* Accept a client (with timeout) */
bool TCP_server_single::acceptClient(unsigned int pTO_ms)
{
  // check, if a client is already connected
  if (!mClient && (mServerSock > 0))
  {
    // no client accepted yet ...
    // define timeout for select
    timeval tTO;
    tTO.tv_usec = (pTO_ms % 1000) * 1000;
    tTO.tv_sec = pTO_ms / 1000;
    
    // accepting with timeout using select
    fd_set tReadFDS;
    
    FD_ZERO(&tReadFDS);              //clear it
    FD_SET (mServerSock, &tReadFDS); //add listening socket to FD set
    int tFDMax = mServerSock;        // maximum socket descriptor number in the set
    
    // call select with timeout for accepting new connections
    int tSelRes = select(tFDMax+1, &tReadFDS, NULL, NULL, &tTO);
    if (tSelRes < 0) 
    {
      throw ilmsens::networking::Error("TCP_server_single::ERROR: error during waiting for client!", ILMSENS_ERROR_IO, errno);
    }

    // test, if new client was connected
    if (FD_ISSET(mServerSock, &tReadFDS))
    {
      // a new client can be accepted
      struct sockaddr_in tClient;
      socklen_t tLen = sizeof(tClient);

      mClientSock = accept(mServerSock, (struct sockaddr*)&tClient, &tLen); 
      if(mClientSock < 0) 
      {
        throw ilmsens::networking::Error("TCP_server_single::ERROR: error accepting new client!", ILMSENS_ERROR_NO_MEMORY, errno);
      }

      // apply socket options: send timeout
      timeval tTO;
      tTO.tv_usec = (mSendTO % 1000) * 1000;
      tTO.tv_sec = mSendTO / 1000;
      setsockopt(mClientSock, SOL_SOCKET, SO_SNDTIMEO, &tTO, sizeof(tTO));

      // get client's IP
      strcpy(mClientIP, inet_ntoa(tClient.sin_addr));

      mClient = true;
    }
  }

  return(mClient);
}

/* Return the IP string of the connected client */
const char* TCP_server_single::getClientIP(void)
{
  return(mClientIP);
}


/**************************************************************************************************/
/** Break connections                                                                             */
/**************************************************************************************************/
        
/* close connection to current client */
void TCP_server_single::closeClient(void)
{
  //check, if client is currently connected
  if (mClient)
  {
    if (mClientSock > 0) 
    {
      // close old connection
      close(mClientSock);
      
      // prepare for new client connections
      mClientSock = 0;
    }
    
    // no client connected anymore
    mClient = false;
    // empty client IP string
    mClientIP[0] = 0;
  }
}

/* close connection to client and also listenung socket of server */
void TCP_server_single::closeServer(void)
{
  // close client connection, if any
  closeClient();

  // also close listening socket
  if (mServerSock > 0) 
  {
    // close old connection
    close(mServerSock);
    
    // prepare for new connections
    mServerSock = 0;
  }
}


/**************************************************************************************************/
/** Receiving data from client                                                                    */
/**************************************************************************************************/
        
/* Blocking receiving */
int TCP_server_single::receiveWait(void *pBuffer, unsigned int pNumBytes)
{
  int tErg = -1; // default result: client connection not available
  
  // check inputs
  if (!pBuffer)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: receive buffer pointer was NULL!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  if (!pNumBytes)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: number of bytes to receive was 0!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  
  // check, if a client is connected anyways
  if (mClient && (mClientSock > 0))
  {
    char *tBufBytes = (char *)pBuffer; //access buffer byte-wise

    tErg = 0;
    while ((unsigned int)tErg < pNumBytes)
    {

      int tLen = recv(mClientSock, (void *)(tBufBytes+tErg), pNumBytes - tErg, 0); // no flags, i.e. no timeout
      
      // check result
      if (tLen < 0)
      {
        // error
        throw ilmsens::networking::Error("TCP_server_single::ERROR: could not receive data from client!", ILMSENS_ERROR_IO, errno);
      }
      if (tLen == 0)
      {
        // client has closed the connection
        tErg = -1;
        break;
      }
      else
      {
        // something has been received
        tErg += tLen;
      }

    } // receive while loop
  } // valid client connection
  
  // return the result
  return(tErg);
}

/* Non-blocking receiving */
int TCP_server_single::receiveTO(void *pBuffer, unsigned int pNumBytes, unsigned int pTO_ms)
{
  int tErg = -1; // default result: client connection not available
  
  // check inputs
  if (!pBuffer)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: receive buffer pointer was NULL!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  if (!pNumBytes)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: number of bytes to receive was 0!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  
  // check, if a client is connected anyways
  if (mClient && (mClientSock > 0))
  {
    // prepare minding the timeout
    auto tStartTime = std::chrono::system_clock::now();
    
    char *tBufBytes = (char *)pBuffer; //access buffer byte-wise
    tErg = 0;
    while ((unsigned int)tErg < pNumBytes)
    {

      int tLen = recv(mClientSock, (void *)(tBufBytes+tErg), pNumBytes - tErg, MSG_DONTWAIT); // do not block when reading
      
      // check result for timeout and error
      if (tLen < 0)
      {
        // an error occured, check for errors that indicate no data was there
        int tErrNo = errno;
        
        if ((tErrNo == EAGAIN) || (tErrNo == EWOULDBLOCK))
        {
          // there simply was no data, so check timeout
          auto tDur = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now() - tStartTime);
          if ((tDur.count() > pTO_ms) || (!pTO_ms))
          {
            // timeout elapsed, so leave
            break;
          }
          else
          {
            // nothing received, so wait some time & try to receive more
            usleep(1000); // wait 1 ms
            continue;
          }
        }
        else
        {
          // non-recoverable error
          throw ilmsens::networking::Error("TCP_server_single::ERROR: could not receive data from client!", ILMSENS_ERROR_IO, tErrNo);
        }
      }

      // check result for lost connection
      if (tLen == 0)
      {
        // client has closed the connection
        tErg = -1;
        break;
      }
      else
      {
        // something has been received
        tErg += tLen;
      }

    } // receive while loop
  } // valid client connection
  
  // return the result
  return(tErg);
}


/**************************************************************************************************/
/** Sending data to client                                                                        */
/**************************************************************************************************/

/* Sending data to client with default send-timeout */
int TCP_server_single::sendTO(void *pBuffer, unsigned int pNumBytes)
{
  int tErg = -1; // default result: client connection not available
  
  // check inputs
  if (!pBuffer)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: send buffer pointer was NULL!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  if (!pNumBytes)
  {
    throw ilmsens::networking::Error("TCP_server_single::ERROR: number of bytes to send was 0!", ILMSENS_ERROR_INVALID_PARAM, 0);
  }
  
  // check, if a client is connected anyways
  if (mClient && (mClientSock > 0))
  {
    // prepare minding the timeout
    auto tStartTime = std::chrono::system_clock::now();

    char *tBufBytes = (char *)pBuffer; //access buffer byte-wise
    tErg = 0;
    while ((unsigned int)tErg < pNumBytes)
    {

      int tLen = send(mClientSock, (void *)(tBufBytes+tErg), pNumBytes - tErg, MSG_NOSIGNAL); // use default timeout, do not signal SIGPIPE when connection was closed
      
      // check result for timeout and lost connection error
      if (tLen < 0)
      {
        int tErrNo = errno;
        
        //socket is closed by peer
        if (tErrNo == EPIPE)
        {
          // indicate closed connection & go
          tErg = -1;
          break;
        }

        // check for timeout
        if ((tErrNo == EAGAIN) || (tErrNo == EWOULDBLOCK))
        {
          // timeout while sending data
          auto tDur = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now() - tStartTime);
          if (tDur.count() > mSendTO)
          {
            // timeout elapsed, so leave
            break;
          }
          else
          {
            // try to send more
            continue;
          }
        }
        else
        {
          // non-recoverable error
          throw ilmsens::networking::Error("TCP_server_single::ERROR: could not send data to client!", ILMSENS_ERROR_IO, errno);
        }
      }

      // check result for lost connection or timeout
      if (tLen == 0)
      {
        // nothing was sent within timeout, so leave
        break;
      }
      else
      {
        // something has been sent
        tErg += tLen;
      }

    } // send while loop
  } // valid client connection
  
  // return the result
  return(tErg);
}

} //namespace networking
} //namespace ilmsens
