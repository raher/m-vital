/* os_intercept.CPP
 * 
 * Intercepting output streams and copying data to a given object.
 */


//#include <iostream>
//#include <iomanip>
//#include <string>
//#include <fstream>

//#include <chrono>

#include "ilmsens/tools/ilmsens_tools_error.hpp"
#include "ilmsens/tools/os_intercept.hpp"


namespace ilmsens 
{
namespace tools
{


/**************************************************************************************************/
/** OSInterceptString                                                                            **/
/**************************************************************************************************/

/* Constructor */
OSInterceptString::OSInterceptString(std::ostream & pSrc, std::string & pBufStr, std::mutex & pLock)
:
  mLock(pLock),
  mSrc(pSrc),
  mNew(pSrc.rdbuf()),
  mOrg(pSrc.rdbuf()),
  mBufStr(pBufStr)
  //,
  //mCnt(0),
  //mProf(0)
{
  // Swap the the old buffer in ostream with this object.
  mSrc.rdbuf(this);
}

/* Destructor */
OSInterceptString::~OSInterceptString()
{
  // Restore old buffer of original object
  mSrc.rdbuf(mOrg);
}

/* Overloaded function receiving the characters streamed. */
std::streamsize OSInterceptString::xsputn(const char *pMsg, std::streamsize pCnt)
{
  /*
  // get current system time
  auto tStart = std::chrono::high_resolution_clock::now();

  mCnt ++;

  std::string tCntMsg = "|Thread count: " + std::to_string(mCnt) + " last execution time: " + std::to_string(mProf) + " | ";
  mNew << tCntMsg;
  */

  // Output to new stream with old buffer (to e.g. screen [std::cout])
  mNew.write(pMsg, pCnt);

  // protect access to provided object
  TGuard tGuard(mLock);

  // Append to external buffering string
  mBufStr.append(pMsg, (std::string::size_type)pCnt);
  
  /*
  // Output to destination stream, if sufficient data has been captured
  if (mBufStr.length() >= 128)
  {
    // protect flushing operation
    TGuard tGuard(mLock);

    // flush buffered string to output
    mDst << mBufStr;
    
    mBufStr.clear();
  }
  */
  
  //mDst.write(pMsg, pCnt);
  //std::string tMsg(pMsg, pCnt);
  //mDst << "Message count: " << std::dec << pCnt << " | Message: '" << tMsg << "'";

  /*
  mCnt --;
  auto tDur = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::high_resolution_clock::now() - tStart);
  mProf = tDur.count();
  */

  return(pCnt);
}

/* Overloaded function handling a buffer overflow (must be present to prevent raised bad bit). */
std::streambuf::int_type OSInterceptString::overflow(int_type pChr)
{
  if(!traits_type::eq_int_type(pChr, traits_type::eof()))
  {
    char_type const tC = traits_type::to_char_type(pChr);
    this->xsputn(&tC, 1);
  }
  return(!traits_type::eof());
}

/* Overloaded function handling a buffer synch request (must be present to prevent raised bad bit). */
int OSInterceptString::sync(void)
{
  return(0);
}


/**************************************************************************************************/
/** OSInterceptRedir                                                                             **/
/**************************************************************************************************/

OSInterceptRedir::OSInterceptRedir(std::ostream & pOS, std::ostream & pRedir)
: mRedir(false), mOS(pOS), mRS(pRedir)
{
}

OSInterceptRedir::~OSInterceptRedir()
{
  undoRedir();
}    

void OSInterceptRedir::doRedir(void)
{
  if (!mRedir)
  {
    // swap read buffer of original and redirected stream
    mOS.rdbuf(mRS.rdbuf(mOS.rdbuf()));
    mRedir = true;
  }
}

void OSInterceptRedir::undoRedir(void)
{
  if (mRedir)
  {
    // swap read buffer of original and redirected stream again
    mOS.rdbuf(mRS.rdbuf(mOS.rdbuf()));
    mRedir = false;
  }
}






#if 0


/**************************************************************************************************/
/** ScopedOSIntercept                                                                            **/
/**************************************************************************************************/

class ScopedOSIntercept : public std::streambuf
{
  public:

    ScopedOSIntercept(std::ostream & pSrc, std::ostream & pDst):
      mSrc(pSrc),
      mDst(pDst),
      mNew(pSrc.rdbuf()),
      mOrg(pSrc.rdbuf()),
      mBufStr("")
    {
      // Swap the the old buffer in ostream with this buffer.
      mSrc.rdbuf(this);
    }

    ~ScopedOSIntercept()
    {
      // Output rest to destination stream
      // protect flushing operation
      TGuard tGuard(mLock);

      // copy rest of buffered string to output
      if (mBufStr.length()) mDst << mBufStr;

      // Restore old buffer
      mSrc.rdbuf(mOrg);
    }

  protected:

    virtual std::streamsize xsputn(const char *pMsg, std::streamsize pCnt)
    {
      TGuard tGuard(mLock);
      
      // Output to new stream with old buffer (to e.g. screen [std::cout])
      mNew.write(pMsg, pCnt);

      // Append to buffering string
      mBufStr.append(pMsg, pCnt);
      
      // Output to destination stream, if sufficient data has been captured
      if (mBufStr.length() >= 128)
      {
        // flush buffered string to output
        mDst << mBufStr;
        
        mBufStr.clear();
      }
      return(pCnt);
    }

    virtual int_type overflow(int_type pChr)
    {
      if(!traits_type::eq_int_type(pChr, traits_type::eof()))
      {
        char_type const tC = traits_type::to_char_type(pChr);
        this->xsputn(&tC, 1);
      }
      return (!traits_type::eof());
    }

    virtual int sync(void)
    {
      return(0);
    }

  private:

    // make it non-copyable
    ScopedOSIntercept(const ScopedOSIntercept&);
    ScopedOSIntercept& operator=(const ScopedOSIntercept&);

    // protection of output stream
    std::mutex mLock;

    // ostream objects
    std::ostream&   mSrc;
    std::ostream&   mDst;
    std::ostream    mNew;

    // stream buffers
    std::streambuf* mOrg;
    
    // string buffers
    std::string mBufStr;
};

#endif

} //namespace tools
} //namespace ilmsens
