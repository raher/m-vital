#includes
add_subdirectory(include)

#Processing library
add_subdirectory(processing_lib)

#Networking library
add_subdirectory(networking_lib)

#Tools library
add_subdirectory(tools_lib)

#m:vital.CES app
add_subdirectory(mvital_ces)
