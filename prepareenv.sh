#!/bin/sh

#prepare
apt-get update

#get basic build environment + poco headers and dev. libs, FFTw3 libs
apt-get install build-essential libpoco-dev libfftw3-3 libfftw3-dev libfftw3-double3 libfftw3-single3

#TODO: check installation of Imsens HAL package
